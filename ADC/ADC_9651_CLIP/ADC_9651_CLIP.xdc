create_clock -name AdcBitClkRaw -period 4.1667 [get_ports {aRmcDio[41]}]
set_false_path  -from [get_ports {aRmcDio[41]}] \
                -through [get_cells window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Isrds_Master] \
                -through [get_cells window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Bufio] \
                -to [get_cells -hierarchical "*Isrds*"]
				
set_false_path  -from [get_clocks AdcBitClkDiv] \
                -to [get_pins {window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/DiagramClockXingx/AdcDataOutLv_reg[*]/R}]

create_generated_clock -name AdcBitClkDiv \
                        -source [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Isrds_Master/DDLY] \
                        -divide_by 6 [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Bufr/O]
						
create_generated_clock -name AdcBitClk \
                        -source [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Isrds_Master/DDLY] \
                        -divide_by 1 [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Bufio/O]
						
#create_generated_clock -name RegClkLv \
#                        -source [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/Gen_Bufr_Div_3.AdcClock_I_Bufr/O] \
#                        -divide_by 1 [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/RegClk_Bufr/O]

create_generated_clock -name AdcBitClkDivLv \
                       -source [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/DataClkDivBufG/SafeBUFGCTRLx/I1] \
                       -divide_by 1 \
                       [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/DataClkDivBufG/SafeBUFGCTRLx/O]
					   
set_clock_uncertainty -hold 0.290 -from [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/DataClkDivBufG/SafeBUFGCTRLx/O]] -to [get_clocks AdcBitClkDivLv]

set clipBufgCtrl [get_cells -hier window/theCLIPs/RMC_Socket_CLIP0/*SafeBUFGCTRLx -filter {IS_SEQUENTIAL==TRUE}]
set clipBufgCtrlCePins [get_pins -of_objects $clipBufgCtrl -filter {REF_PIN_NAME == S0 || REF_PIN_NAME == S1}]
set_false_path -through $clipBufgCtrlCePins


set_false_path -from [get_clocks AdcBitClkDiv] -to [get_clocks AdcBitClkDivLv]
set_false_path -from [get_clocks AdcBitClkDivLv] -to [get_clocks AdcBitClkDiv]
set_false_path -from [get_clocks AdcBitClkDivLv] -to [get_clocks Clk40]
set_false_path -from [get_clocks AdcBitClkDiv] -to [get_clocks AdcBitClk]

#konieczne dla synchronizacji zegarow
set_false_path -from [get_clocks AdcBitClkRaw] -to [get_clocks AdcBitClk]

#jesli AdcBitClkRaw wprost uzyj tego
set_false_path -from [get_clocks AdcBitClkDiv] -to [get_clocks Clk40]
#jesli zanegowane to takze tego
set_false_path -from [get_clocks Clk40] -to [get_clocks AdcBitClkDiv]