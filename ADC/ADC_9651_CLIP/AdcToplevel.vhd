----------------------------------------------------------------------------------------------
-- Copyright 2012, Xilinx, Inc. All rights reserved.
-- This file contains confidential and proprietary information of Xilinx, Inc. and is
-- protected under U.S. and international copyright and other intellectual property laws.
-----------------------------------------------------------------------------------------------
--
-- Disclaimer:
--		This disclaimer is not a license and does not grant any rights to the materials
--		distributed herewith. Except as otherwise provided in a valid license issued to you
--		by Xilinx, and to the maximum extent permitted by applicable law: (1) THESE MATERIALS
--		ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL
--		WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED
--		TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR
--		PURPOSE; and (2) Xilinx shall not be liable (whether in contract or tort, including
--		negligence, or under any other theory of liability) for any loss or damage of any
--		kind or nature related to, arising under or in connection with these materials,
--		including for any direct, or any indirect, special, incidental, or consequential
--		loss or damage (including loss of data, profits, goodwill, or any type of loss or
--		damage suffered as a result of any action brought by a third party) even if such
--		damage or loss was reasonably foreseeable or Xilinx had been advised of the
--		possibility of the same.
--
-- CRITICAL APPLICATIONS
--		Xilinx products are not designed or intended to be fail-safe, or for use in any
--		application requiring fail-safe performance, such as life-support or safety devices
--		or systems, Class III medical devices, nuclear facilities, applications related to
--		the deployment of airbags, or any other applications that could lead to death,
--		personal injury, or severe property or environmental damage (individually and
--		collectively, "Critical Applications"). Customer assumes the sole risk and
--		liability of any use of Xilinx products in Critical Applications, subject only to
--		applicable laws and regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.
--
--		Contact:    e-mail  hotline@xilinx.com        phone   + 1 800 255 7778
--   ____  ____
--  /   /\/   /
-- /___/  \  / 			Vendor:              Xilinx Inc.
-- \   \   \/ 			Version:             V0.02
--  \   \
--  /   /        		Filename:            AdcToplevel.vhd
-- /___/   /\    		Date Created:        Nov 2007
-- \   \  /  \          Date Last Modified:  29 Jan 2018
--  \___\/\___\
--
-- Device:          7-Series
-- Author:          defossez
-- Entity Name:     AdcToplevel
-- Purpose:         Top level for an interface between a Virtex-6 FPGA and ADS6245
-- Tools:           Vivado_2017.3 or later
-- Limitations:     none
--
-- Revision History:
-----------------------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
-----------------------------------------------------------------------------------------------
--
library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.std_logic_UNSIGNED.all;
library UNISIM;
	use UNISIM.VCOMPONENTS.all;
library work;
	use work.all;
-----------------------------------------------------------------------------------------------
-- Entity pin description
-----------------------------------------------------------------------------------------------
--      GENERICS
-- C_AdcChnls           -- ADC Channels available in a package.
-- C_AdcBits            -- Value can be 12, 14, or 16 (14 is means 14-bit buried in 16-bit)
-- C_AdcWireInt         -- 0 = 1-wire, 1 = 2-wire
-- C_FrmPattern         -- Pattern to lock the frame to.
--
-- A 14 or 16 bit ADC in 2-wire mode has a 8-bit frame pattern. The C_FrmPattern parameter
-- must be set to:  C_FrmPattern ==> "0000000011110000".
-- The frame pattern does not use the upper 8-bits of the pattern because the frame Pattern
-- is only 8-bit wide.
-- A 14 or 16 bit ADC in 1-wire mode has a 16-bit frame pattern. The C_FrmPattern parameter
-- must be set to:  C_FrmPattern ==> "1111111100000000".
-- The frame pattern uses the full 16-bit of the pattern.
-- The same applies for a 12-bit ADC device.
--      C_FrmPattern        : string    := "111111000000";      -- 1-wire, 12 bit.
--      C_FrmPattern        : string    := "000000111000";      -- 2-wire, 12 bit.
--
-- C_StatTaps           -- Number of taps the IDELAY starts from (Middle of the Tap chain).
-- C_IdelayCtrlLoc      -- Hard location of the IDELAYCTRL component.
--      PORTS
-- DATA_n           -- I -- ADC data input signals from the ADC device.
-- DATA_p           -- I --
-- DCLK_n, DCLK_p   -- I -- High speed clock from the ADC device.
-- FCLK_n, FCLK_p   -- I -- Word or frame clock from the ADC device.
-- SysRefClk        -- I -- Reference clock for IDELAYCTRL (200 MHz).
-- AdcIntrfcRst     -- I -- Reset for the interface from the application.
-- AdcIntrfcEna     -- I -- Enable signal for the interface from the application.
-- AdcReSync        -- I -- Signal to restart the resync process.
-- AdcFrmSyncWrn    -- O -- Warning from the sync logic, alignment is not possible
-- AdcBitClkAlgnWrn -- O -- Status signal. BitClock adjusted.
-- AdcBitClkInvrtd  -- O -- Bit clock state, rising or falling
-- AdcBitClkDone    -- O -- Bit clock alignment done
-- AdcIdlyCtrlRdy   -- O -- IDELAYCTRL ready
-- AdcFrmDataOut    -- O -- Frame clock pattern as data (normally not used)
-----------------------------------------------------------------------------------------------
entity AdcToplevel is
	generic (
        C_AdcChnls              : integer := 8;     -- Number of ADC in a package       -- OK(8)
        C_AdcWireInt            : integer := 1;     -- 2 = 2-wire, 1 = 1-wire interface -- OK(1)
        C_AdcBits               : integer := 12;                                        -- OK(12)
        C_StatTaps              : integer := 16;                                        -- OK(16)
        C_AdcUseIdlyCtrl        : integer := 1;      -- 0 = No, 1 = Yes                 -- OK(1)
        C_AdcIdlyCtrlLoc	   : string  := "IDELAYCTRL_X1Y3";
        C_FrmPattern            : string  := "111111000000"  -- "1111111100000000"      -- OK(111111000000)
	);
    port (
		DCLK_p	           : in std_logic;
		DCLK_n	           : in std_logic;  -- Not used.
		FCLK_p	           : in std_logic;
		FCLK_n	           : in std_logic;
		DATA_p	           : in std_logic_vector((C_AdcChnls*C_AdcWireInt)-1 downto 0);
		DATA_n	           : in std_logic_vector((C_AdcChnls*C_AdcWireInt)-1 downto 0);
        -- application connections
        SysRefClk          : in std_logic;		-- 200 MHz for IODELAYCTRL from application
--        CLKx2_lv_out       : in std_logic;
        AdcIntrfcRstLv     : in std_logic;
        AdcFrmSyncWrn      : out std_logic;
        AdcBitClkAlgnWrn   : out std_logic;
		AdcBitClkInvrtd    : out std_logic;
		AdcBitClkDone      : out std_logic;
		AdcIdlyCtrlRdy     : out std_logic;
        
        glReset             : in std_logic;
        IntClkDivOutLv      : out std_logic;
        IntClkDivInLv       : in std_logic;
        RegClkOutLv         : out std_logic;
        AdcDataOutLv        : out std_logic_vector((16*(C_AdcChnls*C_AdcWireInt))-1 downto 0);
        AdcIntrfcEnaLv      : in std_logic;
        AdcReSyncLv         : in std_logic;
        RegClkLvInvLv       : in std_logic; 
        AdcFrmDataOutLv     : out std_logic_vector(15 downto 0)
    );
end AdcToplevel;
-----------------------------------------------------------------------------------------------
-- Arcitecture section
-----------------------------------------------------------------------------------------------
architecture RTL of AdcToplevel  is
-----------------------------------------------------------------------------------------------
-- Constants, Signals and Attributes Declarations
-----------------------------------------------------------------------------------------------
-- Constants
constant Low : std_logic := '0';
constant High : std_logic := '1';
-- Signals
signal IntIdlyCtrlRdy		: std_logic;
signal IntRst				: std_logic;
signal IntEna_d				: std_logic;
signal IntEna				: std_logic;
--
signal IntBitClkDone		: std_logic;
signal IntClk				: std_logic;
signal IntClkDiv			: std_logic;
signal IntClkBitSlip_p		: std_logic;
signal IntClkBitSlip_n		: std_logic;
signal IntFrmClk            : std_logic;

signal IntDataOut   	    : std_logic_vector((32*((C_AdcChnls/2)*C_AdcWireInt))-1 downto 0);
signal AdcFrmDataOut        : std_logic_vector(15 downto 0);
signal AdcIntrfcRst         : std_logic;
signal AdcIntrfcEna         : std_logic;
signal AdcReSync            : std_logic;
signal RegClkLvInv          : std_logic;

signal IntBitClkInvrtdIn      : std_logic;
signal IntBitClkInvrtdOut     : std_logic;

signal IntClkReSyncOut          : std_logic;
-- Attributes
attribute LOC : string;
attribute KEEP_HIERARCHY : string;
    attribute KEEP_HIERARCHY of RTL : architecture is "YES";
-----------------------------------------------------------------------------------------------
--
begin
-----------------------------------------------------------------------------------------------
-- IDELAYCTRL
-- An IDELAYCTRL component must be used per IO-bank. Normally a ADC port fits a whole
-- IO-Bank. The number of IDELAYCTRL components should thus fit with the number of ADC port.
-- In case of this test design, two ADC ports fit into one IO-Bank, thus only one IDLEAYCTRL
-- component is needed.
-- Don not forget to hook the outputs of the IDELAYCTRL components correctly to the reset and
-- enable for each ADC block.
-- Don not forget to LOC the IDELAYCTRL components down.
-----------------------------------------------------------------------------------------------
Gen_0 : if C_AdcUseIdlyCtrl = 0 generate
	AdcIdlyCtrlRdy <= High;
end generate Gen_0;
Gen_1 : if C_AdcUseIdlyCtrl = 1 generate
	attribute LOC of AdcToplevel_I_IdlyCtrl_0 : label is C_AdcIdlyCtrlLoc;
begin
	AdcToplevel_I_IdlyCtrl_0 : IDELAYCTRL
		port map (REFCLK => SysRefClk, RST => AdcIntrfcRst, RDY => AdcIdlyCtrlRdy);
end generate Gen_1;
-- IntRst and IntEna are the reset and enable signals to be used in the interface.
-- they are generated from the incoming system enable and reset.
AdcToplevel_I_Fdpe_Rst : FDPE
	generic map (INIT => '1')
	port map (C => IntClkDiv, CE => High, PRE => AdcIntrfcRst, D => Low, Q => IntRst);
AdcToplevel_I_Fdce_Ena_0 : FDCE
	generic map (INIT => '0')
	port map (C => IntClkDiv, CE => AdcIntrfcEna, CLR => IntRst, D => High, Q => IntEna_d);
AdcToplevel_I_Fdce_Ena_1 : FDCE
	generic map (INIT => '0')
	port map (C => IntClkDiv, CE => High, CLR => IntRst, D => IntEna_d, Q => IntEna);
	
	
AppsRstEna_I_LocalRstEna : entity work.LocalRstEna
    generic map (
        C_LocalUseRstDly => 1,
        C_LocalRstDly => 6,
        C_LocalEnaDly => 8
    )
    port map (
        ClkIn     => IntClkDiv,     -- in
        Ena       => AdcIntrfcEnaLv,   -- in
        Rst       => AdcIntrfcRstLv,    -- in
        RstOut    => AdcIntrfcRst,    -- out
        EnaOut    => AdcIntrfcEna     -- out
    );
-----------------------------------------------------------------------------------------------
-- C_AdcChnls		= c
-- C_AdcWireInt		= w
-- C_AdcBits		= b
-----------------------------------------------------------------------------------------------
-- BIT CLOCK
-- IntClk and IntClkDiv are the clock to be used in the interface.
-----------------------------------------------------------------------------------------------
-- There is no IBUFGDS used on this level of the design.
-- The IBUFGDS can be found in the AdcIo level or in the AdcToplevel_Toplevel.
-- That is this the reason why the DCLK_n is not used here.
-- At the AdcIo level the DCLK_n output is connected to GND.
-- Vivado complains about this with this message:
--    [Synth 8-3331] design AdcToplevel has unconnected port DCLK_n
AdcToplevel_I_AdcClock : entity work.AdcClock
generic map (
        C_AdcBits       => C_AdcBits,       -- integer
        C_StatTaps      => C_StatTaps       -- integer
    )
port map (
	BitClk				=> DCLK_p,			-- in
	FrmClk              => IntFrmClk,
	BitClkRst			=> IntRst,			-- in
	BitClkEna			=> IntEna,			-- in
	BitClkReSync		=> AdcReSync,		-- in
	BitClk_MonClkOut	=> IntClk,			-- out	-->--|---->----
	BitClk_MonClkIn		=> IntClk,			-- in	--<--|
	BitClk_RefClkOut	=> IntClkDiv,		-- out	-->----|-->----
	BitClk_RefClkIn		=> IntClkDiv,		-- in	--<----|
	BitClk_RefClkOutLV  => IntClkDivOutLv,  -- out
	BitClkAlignWarn 	=> AdcBitClkAlgnWrn,-- out
	BitClkInvrtd		=> IntBitClkInvrtdIn,	-- out
	BitClkDone 			=> IntBitClkDone,	-- out Enables the AdcFrame block.
    BitClkReSyncOut     => IntClkReSyncOut
);

IntBitClkInvrtdOut <= IntBitClkInvrtdIn xor RegClkLvInv;
AdcBitClkDone <= IntBitClkDone;
AdcBitClkInvrtd <= IntBitClkInvrtdOut;
-----------------------------------------------------------------------------------------------
-- WORD / FRAME CLOCK
-----------------------------------------------------------------------------------------------
AdcToplevel_I_AdcFrame : entity work.AdcFrame
generic map (
	C_AdcBits			=> C_AdcBits,		-- integer;
	C_AdcWireInt		=> C_AdcWireInt,	-- integer;
	C_FrmPattern		=> C_FrmPattern  	-- string -- 1 or 2-wire, 12 or 16(14)-bit
)
port map (
	FrmClk_n		=> FCLK_n,			-- in input n from IBUFDS_DIFF_OUT
	FrmClk_p		=> FCLK_p,			-- in input p from IBUFDS_DIFF_OUT
	FrmClkOut       => IntFrmClk,
	FrmClkEna		=> IntEna,			-- in
	FrmClk			=> IntClk,			-- in
	FrmClkDiv		=> IntClkDiv,		-- in
	FrmClkDone		=> IntBitClkDone,	-- in From AdcClock done.
	FrmClkReSync	=> IntClkReSyncOut,		-- in
	FrmClkInvrtd    => IntBitClkInvrtdOut,
	FrmClkBitSlip_p	=> IntClkBitSlip_p,	-- out
	FrmClkBitSlip_n	=> IntClkBitSlip_n,	-- out
	FrmClkDat		=> AdcFrmDataOut,		-- out
	FrmClkSyncWarn	=> AdcFrmSyncWrn		-- out
);
-----------------------------------------------------------------------------------------------
-- DATA INPUTS
-- Default the interface is set in BYTE and MSB first mode.
-- This is coded in the AdcData level and can be modified if wanted.
-- Enable the generics and all selection possibilities are available.
-----------------------------------------------------------------------------------------------
Gen_2 : for cw in (C_AdcChnls*C_AdcWireInt)-1 downto 0 generate
	AdcToplevel_I_AdcData : entity work.AdcData
	generic map (
		C_AdcBits		=> C_AdcBits,		-- Can be 12, 14 or 16
		C_AdcWireInt	=> C_AdcWireInt		-- 1 = 1-wire, 2 = 2-wire.
	)
	port map (
		DatD_n			=> DATA_n(cw),		-- in
		DatD_p			=> DATA_p(cw),		-- in
		DatClk			=> IntClk,				-- in
		DatClkDiv		=> IntClkDiv,			-- in
		DatEna			=> IntEna,				-- in
		DatDone			=> IntBitClkDone,		-- in
		DatBitSlip_p	=> IntClkBitSlip_p,		-- in
		DatBitSlip_n	=> IntClkBitSlip_n,		-- in
        DatInvrtd       => IntBitClkInvrtdOut,
		DatReSync		=> IntClkReSyncOut,	-- in
		DatOut			=> IntDataOut((16*(cw+1))-1 downto (16*(cw+1))-16)
	);
end generate Gen_2;

  DiagramClockXingx: entity work.DiagramClockXing (RTL)
    generic map (
        C_AdcChnls       => C_AdcChnls,
        C_AdcWireInt     => C_AdcWireInt
    )         
    port map(
    glReset             => Low,
    AdcIntrfcRst        => IntRst,
--    BitClk_RefClkOutLV  => CLKx2_lv_out,
    BitClk_RefClkOutLV  => IntClkDivInLv,
    AdcDataOutLv        => AdcDataOutLv,
    AdcReSyncLv         => AdcReSyncLv,
    RegClkLvInvLv       => RegClkLvInvLv, 
    AdcFrmDataOutLv     => AdcFrmDataOutLv,
    AdcDataOut          => IntDataOut,
    AdcIntrfcEna        => open,
    AdcReSync           => AdcReSync,       
    RegClkLvInv         => RegClkLvInv,  
    AdcFrmDataOut       => AdcFrmDataOut    
  );

end RTL;
