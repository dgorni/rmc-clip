-------------------------------------------------------------------------------
--
-- File: DiagramClockXing.vhd
-- Date: 9 October 2015
--
-------------------------------------------------------------------------------
-- Copyright (c) 2010 National Instruments Corporation.
--
-- Use of this file is subject to the Software License Agreement provided with
-- the NI FlexRIO Adapter Module Support software and, without limiting any of
-- the provisions in that license, modifying or distributing this file is
-- prohibited.
--
-- Adding to, deleting from, or otherwise modifying the contents of this file
-- may cause the software to malfunction.
-------------------------------------------------------------------------------
--
-- In this CLIP, the majority of the logic is clocked with the divided regional
-- clock domain. This regional clock is then passed into a global clock, which
-- is then passed into Labview diagram. This module is used to cross data
-- between the CLIP and the diagram.
--
-- There is a bug in Vivado where this crossing is not analysed properly. This
-- manifests itself as a hold violation crossing signals from the CLIP to
-- Labview. To improve this hold time, data is first passed from the CLIP into
-- a register on the global clock's falling edge, then clocked onto the global
-- clock's rising edge. This improves timing by providing a full half clock
-- period of hold.
--
-- All signals from Labview to the CLIP are first passed into a register in the
-- global clock domain. The global clock arrives later at registers than the
-- regional clock (due to the propagation of the clock from the BUFR to BUFG),
-- so this extra pipeline register isolates the diagram from this crossing and
-- improves timing.
--
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
library work;

entity DiagramClockXing is
    generic (
        C_AdcChnls              : integer := 8;     -- Number of ADC in a package       -- OK(8)
        C_AdcWireInt            : integer := 1     -- 2 = 2-wire, 1 = 1-wire interface -- OK(1)
    );             
  port (
    -- Resets
    glReset                 : in std_logic;
    AdcIntrfcRst            : in std_logic;
    -- Labview clocks
    BitClk_RefClkOutLV      : in std_logic;
    -- Labview data
    AdcDataOutLv            : out std_logic_vector((32*((C_AdcChnls/2)*C_AdcWireInt))-1 downto 0);
    AdcReSyncLv             : in std_logic;  
    RegClkLvInvLv           : in std_logic;        
    AdcFrmDataOutLv         : out std_logic_vector(15 downto 0);
    -- Local data
    AdcDataOut              : in std_logic_vector((32*((C_AdcChnls/2)*C_AdcWireInt))-1 downto 0);
    AdcIntrfcEna            : out std_logic;
    AdcReSync               : out std_logic;        
    RegClkLvInv             : out std_logic; 
    AdcFrmDataOut           : in  std_logic_vector(15 downto 0)
  );
end DiagramClockXing;

architecture RTL of DiagramClockXing is

    signal AdcDataOutFe            : std_logic_vector((32*((C_AdcChnls/2)*C_AdcWireInt))-1 downto 0);
    signal AdcFrmDataOutFe         : std_logic_vector(15 downto 0);
    
begin

  --------------------------------------------------------
  -- Acquisition Data
  --------------------------------------------------------
  -- Sync over to Lv clock, use falling edge to allow for more hold
  AcqDataSyncFe : process (glReset, BitClk_RefClkOutLV)
  begin
    if glReset = '1' then
      AdcDataOutFe <=  (others => '0');
      AdcFrmDataOutFe <=  (others => '0');
    elsif falling_edge(BitClk_RefClkOutLV) then
      if AdcIntrfcRst = '1' then
        AdcDataOutFe <= (others => '0');
        AdcFrmDataOutFe <=  (others => '0');
      else
        AdcDataOutFe <= AdcDataOut;
        AdcFrmDataOutFe <= AdcFrmDataOut;
      end if;
    end if;
  end process AcqDataSyncFe;
  
  -- Now pass data over to rising edge for LV
  AcqDataSyncRe : process (glReset, BitClk_RefClkOutLV)
  begin
    if glReset = '1' then
      AdcDataOutLv <= (others => '0');
      AdcFrmDataOutLv <= (others => '0');
    elsif rising_edge(BitClk_RefClkOutLV) then
      if AdcIntrfcRst = '1' then
        AdcDataOutLv <= (others => '0');
        AdcFrmDataOutLv <= (others => '0');
      else
        AdcDataOutLv <= AdcDataOutFe;
        AdcFrmDataOutLv <= AdcFrmDataOutFe;
      end if;
    end if;
  end process AcqDataSyncRe;
  
  --------------------------------------------------------
  -- LV to CLIP controls
  --------------------------------------------------------
--  AcqCtrlSync : process (glReset, BitClk_RefClkOutLV)
--  begin
--    if glReset = '1' then
--      AdcIntrfcEna   <= '0';
--      AdcReSync <= '0';
--    elsif rising_edge(BitClk_RefClkOutLV) then
--      if AdcIntrfcRst = '1' then
--      AdcIntrfcEna   <= '0';
--      AdcReSync <= '0';
--      else
--      AdcIntrfcEna   <= AdcIntrfcEnaLv;
--      AdcReSync <= AdcReSyncLv;
--      end if;
--    end if;
--  end process AcqCtrlSync;

      AdcReSync <= AdcReSyncLv;
      RegClkLvInv <= RegClkLvInvLv;
  
end RTL;
