
#################################################################
# DIO_00
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[0]}]
set_property SLEW Slow [get_ports {aDio[0]}]
set_property DRIVE 8 [get_ports {aDio[0]}]

#################################################################
# DIO_01
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[1]}]
set_property SLEW Slow [get_ports {aDio[1]}]
set_property DRIVE 8 [get_ports {aDio[1]}]

#################################################################
# DIO_02
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[2]}]
set_property SLEW Slow [get_ports {aDio[2]}]
set_property DRIVE 8 [get_ports {aDio[2]}]

#################################################################
# DIO_03
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[3]}]
set_property SLEW Slow [get_ports {aDio[3]}]
set_property DRIVE 8 [get_ports {aDio[3]}]

#################################################################
# DIO_04
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[4]}]
set_property SLEW Slow [get_ports {aDio[4]}]
set_property DRIVE 8 [get_ports {aDio[4]}]

#################################################################
# DIO_05
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[5]}]
set_property SLEW Slow [get_ports {aDio[5]}]
set_property DRIVE 8 [get_ports {aDio[5]}]

#################################################################
# DIO_06
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[6]}]
set_property SLEW Slow [get_ports {aDio[6]}]
set_property DRIVE 8 [get_ports {aDio[6]}]

#################################################################
# DIO_07
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[7]}]
set_property SLEW Slow [get_ports {aDio[7]}]
set_property DRIVE 8 [get_ports {aDio[7]}]

#################################################################
# DIO_08
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[8]}]
set_property SLEW Slow [get_ports {aDio[8]}]
set_property DRIVE 8 [get_ports {aDio[8]}]

#################################################################
# DIO_09
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[9]}]
set_property SLEW Slow [get_ports {aDio[9]}]
set_property DRIVE 8 [get_ports {aDio[9]}]

#################################################################
# DIO_10
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[10]}]
set_property SLEW Slow [get_ports {aDio[10]}]
set_property DRIVE 8 [get_ports {aDio[10]}]

#################################################################
# DIO_11
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[11]}]
set_property SLEW Slow [get_ports {aDio[11]}]
set_property DRIVE 8 [get_ports {aDio[11]}]

#################################################################
# DIO_12
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[12]}]
set_property SLEW Slow [get_ports {aDio[12]}]
set_property DRIVE 8 [get_ports {aDio[12]}]

#################################################################
# DIO_13
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[13]}]
set_property SLEW Slow [get_ports {aDio[13]}]
set_property DRIVE 8 [get_ports {aDio[13]}]

#################################################################
# DIO_14
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[14]}]
set_property SLEW Slow [get_ports {aDio[14]}]
set_property DRIVE 8 [get_ports {aDio[14]}]

#################################################################
# DIO_15
#################################################################
set_property IOSTANDARD LVCMOS33 [get_ports {aDio[15]}]
set_property SLEW Slow [get_ports {aDio[15]}]
set_property DRIVE 8 [get_ports {aDio[15]}]

#################################################################
# DIO_16
#################################################################
set_property IOSTANDARD LVDS_25 [get_ports {aDio[16]}]
set_property DIFF_TERM TRUE [get_ports {aDio[16]}]

#################################################################
# DIO_17
#################################################################
set_property IOSTANDARD LVDS_25 [get_ports {aDio[17]}]

#################################################################
# DIO_18
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[18]}]
set_property SLEW Slow [get_ports {aDio[18]}]
set_property DRIVE 8 [get_ports {aDio[18]}]

#################################################################
# DIO_18_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[18]}]
set_property SLEW Slow [get_ports {aDio_n[18]}]
set_property DRIVE 8 [get_ports {aDio_n[18]}]

#################################################################
# DIO_19
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[19]}]
set_property SLEW Slow [get_ports {aDio[19]}]
set_property DRIVE 8 [get_ports {aDio[19]}]

#################################################################
# DIO_19_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[19]}]
set_property SLEW Slow [get_ports {aDio_n[19]}]
set_property DRIVE 8 [get_ports {aDio_n[19]}]

#################################################################
# DIO_20
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[20]}]
set_property SLEW Slow [get_ports {aDio[20]}]
set_property DRIVE 8 [get_ports {aDio[20]}]

#################################################################
# DIO_20_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[20]}]
set_property SLEW Slow [get_ports {aDio_n[20]}]
set_property DRIVE 8 [get_ports {aDio_n[20]}]

#################################################################
# DIO_21
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[21]}]
set_property SLEW Slow [get_ports {aDio[21]}]
set_property DRIVE 8 [get_ports {aDio[21]}]

#################################################################
# DIO_21_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[21]}]
set_property SLEW Slow [get_ports {aDio_n[21]}]
set_property DRIVE 8 [get_ports {aDio_n[21]}]

#################################################################
# DIO_22
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[22]}]
set_property SLEW Slow [get_ports {aDio[22]}]
set_property DRIVE 8 [get_ports {aDio[22]}]

#################################################################
# DIO_22_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[22]}]
set_property SLEW Slow [get_ports {aDio_n[22]}]
set_property DRIVE 8 [get_ports {aDio_n[22]}]

#################################################################
# DIO_23
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[23]}]
set_property SLEW Slow [get_ports {aDio[23]}]
set_property DRIVE 8 [get_ports {aDio[23]}]

#################################################################
# DIO_23_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[23]}]
set_property SLEW Slow [get_ports {aDio_n[23]}]
set_property DRIVE 8 [get_ports {aDio_n[23]}]

#################################################################
# DIO_24
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[24]}]
set_property SLEW Slow [get_ports {aDio[24]}]
set_property DRIVE 8 [get_ports {aDio[24]}]

#################################################################
# DIO_24_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[24]}]
set_property SLEW Slow [get_ports {aDio_n[24]}]
set_property DRIVE 8 [get_ports {aDio_n[24]}]

#################################################################
# DIO_25
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[25]}]
set_property SLEW Slow [get_ports {aDio[25]}]
set_property DRIVE 8 [get_ports {aDio[25]}]

#################################################################
# DIO_25_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[25]}]
set_property SLEW Slow [get_ports {aDio_n[25]}]
set_property DRIVE 8 [get_ports {aDio_n[25]}]

#################################################################
# DIO_26
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[26]}]
set_property SLEW Slow [get_ports {aDio[26]}]
set_property DRIVE 8 [get_ports {aDio[26]}]

#################################################################
# DIO_26_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[26]}]
set_property SLEW Slow [get_ports {aDio_n[26]}]
set_property DRIVE 8 [get_ports {aDio_n[26]}]

#################################################################
# DIO_27
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[27]}]
set_property SLEW Slow [get_ports {aDio[27]}]
set_property DRIVE 8 [get_ports {aDio[27]}]

#################################################################
# DIO_27_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[27]}]
set_property SLEW Slow [get_ports {aDio_n[27]}]
set_property DRIVE 8 [get_ports {aDio_n[27]}]

#################################################################
# DIO_28
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[28]}]
set_property SLEW Slow [get_ports {aDio[28]}]
set_property DRIVE 8 [get_ports {aDio[28]}]

#################################################################
# DIO_28_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[28]}]
set_property SLEW Slow [get_ports {aDio_n[28]}]
set_property DRIVE 8 [get_ports {aDio_n[28]}]

#################################################################
# DIO_29
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[29]}]
set_property SLEW Slow [get_ports {aDio[29]}]
set_property DRIVE 8 [get_ports {aDio[29]}]

#################################################################
# DIO_29_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[29]}]
set_property SLEW Slow [get_ports {aDio_n[29]}]
set_property DRIVE 8 [get_ports {aDio_n[29]}]

#################################################################
# DIO_30
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[30]}]
set_property SLEW Slow [get_ports {aDio[30]}]
set_property DRIVE 8 [get_ports {aDio[30]}]

#################################################################
# DIO_30_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[30]}]
set_property SLEW Slow [get_ports {aDio_n[30]}]
set_property DRIVE 8 [get_ports {aDio_n[30]}]

#################################################################
# DIO_31
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[31]}]
set_property SLEW Slow [get_ports {aDio[31]}]
set_property DRIVE 8 [get_ports {aDio[31]}]

#################################################################
# DIO_31_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[31]}]
set_property SLEW Slow [get_ports {aDio_n[31]}]
set_property DRIVE 8 [get_ports {aDio_n[31]}]

#################################################################
# DIO_32
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[32]}]
set_property SLEW Slow [get_ports {aDio[32]}]
set_property DRIVE 8 [get_ports {aDio[32]}]

#################################################################
# DIO_32_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[32]}]
set_property SLEW Slow [get_ports {aDio_n[32]}]
set_property DRIVE 8 [get_ports {aDio_n[32]}]

#################################################################
# DIO_33
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[33]}]
set_property SLEW Slow [get_ports {aDio[33]}]
set_property DRIVE 8 [get_ports {aDio[33]}]

#################################################################
# DIO_33_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[33]}]
set_property SLEW Slow [get_ports {aDio_n[33]}]
set_property DRIVE 8 [get_ports {aDio_n[33]}]

#################################################################
# DIO_34
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[34]}]
set_property SLEW Slow [get_ports {aDio[34]}]
set_property DRIVE 8 [get_ports {aDio[34]}]

#################################################################
# DIO_34_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[34]}]
set_property SLEW Slow [get_ports {aDio_n[34]}]
set_property DRIVE 8 [get_ports {aDio_n[34]}]

#################################################################
# DIO_35
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[35]}]
set_property SLEW Slow [get_ports {aDio[35]}]
set_property DRIVE 8 [get_ports {aDio[35]}]

#################################################################
# DIO_35_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[35]}]
set_property SLEW Slow [get_ports {aDio_n[35]}]
set_property DRIVE 8 [get_ports {aDio_n[35]}]

#################################################################
# DIO_36
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[36]}]
set_property SLEW Slow [get_ports {aDio[36]}]
set_property DRIVE 8 [get_ports {aDio[36]}]

#################################################################
# DIO_36_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[36]}]
set_property SLEW Slow [get_ports {aDio_n[36]}]
set_property DRIVE 8 [get_ports {aDio_n[36]}]

#################################################################
# DIO_37
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[37]}]
set_property SLEW Slow [get_ports {aDio[37]}]
set_property DRIVE 8 [get_ports {aDio[37]}]

#################################################################
# DIO_37_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[37]}]
set_property SLEW Slow [get_ports {aDio_n[37]}]
set_property DRIVE 8 [get_ports {aDio_n[37]}]

#################################################################
# DIO_38
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[38]}]
set_property SLEW Slow [get_ports {aDio[38]}]
set_property DRIVE 8 [get_ports {aDio[38]}]

#################################################################
# DIO_38_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[38]}]
set_property SLEW Slow [get_ports {aDio_n[38]}]
set_property DRIVE 8 [get_ports {aDio_n[38]}]

#################################################################
# DIO_39
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[39]}]
set_property SLEW Slow [get_ports {aDio[39]}]
set_property DRIVE 8 [get_ports {aDio[39]}]

#################################################################
# DIO_39_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[39]}]
set_property SLEW Slow [get_ports {aDio_n[39]}]
set_property DRIVE 8 [get_ports {aDio_n[39]}]

#################################################################
# DIO_40
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[40]}]
set_property SLEW Slow [get_ports {aDio[40]}]
set_property DRIVE 8 [get_ports {aDio[40]}]

#################################################################
# DIO_40_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[40]}]
set_property SLEW Slow [get_ports {aDio_n[40]}]
set_property DRIVE 8 [get_ports {aDio_n[40]}]

#################################################################
# DIO_41
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[41]}]
set_property SLEW Slow [get_ports {aDio[41]}]
set_property DRIVE 8 [get_ports {aDio[41]}]

#################################################################
# DIO_41_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[41]}]
set_property SLEW Slow [get_ports {aDio_n[41]}]
set_property DRIVE 8 [get_ports {aDio_n[41]}]

#################################################################
# DIO_42
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[42]}]
set_property SLEW Slow [get_ports {aDio[42]}]
set_property DRIVE 8 [get_ports {aDio[42]}]

#################################################################
# DIO_42_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[42]}]
set_property SLEW Slow [get_ports {aDio_n[42]}]
set_property DRIVE 8 [get_ports {aDio_n[42]}]

#################################################################
# DIO_43
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[43]}]
set_property SLEW Slow [get_ports {aDio[43]}]
set_property DRIVE 8 [get_ports {aDio[43]}]

#################################################################
# DIO_43_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[43]}]
set_property SLEW Slow [get_ports {aDio_n[43]}]
set_property DRIVE 8 [get_ports {aDio_n[43]}]

#################################################################
# DIO_44
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[44]}]
set_property SLEW Slow [get_ports {aDio[44]}]
set_property DRIVE 8 [get_ports {aDio[44]}]

#################################################################
# DIO_44_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[44]}]
set_property SLEW Slow [get_ports {aDio_n[44]}]
set_property DRIVE 8 [get_ports {aDio_n[44]}]

#################################################################
# DIO_45
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[45]}]
set_property SLEW Slow [get_ports {aDio[45]}]
set_property DRIVE 8 [get_ports {aDio[45]}]

#################################################################
# DIO_45_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[45]}]
set_property SLEW Slow [get_ports {aDio_n[45]}]
set_property DRIVE 8 [get_ports {aDio_n[45]}]

#################################################################
# DIO_46
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[46]}]
set_property SLEW Slow [get_ports {aDio[46]}]
set_property DRIVE 8 [get_ports {aDio[46]}]

#################################################################
# DIO_46_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[46]}]
set_property SLEW Slow [get_ports {aDio_n[46]}]
set_property DRIVE 8 [get_ports {aDio_n[46]}]

#################################################################
# DIO_47
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[47]}]
set_property SLEW Slow [get_ports {aDio[47]}]
set_property DRIVE 8 [get_ports {aDio[47]}]

#################################################################
# DIO_47_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[47]}]
set_property SLEW Slow [get_ports {aDio_n[47]}]
set_property DRIVE 8 [get_ports {aDio_n[47]}]

#################################################################
# DIO_48
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[48]}]
set_property SLEW Slow [get_ports {aDio[48]}]
set_property DRIVE 8 [get_ports {aDio[48]}]

#################################################################
# DIO_48_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[48]}]
set_property SLEW Slow [get_ports {aDio_n[48]}]
set_property DRIVE 8 [get_ports {aDio_n[48]}]

#################################################################
# DIO_49
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[49]}]
set_property SLEW Slow [get_ports {aDio[49]}]
set_property DRIVE 8 [get_ports {aDio[49]}]

#################################################################
# DIO_49_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[49]}]
set_property SLEW Slow [get_ports {aDio_n[49]}]
set_property DRIVE 8 [get_ports {aDio_n[49]}]

#################################################################
# DIO_50
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[50]}]
set_property SLEW Slow [get_ports {aDio[50]}]
set_property DRIVE 8 [get_ports {aDio[50]}]

#################################################################
# DIO_50_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[50]}]
set_property SLEW Slow [get_ports {aDio_n[50]}]
set_property DRIVE 8 [get_ports {aDio_n[50]}]

#################################################################
# DIO_51
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[51]}]
set_property SLEW Slow [get_ports {aDio[51]}]
set_property DRIVE 8 [get_ports {aDio[51]}]

#################################################################
# DIO_51_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[51]}]
set_property SLEW Slow [get_ports {aDio_n[51]}]
set_property DRIVE 8 [get_ports {aDio_n[51]}]

#################################################################
# DIO_52
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[52]}]
set_property SLEW Slow [get_ports {aDio[52]}]
set_property DRIVE 8 [get_ports {aDio[52]}]

#################################################################
# DIO_52_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[52]}]
set_property SLEW Slow [get_ports {aDio_n[52]}]
set_property DRIVE 8 [get_ports {aDio_n[52]}]

#################################################################
# DIO_53
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[53]}]
set_property SLEW Slow [get_ports {aDio[53]}]
set_property DRIVE 8 [get_ports {aDio[53]}]

#################################################################
# DIO_53_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[53]}]
set_property SLEW Slow [get_ports {aDio_n[53]}]
set_property DRIVE 8 [get_ports {aDio_n[53]}]

#################################################################
# DIO_54
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[54]}]
set_property SLEW Slow [get_ports {aDio[54]}]
set_property DRIVE 8 [get_ports {aDio[54]}]

#################################################################
# DIO_54_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[54]}]
set_property SLEW Slow [get_ports {aDio_n[54]}]
set_property DRIVE 8 [get_ports {aDio_n[54]}]

#################################################################
# DIO_55
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[55]}]
set_property SLEW Slow [get_ports {aDio[55]}]
set_property DRIVE 8 [get_ports {aDio[55]}]

#################################################################
# DIO_55_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[55]}]
set_property SLEW Slow [get_ports {aDio_n[55]}]
set_property DRIVE 8 [get_ports {aDio_n[55]}]

#################################################################
# DIO_56
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[56]}]
set_property SLEW Slow [get_ports {aDio[56]}]
set_property DRIVE 8 [get_ports {aDio[56]}]

#################################################################
# DIO_56_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[56]}]
set_property SLEW Slow [get_ports {aDio_n[56]}]
set_property DRIVE 8 [get_ports {aDio_n[56]}]

#################################################################
# DIO_57
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[57]}]
set_property SLEW Slow [get_ports {aDio[57]}]
set_property DRIVE 8 [get_ports {aDio[57]}]

#################################################################
# DIO_57_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[57]}]
set_property SLEW Slow [get_ports {aDio_n[57]}]
set_property DRIVE 8 [get_ports {aDio_n[57]}]

#################################################################
# DIO_58
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[58]}]
set_property SLEW Slow [get_ports {aDio[58]}]
set_property DRIVE 8 [get_ports {aDio[58]}]

#################################################################
# DIO_58_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[58]}]
set_property SLEW Slow [get_ports {aDio_n[58]}]
set_property DRIVE 8 [get_ports {aDio_n[58]}]

#################################################################
# DIO_59
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[59]}]
set_property SLEW Slow [get_ports {aDio[59]}]
set_property DRIVE 8 [get_ports {aDio[59]}]

#################################################################
# DIO_59_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[59]}]
set_property SLEW Slow [get_ports {aDio_n[59]}]
set_property DRIVE 8 [get_ports {aDio_n[59]}]

#################################################################
# DIO_60
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[60]}]
set_property SLEW Slow [get_ports {aDio[60]}]
set_property DRIVE 8 [get_ports {aDio[60]}]

#################################################################
# DIO_60_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[60]}]
set_property SLEW Slow [get_ports {aDio_n[60]}]
set_property DRIVE 8 [get_ports {aDio_n[60]}]

#################################################################
# DIO_61
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[61]}]
set_property SLEW Slow [get_ports {aDio[61]}]
set_property DRIVE 8 [get_ports {aDio[61]}]

#################################################################
# DIO_61_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[61]}]
set_property SLEW Slow [get_ports {aDio_n[61]}]
set_property DRIVE 8 [get_ports {aDio_n[61]}]

#################################################################
# DIO_62
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[62]}]
set_property SLEW Slow [get_ports {aDio[62]}]
set_property DRIVE 8 [get_ports {aDio[62]}]

#################################################################
# DIO_62_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[62]}]
set_property SLEW Slow [get_ports {aDio_n[62]}]
set_property DRIVE 8 [get_ports {aDio_n[62]}]

#################################################################
# DIO_63
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[63]}]
set_property SLEW Slow [get_ports {aDio[63]}]
set_property DRIVE 8 [get_ports {aDio[63]}]

#################################################################
# DIO_63_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[63]}]
set_property SLEW Slow [get_ports {aDio_n[63]}]
set_property DRIVE 8 [get_ports {aDio_n[63]}]

#################################################################
# DIO_64
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[64]}]
set_property SLEW Slow [get_ports {aDio[64]}]
set_property DRIVE 8 [get_ports {aDio[64]}]

#################################################################
# DIO_64_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[64]}]
set_property SLEW Slow [get_ports {aDio_n[64]}]
set_property DRIVE 8 [get_ports {aDio_n[64]}]

#################################################################
# DIO_65
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[65]}]
set_property SLEW Slow [get_ports {aDio[65]}]
set_property DRIVE 8 [get_ports {aDio[65]}]

#################################################################
# DIO_65_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[65]}]
set_property SLEW Slow [get_ports {aDio_n[65]}]
set_property DRIVE 8 [get_ports {aDio_n[65]}]

#################################################################
# DIO_66
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[66]}]
set_property SLEW Slow [get_ports {aDio[66]}]
set_property DRIVE 8 [get_ports {aDio[66]}]

#################################################################
# DIO_66_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[66]}]
set_property SLEW Slow [get_ports {aDio_n[66]}]
set_property DRIVE 8 [get_ports {aDio_n[66]}]

#################################################################
# DIO_67
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[67]}]
set_property SLEW Slow [get_ports {aDio[67]}]
set_property DRIVE 8 [get_ports {aDio[67]}]

#################################################################
# DIO_67_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[67]}]
set_property SLEW Slow [get_ports {aDio_n[67]}]
set_property DRIVE 8 [get_ports {aDio_n[67]}]

#################################################################
# DIO_68
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[68]}]
set_property SLEW Slow [get_ports {aDio[68]}]
set_property DRIVE 8 [get_ports {aDio[68]}]

#################################################################
# DIO_68_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[68]}]
set_property SLEW Slow [get_ports {aDio_n[68]}]
set_property DRIVE 8 [get_ports {aDio_n[68]}]

#################################################################
# DIO_69
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[69]}]
set_property SLEW Slow [get_ports {aDio[69]}]
set_property DRIVE 8 [get_ports {aDio[69]}]

#################################################################
# DIO_69_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[69]}]
set_property SLEW Slow [get_ports {aDio_n[69]}]
set_property DRIVE 8 [get_ports {aDio_n[69]}]

#################################################################
# DIO_70
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[70]}]
set_property SLEW Slow [get_ports {aDio[70]}]
set_property DRIVE 8 [get_ports {aDio[70]}]

#################################################################
# DIO_70_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[70]}]
set_property SLEW Slow [get_ports {aDio_n[70]}]
set_property DRIVE 8 [get_ports {aDio_n[70]}]

#################################################################
# DIO_71
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[71]}]
set_property SLEW Slow [get_ports {aDio[71]}]
set_property DRIVE 8 [get_ports {aDio[71]}]

#################################################################
# DIO_71_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[71]}]
set_property SLEW Slow [get_ports {aDio_n[71]}]
set_property DRIVE 8 [get_ports {aDio_n[71]}]

#################################################################
# DIO_72
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[72]}]
set_property SLEW Slow [get_ports {aDio[72]}]
set_property DRIVE 8 [get_ports {aDio[72]}]

#################################################################
# DIO_72_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[72]}]
set_property SLEW Slow [get_ports {aDio_n[72]}]
set_property DRIVE 8 [get_ports {aDio_n[72]}]

#################################################################
# DIO_73
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[73]}]
set_property SLEW Slow [get_ports {aDio[73]}]
set_property DRIVE 8 [get_ports {aDio[73]}]

#################################################################
# DIO_73_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[73]}]
set_property SLEW Slow [get_ports {aDio_n[73]}]
set_property DRIVE 8 [get_ports {aDio_n[73]}]

#################################################################
# DIO_74
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[74]}]
set_property SLEW Slow [get_ports {aDio[74]}]
set_property DRIVE 8 [get_ports {aDio[74]}]

#################################################################
# DIO_74_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[74]}]
set_property SLEW Slow [get_ports {aDio_n[74]}]
set_property DRIVE 8 [get_ports {aDio_n[74]}]

#################################################################
# DIO_75
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[75]}]
set_property SLEW Slow [get_ports {aDio[75]}]
set_property DRIVE 8 [get_ports {aDio[75]}]

#################################################################
# DIO_75_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[75]}]
set_property SLEW Slow [get_ports {aDio_n[75]}]
set_property DRIVE 8 [get_ports {aDio_n[75]}]

#################################################################
# DIO_76
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[76]}]
set_property SLEW Slow [get_ports {aDio[76]}]
set_property DRIVE 8 [get_ports {aDio[76]}]

#################################################################
# DIO_76_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[76]}]
set_property SLEW Slow [get_ports {aDio_n[76]}]
set_property DRIVE 8 [get_ports {aDio_n[76]}]

#################################################################
# DIO_77
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[77]}]
set_property SLEW Slow [get_ports {aDio[77]}]
set_property DRIVE 8 [get_ports {aDio[77]}]

#################################################################
# DIO_77_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[77]}]
set_property SLEW Slow [get_ports {aDio_n[77]}]
set_property DRIVE 8 [get_ports {aDio_n[77]}]

#################################################################
# DIO_78
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[78]}]
set_property SLEW Slow [get_ports {aDio[78]}]
set_property DRIVE 8 [get_ports {aDio[78]}]

#################################################################
# DIO_78_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[78]}]
set_property SLEW Slow [get_ports {aDio_n[78]}]
set_property DRIVE 8 [get_ports {aDio_n[78]}]

#################################################################
# DIO_79
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[79]}]
set_property SLEW Slow [get_ports {aDio[79]}]
set_property DRIVE 8 [get_ports {aDio[79]}]

#################################################################
# DIO_79_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[79]}]
set_property SLEW Slow [get_ports {aDio_n[79]}]
set_property DRIVE 8 [get_ports {aDio_n[79]}]

#################################################################
# DIO_80
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[80]}]
set_property SLEW Slow [get_ports {aDio[80]}]
set_property DRIVE 8 [get_ports {aDio[80]}]

#################################################################
# DIO_80_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[80]}]
set_property SLEW Slow [get_ports {aDio_n[80]}]
set_property DRIVE 8 [get_ports {aDio_n[80]}]

#################################################################
# DIO_81
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[81]}]
set_property SLEW Slow [get_ports {aDio[81]}]
set_property DRIVE 8 [get_ports {aDio[81]}]

#################################################################
# DIO_81_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[81]}]
set_property SLEW Slow [get_ports {aDio_n[81]}]
set_property DRIVE 8 [get_ports {aDio_n[81]}]

#################################################################
# DIO_82
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[82]}]
set_property SLEW Slow [get_ports {aDio[82]}]
set_property DRIVE 8 [get_ports {aDio[82]}]

#################################################################
# DIO_82_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[82]}]
set_property SLEW Slow [get_ports {aDio_n[82]}]
set_property DRIVE 8 [get_ports {aDio_n[82]}]

#################################################################
# DIO_83
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[83]}]
set_property SLEW Slow [get_ports {aDio[83]}]
set_property DRIVE 8 [get_ports {aDio[83]}]

#################################################################
# DIO_83_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[83]}]
set_property SLEW Slow [get_ports {aDio_n[83]}]
set_property DRIVE 8 [get_ports {aDio_n[83]}]

#################################################################
# DIO_84
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[84]}]
set_property SLEW Slow [get_ports {aDio[84]}]
set_property DRIVE 8 [get_ports {aDio[84]}]

#################################################################
# DIO_84_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[84]}]
set_property SLEW Slow [get_ports {aDio_n[84]}]
set_property DRIVE 8 [get_ports {aDio_n[84]}]

#################################################################
# DIO_85
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[85]}]
set_property SLEW Slow [get_ports {aDio[85]}]
set_property DRIVE 8 [get_ports {aDio[85]}]

#################################################################
# DIO_85_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[85]}]
set_property SLEW Slow [get_ports {aDio_n[85]}]
set_property DRIVE 8 [get_ports {aDio_n[85]}]

#################################################################
# DIO_86
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[86]}]
set_property SLEW Slow [get_ports {aDio[86]}]
set_property DRIVE 8 [get_ports {aDio[86]}]

#################################################################
# DIO_86_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[86]}]
set_property SLEW Slow [get_ports {aDio_n[86]}]
set_property DRIVE 8 [get_ports {aDio_n[86]}]

#################################################################
# DIO_87
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio[87]}]
set_property SLEW Slow [get_ports {aDio[87]}]
set_property DRIVE 8 [get_ports {aDio[87]}]

#################################################################
# DIO_87_N
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aDio_n[87]}]
set_property SLEW Slow [get_ports {aDio_n[87]}]
set_property DRIVE 8 [get_ports {aDio_n[87]}]
