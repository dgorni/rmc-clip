--LOADING CLIPS THRU LV21f2 is NOT WORKING (USE PERVIOUS VERSION AND UPDATE)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.PkgNiUtilities.all;
use work.PkgRegPortAxiFlat.all;
use work.PkgRegPort.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity ADC_9651_CLIP is
	port(
		--- CLOCKS
		SysRefClkFromLv			: in std_logic;      -- 200 MHz for IODELAYCTRL from application
		
		AdcClkDivSelLv			: in  std_logic;
		AdcClkDivToLv			: out std_logic;
		AdcClkDivFromLv			: in std_logic;
		
		FpgaRefClkSelLv			: in  std_logic;
        FpgaRefClkFromLv		: in  std_logic;
		FpgaRefClkInToLv		: out std_logic; --send as data, not as clock
		
		AdcSampleClkToLv		: out std_logic; --clock can be only std_logic
		AdcSampleClkRstLv		: in  std_logic;
		AdcSampleClkLockLv		: out std_logic;
		AdcSampleClkEnLv		: in  std_logic;
		
		FpgaClkOutFromLv		: in  std_logic;
		FpgaClkOutEnLv			: in  std_logic;
		
		PpsFpgaClkInToLv		: out std_logic;
		--- /CLOCKS
		--- INTERFACE CONTROL
		AdcIntrfcRstLv			: in std_logic;
        AdcIntrfcEnaLv			: in std_logic;
        AdcReSyncLv				: in std_logic;
        AdcBitSlipLv			: in std_logic;
		AdcDatOrderRevLv		: in std_logic;
		AdcBitClkIdlyCtrlRdyLv	: out std_logic;
		AdcBitClkDoneLv			: out std_logic;
        AdcBitClkAlgnWrnLv		: out std_logic;
		AdcBitClkInvrtdLv		: out std_logic;
		--- /INTERFACE CONTROL
		--- DATA
			--LVDS25
		AdcDataA			: out std_logic_vector(15 downto 0);
		AdcDataB			: out std_logic_vector(15 downto 0);
		AdcDataC			: out std_logic_vector(15 downto 0);
		AdcDataD			: out std_logic_vector(15 downto 0);
		AdcDataE			: out std_logic_vector(15 downto 0);
		AdcDataF			: out std_logic_vector(15 downto 0);
		AdcDataG			: out std_logic_vector(15 downto 0);
		AdcDataH			: out std_logic_vector(15 downto 0);
		AdcDataFrame		: out std_logic_vector(15 downto 0);
			--LVCMOS25 OUT
		DigO1_lv_out		: in std_logic_vector(0 downto 0);
		DigO1_lv_enable		: in std_logic_vector(0 downto 0);
		DigO2_lv_out		: in std_logic_vector(0 downto 0);
		DigO2_lv_enable		: in std_logic_vector(0 downto 0);
		DigO3_lv_out		: in std_logic_vector(0 downto 0);
		DigO3_lv_enable		: in std_logic_vector(0 downto 0);
		DigO4_lv_out		: in std_logic_vector(0 downto 0);
		DigO4_lv_enable		: in std_logic_vector(0 downto 0);
		DigO5_lv_out		: in std_logic_vector(0 downto 0);
		DigO5_lv_enable		: in std_logic_vector(0 downto 0);
		DigO6_lv_out		: in std_logic_vector(0 downto 0);
		DigO6_lv_enable		: in std_logic_vector(0 downto 0);
		DigO7_lv_out		: in std_logic_vector(0 downto 0);
		DigO7_lv_enable		: in std_logic_vector(0 downto 0);
		DigO8_lv_out		: in std_logic_vector(0 downto 0);
		DigO8_lv_enable		: in std_logic_vector(0 downto 0);
		DigO9_lv_out		: in std_logic_vector(0 downto 0);
		DigO9_lv_enable		: in std_logic_vector(0 downto 0);
		DigO10_lv_out		: in std_logic_vector(0 downto 0);
		DigO10_lv_enable	: in std_logic_vector(0 downto 0);
			--LVCMOS25 IN
		DigA_lv_in			: out std_logic_vector(0 downto 0);
		DigB_lv_in			: out std_logic_vector(0 downto 0);
		DigC_lv_in			: out std_logic_vector(0 downto 0);
		DigD_lv_in			: out std_logic_vector(0 downto 0);
		DigE_lv_in			: out std_logic_vector(0 downto 0);
		DigF_lv_in			: out std_logic_vector(0 downto 0);
		DigG_lv_in			: out std_logic_vector(0 downto 0);
		DigH_lv_in			: out std_logic_vector(0 downto 0);
		Dig1_lv_in			: out std_logic_vector(0 downto 0);
		Dig2_lv_in			: out std_logic_vector(0 downto 0);
		Dig3_lv_in			: out std_logic_vector(0 downto 0);
		--- /DATA
		--- SERIAL COMM
		RmcSclk_lv_out      : in std_logic_vector(0 downto 0);
		RmcSclk_lv_enable   : in std_logic_vector(0 downto 0);
		RmcSdio_lv_in       : out std_logic_vector(0 downto 0);
		RmcSdio_lv_out      : in std_logic_vector(0 downto 0);
		RmcSdio_lv_enable   : in std_logic_vector(0 downto 0);
		RmcCsb_lv_out       : in std_logic_vector(0 downto 0);
		RmcCsb_lv_enable    : in std_logic_vector(0 downto 0);
		--- /SERIAL COMM
		--- MMCM
		MMCM_DADDR 			: in std_logic_vector(7 downto 0);
		MMCM_DEN 			: in std_logic;
		MMCM_DIN 			: in std_logic_vector(15 downto 0);
		MMCM_DOUT 			: out std_logic_vector(15 downto 0);
		MMCM_DRDY 			: out std_logic;
		MMCM_DWE 			: in std_logic;
		MMCM_DCLK 			: in std_logic;
		--- /MMCM
		--- FABRIC (TARGET SPECYFING)
		aReset		: in std_logic;
		aDio		: inout std_logic_vector(87 downto 0);
		aDio_n		: inout std_logic_vector(87 downto 16);
		--- /FABRIC
		--- SOCKET DEFAULTS (TARGET SPECYFING)
			--connect these to pins for CAN ports
		CAN0_rx : out std_logic;
		CAN0_tx : in std_logic;
		CAN0_rs : in std_logic;
		CAN1_rx : out std_logic;
		CAN1_tx : in std_logic;
		CAN1_rs : in std_logic;

			--common clocks
		EnetClk125 : in std_logic;
		SerialClk : in std_logic;

			--zynq's ethernet1 MAC
		GBE1_GMII_COL : out std_logic;
		GBE1_GMII_CRS : out std_logic;
		GBE1_GMII_RX_CLK : out std_logic;
		GBE1_GMII_RX_DV : out std_logic;
		GBE1_GMII_RX_ER : out std_logic;
		GBE1_GMII_RX_D : out std_logic_vector(7 downto 0);
		GBE1_GMII_TX_CLK : out std_logic;
		GBE1_GMII_TX_EN : in std_logic;
		GBE1_GMII_TX_ER : in std_logic;
		GBE1_GMII_TX_D : in std_logic_vector(7 downto 0);
		GBE1_MDC : in std_logic;
		GBE1_MDIO_In : out std_logic;
		GBE1_MDIO_Out : in std_logic;
		GBE1_MDIO_Enable : in std_logic;
		aEth1AtGigabit_n : in std_logic;
		aEth1At10Mb_n : in std_logic;
		GBE1_IRQ : out std_logic;

			--connect this to soft serial components
		sSerialRegPortIn : in std_logic_vector(100 downto 0);
		sSerialRegPortOut : out std_logic_vector(64 downto 0);

			--the modem lines from Serial1, connect to pins if desired
		aSerial1Dtr_n : in std_logic;
		aSerial1Rts_n : in std_logic;
		aSerial1Cts_n : out std_logic;
		aSerial1Dsr_n : out std_logic;
		aSerial1Ri_n : out std_logic;
		aSerial1Dcd_n : out std_logic;
		aSerial2Irq : out std_logic;
		aSerial3Irq : out std_logic;
		aSerial4Irq : out std_logic;
		aSerial5Irq : out std_logic;
		aSerial6Irq : out std_logic
		--- /SOCKET DEFAULTS

	);

end ADC_9651_CLIP;

architecture RTL of ADC_9651_CLIP is

	signal sSerial2RegPortOut, sSerial3RegPortOut, sSerial4RegPortOut, sSerial5RegPortOut, sSerial6RegPortOut : RegPortOut_t;

	---------------------------------------------------------------
	-- Eth2 Signals
	---------------------------------------------------------------
	signal eEth1AtMegabit, eEth1AtGigabit, eEth1At10Megabit : std_logic;
	signal aDiffData, aDiffDataTristate, aDiffData_p, aDiffData_n : std_logic_vector(9 downto 0);
	signal EnetClk125TxFb,EnetClkRxFb : std_logic;
	signal Eth2GmiiTxClkDly : std_logic;
	signal Eth2GmiiRxClk, Eth2GmiiRxClk_bufg, Eth2GmiiRxClk_bufio : std_logic;
	signal Eth2GmiiRxd, Eth2GmiiRxdDly : std_logic_vector(7 downto 0);
	signal Eth2GmiiRxEr, Eth2GmiiRxErDly : std_logic;
	signal Eth2GmiiRxDv, Eth2GmiiRxDvDly : std_logic;
	-- Using dont_touch to prevent Vivado from inserting a BUFG after the PLL that
	-- used to delay TxClk. Vivado doesn't reliable insert the BUFG (based on
	-- resource usage) and can lead to timing violations. Another option is to
	-- explicitly instantiate a BUFG and compensate the PLL phase.
	attribute dont_touch : string;
	attribute dont_touch of Eth2GmiiTxClkDly : signal is "true";

	---------------------------------------------------------------
	-- I/O Signals
	---------------------------------------------------------------
	signal aDio_in : std_logic_vector(87 downto 0);
	signal aDio_out : std_logic_vector(87 downto 0);
	signal aDio_enable : std_logic_vector(87 downto 0);
	signal aDio_n_in : std_logic_vector(87 downto 16);
	signal aDio_n_out : std_logic_vector(87 downto 16);
	signal aDio_n_enable : std_logic_vector(87 downto 16);


	-- Negated signals of aDio_enable and aDio_n_enable for VSIM compliance
	signal aDio_enable_n : std_logic_vector(87 downto 0);
	signal aDio_n_enable_n : std_logic_vector(87 downto 16);

	-- Forcing input buffers on PUDC configuration pin to prevent glitching
	signal aFpgaPudcInternal : std_logic;
	attribute dont_touch of aFpgaPudcInternal : signal is "true";



---------------------------------------------------------------------------------------------
-- Component Instantiation
---------------------------------------------------------------------------------------------
-- Constants, Signals and Attributes Declarations
	constant C_OnChipLvdsTerm        : integer := 1;     -- 0 = No Term, 1 - Termination ON. -- OK(1)
	constant C_AdcChnls              : integer := 8;     -- Number of ADC in a package       -- OK(8)
	constant C_AdcBits               : integer := 14;                                        -- OK(12)
	constant C_StatTaps              : integer := 16;                                        -- OK(16)

-- Functions

	function ConvTrueFalse (Term : integer) return boolean is
	begin
		if (Term = 0) then
			return FALSE;
		else
			return TRUE;
		end if;
	end ConvTrueFalse;
	
-- Constants

	constant Low  : std_logic   := '0';
	constant High : std_logic   := '1';
	
-- Signals

	signal IntDCLK_p : std_logic;
	signal IntDCLK_n : std_logic;
	signal IntFCLK_p : std_logic;
	signal IntFCLK_n : std_logic;
	signal IntDATA_p : std_logic_vector(C_AdcChnls-1 downto 0);
	signal IntDATA_n : std_logic_vector(C_AdcChnls-1 downto 0);

	signal FpgaRefClkFromLvInt	: std_logic;
	signal AdcSampleClkInt  	: std_logic;
    signal AdcFrmDataOutInt		: std_logic_vector(C_AdcBits-1 downto 0);
    signal AdcDatDataOutInt		: std_logic_vector((C_AdcBits*C_AdcChnls)-1 downto 0);
	signal AdcClkDivInt			: std_logic; 
	signal AdcBitClkDoneInt		: std_logic;
	signal AdcClkDivSelInt		: std_logic;
	signal AdcSampleClkLockInt	: std_logic;
	
-- Attributes

begin

	aFpgaPudcInternal <= aDio_in(35);

	aDio_enable_n <= not aDio_enable;
	aDio_n_enable_n <= not aDio_n_enable;

	sSerialRegPortOut <= AxiRegPortToSlv(sSerial2RegPortOut or sSerial3RegPortOut or sSerial4RegPortOut or sSerial5RegPortOut or sSerial6RegPortOut);


	---------------------------------------------------------------
	--Peripheral : CAN0
	---------------------------------------------------------------
	CAN0_rx <= '0';



	---------------------------------------------------------------
	--Peripheral : CAN1
	---------------------------------------------------------------
	CAN1_rx <= '0';



	---------------------------------------------------------------
	--Peripheral : Serial2
	---------------------------------------------------------------
	sSerial2RegPortOut <= kRegPortOutZero;
	aSerial2Irq <= '0';




	---------------------------------------------------------------
	--Peripheral : Serial3
	---------------------------------------------------------------
	sSerial3RegPortOut <= kRegPortOutZero;
	aSerial3Irq <= '0';




	---------------------------------------------------------------
	--Peripheral : Serial4
	---------------------------------------------------------------
	sSerial4RegPortOut <= kRegPortOutZero;
	aSerial4Irq <= '0';




	---------------------------------------------------------------
	--Peripheral : Serial5
	---------------------------------------------------------------
	sSerial5RegPortOut <= kRegPortOutZero;
	aSerial5Irq <= '0';




	---------------------------------------------------------------
	--Peripheral : Serial6
	---------------------------------------------------------------
	sSerial6RegPortOut <= kRegPortOutZero;
	aSerial6Irq <= '0';




	---------------------------------------------------------------
	--Peripheral : Serial1
	---------------------------------------------------------------
	aSerial1Cts_n <= '0';
	aSerial1Dsr_n <= '0';
	aSerial1Ri_n <= '1';
	aSerial1Dcd_n <= '0';



	---------------------------------------------------------------
	--Peripheral : Eth2
	---------------------------------------------------------------
	GBE1_IRQ <= '1';
	-- End Eth 1

--


----------------------------------------------------------------
	--ADC SAMPLE, DATA AND FRAME CLOKS >
----------------------------------------------------------------


	---------------------------------------------------------------
	--I/O Buffer: DIO_39_DIFF - DCO_ADC
	---------------------------------------------------------------
	
	IOBUFDS_39 : IBUFGDS
		generic map 
		(
			DIFF_TERM  => ConvTrueFalse(C_OnChipLvdsTerm), 
			IOSTANDARD  => "LVDS_25"
		)
		port map 
		(
			I => aDio(39), 
			IB => aDio_n(39), 
			O => IntDCLK_p
		);
	
	IntDCLK_n <= Low;

	IntFCLK_p <= aDio(36);
	IntFCLK_n <= aDio_n(36);
	
----------------------------------------------------------------
	--ADC DATA AND FRAME CLOKS <
----------------------------------------------------------------

----------------------------------------------------------------
	--ADC SERIAL DATA >
----------------------------------------------------------------


	IntDATA_p <=   aDio(33)	&   aDio(32)	&   aDio(31)	&   aDio(29)	&   aDio(28)	&   aDio(27)	&   aDio(25)	&   aDio(19);
	IntDATA_n <= aDio_n(33)	& aDio_n(32)	& aDio_n(31)	& aDio_n(29)	& aDio_n(28)	& aDio_n(27)	& aDio_n(25)	& aDio_n(19);		

----------------------------------------------------------------
	--ADC SERIAL DATA <
----------------------------------------------------------------

---------------------------------------------------------------------------------------------


	clk_4x_inst: entity work.clk_4x
		port map 
		(
			clk_in_sel	=> FpgaRefClkSelLv,
			clk_in 		=> FpgaRefClkFromLv,		--10MHz INTERNAL
			clk_in2 	=> aDio(37),				--10MHz EXTERNAL
			clk_in_buf 	=> FpgaRefClkFromLvInt,		--10MHz INTERNAL
			clk_in2_buf => FpgaRefClkInToLv,		--10MHz EXTERNAL
			clk_out 	=> AdcSampleClkInt,			--40MHz
			locked 		=> AdcSampleClkLockInt,
			reset 		=> AdcSampleClkRstLv,
			
			MMCM_DADDR	=> MMCM_DADDR,
			MMCM_DEN	=> MMCM_DEN,
			MMCM_DIN	=> MMCM_DIN,
			MMCM_DOUT	=> MMCM_DOUT,
			MMCM_DRDY	=> MMCM_DRDY,
			MMCM_DWE	=> MMCM_DWE,
			MMCM_DCLK	=> MMCM_DCLK
		);

	AdcSampleClkToLv 	<= AdcSampleClkInt;
	AdcSampleClkLockLv  <= AdcSampleClkLockInt;
	---------------------------------------------------------------
	--I/O Buffer: DIO_60_DIFF - SCO_ADC
	---------------------------------------------------------------
	IOBUFDS_60 : OBUFTDS
		generic map 
		(
			IOSTANDARD => "LVDS_25"
		)
		port map 
		(
			O  => aDio(60),
			OB => aDio_n(60),
			I  => aDio_out(60),
			T  => aDio_enable_n(60)
		);

	aDio_in(60) 	<= aDio_out(60);
	aDio_out(60) 	<= AdcSampleClkInt;
	aDio_enable(60)	<= AdcSampleClkEnLv and AdcSampleClkLockInt;
	
	AdcSampleClkMux: BUFGMUX_CTRL
		port map 
		(
			S 	=> AdcClkDivSelInt,
			I0	=> AdcClkDivFromLv,	-- (S=0)
			I1 	=> AdcClkDivInt,	-- (S=1)
			O	=> AdcClkDivToLv
		);

	AdcClkDivSelInt <= AdcBitClkDoneInt and AdcClkDivSelLv and AdcSampleClkEnLv;

	TopLrvrl14ddr_inst : entity work.TopLrvrl14ddr 
		generic map (
			C_AdcChnls          => 8,     -- Number of ADC in a package
			C_AdcBits           => 14,
			C_StatTaps          => 16
		)
		port map 
		(
			DCLK_p	          		=> IntDCLK_p,
			DCLK_n	          		=> IntDCLK_n,
			FCLK_p	         	 	=> IntFCLK_p,
			FCLK_n	          		=> IntFCLK_n,
			DATA_p	          		=> IntDATA_p,
			DATA_n	          		=> IntDATA_n,
			-- application connections
			SysRefClk          		=> SysRefClkFromLv,
			AdcIntrfcRst       		=> AdcIntrfcRstLv, 
			AdcIntrfcEna       		=> AdcIntrfcEnaLv,
			AdcReSync          		=> AdcReSyncLv,
			AdcBitSlip         		=> AdcBitSlipLv,
			AdcBitClkAlgnWrn   		=> AdcBitClkAlgnWrnLv, 
			AdcBitClkInvrtd    		=> AdcBitClkInvrtdLv, 
			AdcBitClkDone      		=> AdcBitClkDoneInt, 
			AdcBitClkIdlyCtrlRdy   	=> AdcBitClkIdlyCtrlRdyLv,
				-- Data from the frame clock
			AdcFrmDataOut      		=> AdcFrmDataOutInt,
				-- ADC memory data output    
			AdcDatClkDiv       		=> AdcClkDivInt,
			AdcDatOrderRev			=> AdcDatOrderRevLv,
			AdcDatDataOut      		=> AdcDatDataOutInt
		);
		 
	AdcBitClkDoneLv <= AdcBitClkDoneInt;

	AdcDataA     <= "00" & AdcDatDataOutInt(13 downto 0);
	AdcDataB     <= "00" & AdcDatDataOutInt(27 downto 14);
	AdcDataC     <= "00" & AdcDatDataOutInt(41 downto 28);
	AdcDataD     <= "00" & AdcDatDataOutInt(55 downto 42);
	AdcDataE     <= "00" & AdcDatDataOutInt(69 downto 56);
	AdcDataF     <= "00" & AdcDatDataOutInt(83 downto 70);
	AdcDataG     <= "00" & AdcDatDataOutInt(97 downto 84);
	AdcDataH     <= "00" & AdcDatDataOutInt(111 downto 98);
	
	AdcDataFrame <=  "00" & AdcFrmDataOutInt(13 downto 0);


---------------------------------------------------------------------------------------------
--

----------------------------------------------------------------
	--FPGA PPS, INPUT AND OUTPUT CLOCKS >
----------------------------------------------------------------

	---------------------------------------------------------------
	--I/O Buffer: DIO_61 - CLK_PPS_FPGA_IN
	---------------------------------------------------------------
	IOBUF_61 : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_in(61),
			I  => aDio(61));

	PpsFpgaClkInToLv <= aDio_in(61);
	

	---------------------------------------------------------------
	--I/O Buffer: DIO_37 - CLK_10MHZ_FPGA_IN
	---------------------------------------------------------------
--	IOBUF_37 : IBUF
--		generic map (IOSTANDARD => "LVCMOS25")
--		port map (
--			O  => aDio_in(37),
--			I  => aDio(37));


	---------------------------------------------------------------
	--I/O Buffer: DIO_38 - CLK_10MHZ_FPGA_OUT 
	---------------------------------------------------------------
	IOBUF_38 : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 12, SLEW => "FAST")
		port map (
			O  => aDio(38),
			I  => aDio_out(38),
			T  => aDio_enable_n(38));

	aDio_in(38)      <= aDio_out(38);
	aDio_out(38)     <= FpgaClkOutFromLv;
	aDio_enable(38)  <= FpgaClkOutEnLv;


----------------------------------------------------------------
	--FPGA PPS, INPUT AND OUTPUT CLOCKS <
----------------------------------------------------------------

----------------------------------------------------------------
	--DIGITAL OUTPUTS >
----------------------------------------------------------------

	---------------------------------------------------------------
	--I/O Buffer: DIO_64 - PROGRAM_LED
	---------------------------------------------------------------
	IOBUF_64 : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio(64),
			I  => aDio_out(64),
			T  => aDio_enable_n(64));

	aDio_in(64)      <= aDio_out(64);
	aDio_out(64)     <= DigO1_lv_out(0);
	aDio_enable(64)  <= DigO1_lv_enable(0);
	
	
	---------------------------------------------------------------
	--I/O Buffer: DIO_64_N - DO_EN
	---------------------------------------------------------------
	IOBUF_64_N : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n(64),
			I  => aDio_n_out(64),
			T  => aDio_n_enable_n(64));

	aDio_n_in(64)      <= aDio_n_out(64);
	aDio_n_out(64)     <= DigO2_lv_out(0);
	aDio_n_enable(64)  <= DigO2_lv_enable(0);
	
	
	---------------------------------------------------------------
	--I/O Buffer: DIO_65 - BITE_PR_LE
	---------------------------------------------------------------
	IOBUF_65 : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio(65),
			I  => aDio_out(65),
			T  => aDio_enable_n(65));

	aDio_in(65)      <= aDio_out(65);
	aDio_out(65)     <= DigO3_lv_out(0);
	aDio_enable(65)  <= DigO3_lv_enable(0);
	
	
	---------------------------------------------------------------
	--I/O Buffer: DIO_65_N - TLUMIK_S1_LE
	---------------------------------------------------------------
	IOBUF_65_N : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n(65),
			I  => aDio_n_out(65),
			T  => aDio_n_enable_n(65));

	aDio_n_in(65)      <= aDio_n_out(65);
	aDio_n_out(65)     <= DigO4_lv_out(0);
	aDio_n_enable(65)  <= DigO4_lv_enable(0);
	

	---------------------------------------------------------------
	--I/O Buffer: DIO_66 - TLUMIK_S1_PR
	---------------------------------------------------------------
	IOBUF_66 : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio(66),
			I  => aDio_out(66),
			T  => aDio_enable_n(66));

	aDio_in(66)      <= aDio_out(66);
	aDio_out(66)     <= DigO5_lv_out(0);
	aDio_enable(66)  <= DigO5_lv_enable(0);
	
	
	---------------------------------------------------------------
	--I/O Buffer: DIO_66 - TLUMIK_S2_LE
	---------------------------------------------------------------
	IOBUF_66_N : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n(66),
			I  => aDio_n_out(66),
			T  => aDio_n_enable_n(66));

	aDio_n_in(66)      <= aDio_n_out(66);
	aDio_n_out(66)     <= DigO6_lv_out(0);
	aDio_n_enable(66)  <= DigO6_lv_enable(0);
	
	
	---------------------------------------------------------------
	--I/O Buffer: DIO_67 - TLUMIK_S2_PR
	---------------------------------------------------------------
	IOBUF_67 : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio(67),
			I  => aDio_out(67),
			T  => aDio_enable_n(67));

	aDio_in(67)      <= aDio_out(67);
	aDio_out(67)     <= DigO7_lv_out(0);
	aDio_enable(67)  <= DigO7_lv_enable(0);
	
	
	---------------------------------------------------------------
	--I/O Buffer: DIO_67 - TLUMIK_S3_LE
	---------------------------------------------------------------
	IOBUF_67_N : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n(67),
			I  => aDio_n_out(67),
			T  => aDio_n_enable_n(67));

	aDio_n_in(67)      <= aDio_n_out(67);
	aDio_n_out(67)     <= DigO8_lv_out(0);
	aDio_n_enable(67)  <= DigO8_lv_enable(0);
	
	---------------------------------------------------------------
	--I/O Buffer: DIO_68 - TLUMIK_S3_PR
	---------------------------------------------------------------
	IOBUF_68 : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio(68),
			I  => aDio_out(68),
			T  => aDio_enable_n(68));

	aDio_in(68)      <= aDio_out(68);
	aDio_out(68)     <= DigO9_lv_out(0);
	aDio_enable(68)  <= DigO9_lv_enable(0);
	
	
	---------------------------------------------------------------
	--I/O Buffer: DIO_68 - REZERWA
	---------------------------------------------------------------
	IOBUF_68_N : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n(68),
			I  => aDio_n_out(68),
			T  => aDio_n_enable_n(68));

	aDio_n_in(68)      <= aDio_n_out(68);
	aDio_n_out(68)     <= DigO10_lv_out(0);
	aDio_n_enable(68)  <= DigO10_lv_enable(0);

----------------------------------------------------------------
	--DIGITAL OUTPUTS <
----------------------------------------------------------------

----------------------------------------------------------------
	--DIGITAL INPUTS >
----------------------------------------------------------------

	---------------------------------------------------------------
	--I/O Buffer: DIO_16 - DI_SYNC_1
	---------------------------------------------------------------
	IOBUF_16 : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_in(16),
			I  => aDio(16));

	DigA_lv_in(0) <= aDio_in(16);


	---------------------------------------------------------------
	--I/O Buffer: DIO_16_N - DI_SYNC_2
	---------------------------------------------------------------
	IOBUF_16_N : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_n_in(16),
			I  => aDio_n(16));

	DigB_lv_in(0) <= aDio_n_in(16);


	---------------------------------------------------------------
	--I/O Buffer: DIO_17 - DI_SYNC_3
	---------------------------------------------------------------
	IOBUF_17 : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_in(17),
			I  => aDio(17));

	DigC_lv_in(0) <= aDio_in(17);


	---------------------------------------------------------------
	--I/O Buffer: DIO_17_N - DI_SYNC_4
	---------------------------------------------------------------
	IOBUF_17_N : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_n_in(17),
			I  => aDio_n(17));

	DigD_lv_in(0) <= aDio_n_in(17);


	---------------------------------------------------------------
	--I/O Buffer: DIO_18 - DI_SYNC_5
	---------------------------------------------------------------
	IOBUF_18 : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_in(18),
			I  => aDio(18));

	DigE_lv_in(0) <= aDio_in(18);


	---------------------------------------------------------------
	--I/O Buffer: DIO_18_N - DI_SYNC_6
	---------------------------------------------------------------
	IOBUF_18_N : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_n_in(18),
			I  => aDio_n(18));

	DigF_lv_in(0) <= aDio_n_in(18);


	---------------------------------------------------------------
	--I/O Buffer: DIO_20 - DI_SYNC_7
	---------------------------------------------------------------
	IOBUF_20 : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_in(20),
			I  => aDio(20));

	DigG_lv_in(0) <= aDio_in(20);
	
	
	---------------------------------------------------------------
	--I/O Buffer: DIO_20_N - DI_SYNC_8
	---------------------------------------------------------------
	IOBUF_20_N : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_n_in(20),
			I  => aDio_n(20));

	DigH_lv_in(0) <= aDio_n_in(20);


	---------------------------------------------------------------
	--I/O Buffer: DIO_69 - KOD_WTY_1
	---------------------------------------------------------------
	IOBUF_69 : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_in(69),
			I  => aDio(69));

	Dig1_lv_in(0) <= aDio_in(69);


	---------------------------------------------------------------
	--I/O Buffer: DIO_69_N - KOD_WTY_2
	---------------------------------------------------------------
	IOBUF_69_N : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_n_in(69),
			I  => aDio_n(69));

	Dig2_lv_in(0) <= aDio_n_in(69);


	---------------------------------------------------------------
	--I/O Buffer: DIO_70 - KOD_WTY_3
	---------------------------------------------------------------
	IOBUF_70 : IBUF
		generic map (IOSTANDARD => "LVCMOS25")
		port map (
			O  => aDio_in(70),
			I  => aDio(70));

	Dig3_lv_in(0) <= aDio_in(70);
	
	
----------------------------------------------------------------
	--DIGITAL INPUTS <
----------------------------------------------------------------

----------------------------------------------------------------
	--SPI INTERFACE >
----------------------------------------------------------------

	---------------------------------------------------------------
	--I/O Buffer: DIO_62 - ADC_SCLK
	---------------------------------------------------------------
	IOBUF_62 : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "FAST")
		port map (
			O  => aDio(62),
			I  => aDio_out(62),
			T  => aDio_enable_n(62));

	aDio_in(62)      <= aDio_out(62);
	aDio_out(62)     <= RmcSclk_lv_out(0);
	aDio_enable(62)  <= RmcSclk_lv_enable(0);
	
	
	---------------------------------------------------------------
	--I/O Buffer: DIO56 - ADC_SDIO
	---------------------------------------------------------------
	IOBUF_56 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(56),
			IO => aDio(56),
			I  => aDio_out(56),
			T  => aDio_enable_n(56));

	RmcSdio_lv_in(0) <= aDio_in(56);
	aDio_out(56)     <= RmcSdio_lv_out(0);
	aDio_enable(56)  <= RmcSdio_lv_enable(0);
	
	
	---------------------------------------------------------------
	--I/O Buffer: DIO_59 - ADC_CS
	---------------------------------------------------------------
	IOBUF_59 : OBUFT
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio(59),
			I  => aDio_out(59),
			T  => aDio_enable_n(59));

	aDio_in(59)      <= aDio_out(59);
	aDio_out(59)     <= RmcCsb_lv_out(0);
	aDio_enable(59)  <= RmcCsb_lv_enable(0);


----------------------------------------------------------------
	--SPI INTERFACE <
----------------------------------------------------------------

----------------------------------------------------------------
	--NOT CONNECTED >
----------------------------------------------------------------

	---------------------------------------------------------------
	--I/O Buffer: DIO_00
	---------------------------------------------------------------
	IOBUF_0 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(0),
			IO => aDio(0),
			I  => aDio_out(0),
			T  => aDio_enable_n(0));

	---------------------------------------------------------------
	--I/O Buffer: DIO_01
	---------------------------------------------------------------
	IOBUF_1 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(1),
			IO => aDio(1),
			I  => aDio_out(1),
			T  => aDio_enable_n(1));

	---------------------------------------------------------------
	--I/O Buffer: DIO_02
	---------------------------------------------------------------
	IOBUF_2 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(2),
			IO => aDio(2),
			I  => aDio_out(2),
			T  => aDio_enable_n(2));

	---------------------------------------------------------------
	--I/O Buffer: DIO_03
	---------------------------------------------------------------
	IOBUF_3 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(3),
			IO => aDio(3),
			I  => aDio_out(3),
			T  => aDio_enable_n(3));

	---------------------------------------------------------------
	--I/O Buffer: DIO_04
	---------------------------------------------------------------
	IOBUF_4 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(4),
			IO => aDio(4),
			I  => aDio_out(4),
			T  => aDio_enable_n(4));

	---------------------------------------------------------------
	--I/O Buffer: DIO_05
	---------------------------------------------------------------
	IOBUF_5 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(5),
			IO => aDio(5),
			I  => aDio_out(5),
			T  => aDio_enable_n(5));

	---------------------------------------------------------------
	--I/O Buffer: DIO_06
	---------------------------------------------------------------
	IOBUF_6 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(6),
			IO => aDio(6),
			I  => aDio_out(6),
			T  => aDio_enable_n(6));

	---------------------------------------------------------------
	--I/O Buffer: DIO_07
	---------------------------------------------------------------
	IOBUF_7 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(7),
			IO => aDio(7),
			I  => aDio_out(7),
			T  => aDio_enable_n(7));

	---------------------------------------------------------------
	--I/O Buffer: DIO_08
	---------------------------------------------------------------
	IOBUF_8 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(8),
			IO => aDio(8),
			I  => aDio_out(8),
			T  => aDio_enable_n(8));

	---------------------------------------------------------------
	--I/O Buffer: DIO_09
	---------------------------------------------------------------
	IOBUF_9 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(9),
			IO => aDio(9),
			I  => aDio_out(9),
			T  => aDio_enable_n(9));

	---------------------------------------------------------------
	--I/O Buffer: DIO_10
	---------------------------------------------------------------
	IOBUF_10 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(10),
			IO => aDio(10),
			I  => aDio_out(10),
			T  => aDio_enable_n(10));

	---------------------------------------------------------------
	--I/O Buffer: DIO_11
	---------------------------------------------------------------
	IOBUF_11 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(11),
			IO => aDio(11),
			I  => aDio_out(11),
			T  => aDio_enable_n(11));

	---------------------------------------------------------------
	--I/O Buffer: DIO_12
	---------------------------------------------------------------
	IOBUF_12 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(12),
			IO => aDio(12),
			I  => aDio_out(12),
			T  => aDio_enable_n(12));

	---------------------------------------------------------------
	--I/O Buffer: DIO_13
	---------------------------------------------------------------
	IOBUF_13 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(13),
			IO => aDio(13),
			I  => aDio_out(13),
			T  => aDio_enable_n(13));

	---------------------------------------------------------------
	--I/O Buffer: DIO_14
	---------------------------------------------------------------
	IOBUF_14 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(14),
			IO => aDio(14),
			I  => aDio_out(14),
			T  => aDio_enable_n(14));

	---------------------------------------------------------------
	--I/O Buffer: DIO_15
	---------------------------------------------------------------
	IOBUF_15 : IOBUF
		generic map (IOSTANDARD => "LVCMOS33", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(15),
			IO => aDio(15),
			I  => aDio_out(15),
			T  => aDio_enable_n(15));

	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_21
	  ---------------------------------------------------------------
	  IOBUF_21 : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_in(21),
		IO => aDio(21),
		I  => aDio_out(21),
		T  => aDio_enable_n(21));

	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_21_N
	  ---------------------------------------------------------------
	  IOBUF_21_N : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_n_in(21),
		IO => aDio_n(21),
		I  => aDio_n_out(21),
		T  => aDio_n_enable_n(21));

	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_22
	  ---------------------------------------------------------------
	  IOBUF_22 : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_in(22),
		IO => aDio(22),
		I  => aDio_out(22),
		T  => aDio_enable_n(22));

	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_22_N
	  ---------------------------------------------------------------
	  IOBUF_22_N : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_n_in(22),
		IO => aDio_n(22),
		I  => aDio_n_out(22),
		T  => aDio_n_enable_n(22));

	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_23
	  ---------------------------------------------------------------
	  IOBUF_23 : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_in(23),
		IO => aDio(23),
		I  => aDio_out(23),
		T  => aDio_enable_n(23));

	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_23_N
	  ---------------------------------------------------------------
	  IOBUF_23_N : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_n_in(23),
		IO => aDio_n(23),
		I  => aDio_n_out(23),
		T  => aDio_n_enable_n(23));

	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_24
	  ---------------------------------------------------------------
	  IOBUF_24 : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_in(24),
		IO => aDio(24),
		I  => aDio_out(24),
		T  => aDio_enable_n(24));

	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_24_N
	  ---------------------------------------------------------------
	  IOBUF_24_N : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_n_in(24),
		IO => aDio_n(24),
		I  => aDio_n_out(24),
		T  => aDio_n_enable_n(24));

	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_26
	  ---------------------------------------------------------------
	  IOBUF_26 : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_in(26),
		IO => aDio(26),
		I  => aDio_out(26),
		T  => aDio_enable_n(26));

	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_26_N
	  ---------------------------------------------------------------
	  IOBUF_26_N : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_n_in(26),
		IO => aDio_n(26),
		I  => aDio_n_out(26),
		T  => aDio_n_enable_n(26));
		
	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_30
	  ---------------------------------------------------------------
	  IOBUF_30 : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_in(30),
		IO => aDio(30),
		I  => aDio_out(30),
		T  => aDio_enable_n(30));

	  ---------------------------------------------------------------
	  --I/O Buffer: DIO_30_N
	  ---------------------------------------------------------------
	  IOBUF_30_N : IOBUF
	  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
	  port map (
		O  => aDio_n_in(30),
		IO => aDio_n(30),
		I  => aDio_n_out(30),
		T  => aDio_n_enable_n(30));
		
	---------------------------------------------------------------
	--I/O Buffer: DIO_34
	---------------------------------------------------------------
	IOBUF_34 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(34),
			IO => aDio(34),
			I  => aDio_out(34),
			T  => aDio_enable_n(34));		
		
	---------------------------------------------------------------
	--I/O Buffer: DIO_34_N
	---------------------------------------------------------------
	IOBUF_34_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(34),
			IO => aDio_n(34),
			I  => aDio_n_out(34),
			T  => aDio_n_enable_n(34));

	---------------------------------------------------------------
	--I/O Buffer: DIO_35
	---------------------------------------------------------------
	-- Using a Bidirectional buffer for PUDC pin because it prevents a glitch.
	IOBUF_35 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(35),
			IO => aDio(35),
			I  => aDio_out(35),
			T  => aDio_enable_n(35));

	---------------------------------------------------------------
	--I/O Buffer: DIO_35_N
	---------------------------------------------------------------
	IOBUF_35_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(35),
			IO => aDio_n(35),
			I  => aDio_n_out(35),
			T  => aDio_n_enable_n(35));

	---------------------------------------------------------------
	--I/O Buffer: DIO_37_N
	---------------------------------------------------------------
	IOBUF_37_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(37),
			IO => aDio_n(37),
			I  => aDio_n_out(37),
			T  => aDio_n_enable_n(37));

	---------------------------------------------------------------
	--I/O Buffer: DIO_38_N
	---------------------------------------------------------------
	IOBUF_38_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(38),
			IO => aDio_n(38),
			I  => aDio_n_out(38),
			T  => aDio_n_enable_n(38));

	---------------------------------------------------------------
	--I/O Buffer: DIO_40
	---------------------------------------------------------------
	IOBUF_40 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(40),
			IO => aDio(40),
			I  => aDio_out(40),
			T  => aDio_enable_n(40));

	---------------------------------------------------------------
	--I/O Buffer: DIO_40_N
	---------------------------------------------------------------
	IOBUF_40_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(40),
			IO => aDio_n(40),
			I  => aDio_n_out(40),
			T  => aDio_n_enable_n(40));

	---------------------------------------------------------------
	--I/O Buffer: DIO_41
	---------------------------------------------------------------
	IOBUF_41 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(41),
			IO => aDio(41),
			I  => aDio_out(41),
			T  => aDio_enable_n(41));

	---------------------------------------------------------------
	--I/O Buffer: DIO_41_N
	---------------------------------------------------------------
	IOBUF_41_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(41),
			IO => aDio_n(41),
			I  => aDio_n_out(41),
			T  => aDio_n_enable_n(41));

	---------------------------------------------------------------
	--I/O Buffer: DIO_42
	---------------------------------------------------------------
	IOBUF_42 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(42),
			IO => aDio(42),
			I  => aDio_out(42),
			T  => aDio_enable_n(42));

	---------------------------------------------------------------
	--I/O Buffer: DIO_42_N
	---------------------------------------------------------------
	IOBUF_42_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(42),
			IO => aDio_n(42),
			I  => aDio_n_out(42),
			T  => aDio_n_enable_n(42));

	---------------------------------------------------------------
	--I/O Buffer: DIO_43
	---------------------------------------------------------------
	IOBUF_43 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(43),
			IO => aDio(43),
			I  => aDio_out(43),
			T  => aDio_enable_n(43));

	---------------------------------------------------------------
	--I/O Buffer: DIO_43_N
	---------------------------------------------------------------
	IOBUF_43_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(43),
			IO => aDio_n(43),
			I  => aDio_n_out(43),
			T  => aDio_n_enable_n(43));

	---------------------------------------------------------------
	--I/O Buffer: DIO_44
	---------------------------------------------------------------
	IOBUF_44 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(44),
			IO => aDio(44),
			I  => aDio_out(44),
			T  => aDio_enable_n(44));

	---------------------------------------------------------------
	--I/O Buffer: DIO_44_N
	---------------------------------------------------------------
	IOBUF_44_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(44),
			IO => aDio_n(44),
			I  => aDio_n_out(44),
			T  => aDio_n_enable_n(44));

	---------------------------------------------------------------
	--I/O Buffer: DIO_45
	---------------------------------------------------------------
	IOBUF_45 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(45),
			IO => aDio(45),
			I  => aDio_out(45),
			T  => aDio_enable_n(45));

	---------------------------------------------------------------
	--I/O Buffer: DIO_45_N
	---------------------------------------------------------------
	IOBUF_45_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(45),
			IO => aDio_n(45),
			I  => aDio_n_out(45),
			T  => aDio_n_enable_n(45));

	---------------------------------------------------------------
	--I/O Buffer: DIO_46
	---------------------------------------------------------------
	IOBUF_46 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(46),
			IO => aDio(46),
			I  => aDio_out(46),
			T  => aDio_enable_n(46));

	---------------------------------------------------------------
	--I/O Buffer: DIO_46_N
	---------------------------------------------------------------
	IOBUF_46_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(46),
			IO => aDio_n(46),
			I  => aDio_n_out(46),
			T  => aDio_n_enable_n(46));

	---------------------------------------------------------------
	--I/O Buffer: DIO_47
	---------------------------------------------------------------
	IOBUF_47 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(47),
			IO => aDio(47),
			I  => aDio_out(47),
			T  => aDio_enable_n(47));

	---------------------------------------------------------------
	--I/O Buffer: DIO_47_N
	---------------------------------------------------------------
	IOBUF_47_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(47),
			IO => aDio_n(47),
			I  => aDio_n_out(47),
			T  => aDio_n_enable_n(47));

	---------------------------------------------------------------
	--I/O Buffer: DIO_48
	---------------------------------------------------------------
	IOBUF_48 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(48),
			IO => aDio(48),
			I  => aDio_out(48),
			T  => aDio_enable_n(48));

	---------------------------------------------------------------
	--I/O Buffer: DIO_48_N
	---------------------------------------------------------------
	IOBUF_48_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(48),
			IO => aDio_n(48),
			I  => aDio_n_out(48),
			T  => aDio_n_enable_n(48));

	---------------------------------------------------------------
	--I/O Buffer: DIO_49
	---------------------------------------------------------------
	IOBUF_49 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(49),
			IO => aDio(49),
			I  => aDio_out(49),
			T  => aDio_enable_n(49));

	---------------------------------------------------------------
	--I/O Buffer: DIO_49_N
	---------------------------------------------------------------
	IOBUF_49_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(49),
			IO => aDio_n(49),
			I  => aDio_n_out(49),
			T  => aDio_n_enable_n(49));

	---------------------------------------------------------------
	--I/O Buffer: DIO_50
	---------------------------------------------------------------
	IOBUF_50 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(50),
			IO => aDio(50),
			I  => aDio_out(50),
			T  => aDio_enable_n(50));

	---------------------------------------------------------------
	--I/O Buffer: DIO_50_N
	---------------------------------------------------------------
	IOBUF_50_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(50),
			IO => aDio_n(50),
			I  => aDio_n_out(50),
			T  => aDio_n_enable_n(50));

	---------------------------------------------------------------
	--I/O Buffer: DIO_51
	---------------------------------------------------------------
	IOBUF_51 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(51),
			IO => aDio(51),
			I  => aDio_out(51),
			T  => aDio_enable_n(51));

	---------------------------------------------------------------
	--I/O Buffer: DIO_51_N
	---------------------------------------------------------------
	IOBUF_51_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(51),
			IO => aDio_n(51),
			I  => aDio_n_out(51),
			T  => aDio_n_enable_n(51));

	---------------------------------------------------------------
	--I/O Buffer: DIO_52
	---------------------------------------------------------------
	IOBUF_52 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(52),
			IO => aDio(52),
			I  => aDio_out(52),
			T  => aDio_enable_n(52));

	---------------------------------------------------------------
	--I/O Buffer: DIO_52_N
	---------------------------------------------------------------
	IOBUF_52_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(52),
			IO => aDio_n(52),
			I  => aDio_n_out(52),
			T  => aDio_n_enable_n(52));

	---------------------------------------------------------------
	--I/O Buffer: DIO_53
	---------------------------------------------------------------
	IOBUF_53 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(53),
			IO => aDio(53),
			I  => aDio_out(53),
			T  => aDio_enable_n(53));

	---------------------------------------------------------------
	--I/O Buffer: DIO_53_N
	---------------------------------------------------------------
	IOBUF_53_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(53),
			IO => aDio_n(53),
			I  => aDio_n_out(53),
			T  => aDio_n_enable_n(53));

	---------------------------------------------------------------
	--I/O Buffer: DIO_54
	---------------------------------------------------------------
	IOBUF_54 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(54),
			IO => aDio(54),
			I  => aDio_out(54),
			T  => aDio_enable_n(54));

	---------------------------------------------------------------
	--I/O Buffer: DIO_54_N
	---------------------------------------------------------------
	IOBUF_54_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(54),
			IO => aDio_n(54),
			I  => aDio_n_out(54),
			T  => aDio_n_enable_n(54));

	---------------------------------------------------------------
	--I/O Buffer: DIO_55
	---------------------------------------------------------------
	IOBUF_55 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(55),
			IO => aDio(55),
			I  => aDio_out(55),
			T  => aDio_enable_n(55));

	---------------------------------------------------------------
	--I/O Buffer: DIO_55_N
	---------------------------------------------------------------
	IOBUF_55_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(55),
			IO => aDio_n(55),
			I  => aDio_n_out(55),
			T  => aDio_n_enable_n(55));

	---------------------------------------------------------------
	--I/O Buffer: DIO_56_N
	---------------------------------------------------------------
	IOBUF_56_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(56),
			IO => aDio_n(56),
			I  => aDio_n_out(56),
			T  => aDio_n_enable_n(56));

	---------------------------------------------------------------
	--I/O Buffer: DIO_57
	---------------------------------------------------------------
	IOBUF_57 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(57),
			IO => aDio(57),
			I  => aDio_out(57),
			T  => aDio_enable_n(57));

	---------------------------------------------------------------
	--I/O Buffer: DIO_57_N
	---------------------------------------------------------------
	IOBUF_57_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(57),
			IO => aDio_n(57),
			I  => aDio_n_out(57),
			T  => aDio_n_enable_n(57));

	---------------------------------------------------------------
	--I/O Buffer: DIO_58
	---------------------------------------------------------------
	IOBUF_58 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(58),
			IO => aDio(58),
			I  => aDio_out(58),
			T  => aDio_enable_n(58));

	---------------------------------------------------------------
	--I/O Buffer: DIO_58_N
	---------------------------------------------------------------
	IOBUF_58_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(58),
			IO => aDio_n(58),
			I  => aDio_n_out(58),
			T  => aDio_n_enable_n(58));

	---------------------------------------------------------------
	--I/O Buffer: DIO_59_N
	---------------------------------------------------------------
	IOBUF_59_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(59),
			IO => aDio_n(59),
			I  => aDio_n_out(59),
			T  => aDio_n_enable_n(59));

	---------------------------------------------------------------
	--I/O Buffer: DIO_61_N
	---------------------------------------------------------------
	IOBUF_61_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(61),
			IO => aDio_n(61),
			I  => aDio_n_out(61),
			T  => aDio_n_enable_n(61));

	---------------------------------------------------------------
	--I/O Buffer: DIO_62_N
	---------------------------------------------------------------
	IOBUF_62_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(62),
			IO => aDio_n(62),
			I  => aDio_n_out(62),
			T  => aDio_n_enable_n(62));

	---------------------------------------------------------------
	--I/O Buffer: DIO_63
	---------------------------------------------------------------
	IOBUF_63 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(63),
			IO => aDio(63),
			I  => aDio_out(63),
			T  => aDio_enable_n(63));

	---------------------------------------------------------------
	--I/O Buffer: DIO_63_N
	---------------------------------------------------------------
	IOBUF_63_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(63),
			IO => aDio_n(63),
			I  => aDio_n_out(63),
			T  => aDio_n_enable_n(63));

	---------------------------------------------------------------
	--I/O Buffer: DIO_70_N
	---------------------------------------------------------------
	IOBUF_70_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(70),
			IO => aDio_n(70),
			I  => aDio_n_out(70),
			T  => aDio_n_enable_n(70));

	---------------------------------------------------------------
	--I/O Buffer: DIO_71
	---------------------------------------------------------------
	IOBUF_71 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(71),
			IO => aDio(71),
			I  => aDio_out(71),
			T  => aDio_enable_n(71));

	---------------------------------------------------------------
	--I/O Buffer: DIO_71_N
	---------------------------------------------------------------
	IOBUF_71_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(71),
			IO => aDio_n(71),
			I  => aDio_n_out(71),
			T  => aDio_n_enable_n(71));

	---------------------------------------------------------------
	--I/O Buffer: DIO_72
	---------------------------------------------------------------
	IOBUF_72 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(72),
			IO => aDio(72),
			I  => aDio_out(72),
			T  => aDio_enable_n(72));

	---------------------------------------------------------------
	--I/O Buffer: DIO_72_N
	---------------------------------------------------------------
	IOBUF_72_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(72),
			IO => aDio_n(72),
			I  => aDio_n_out(72),
			T  => aDio_n_enable_n(72));

	---------------------------------------------------------------
	--I/O Buffer: DIO_73
	---------------------------------------------------------------
	IOBUF_73 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(73),
			IO => aDio(73),
			I  => aDio_out(73),
			T  => aDio_enable_n(73));

	---------------------------------------------------------------
	--I/O Buffer: DIO_73_N
	---------------------------------------------------------------
	IOBUF_73_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(73),
			IO => aDio_n(73),
			I  => aDio_n_out(73),
			T  => aDio_n_enable_n(73));

	---------------------------------------------------------------
	--I/O Buffer: DIO_74
	---------------------------------------------------------------
	IOBUF_74 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(74),
			IO => aDio(74),
			I  => aDio_out(74),
			T  => aDio_enable_n(74));

	---------------------------------------------------------------
	--I/O Buffer: DIO_74_N
	---------------------------------------------------------------
	IOBUF_74_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(74),
			IO => aDio_n(74),
			I  => aDio_n_out(74),
			T  => aDio_n_enable_n(74));

	---------------------------------------------------------------
	--I/O Buffer: DIO_75
	---------------------------------------------------------------
	IOBUF_75 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(75),
			IO => aDio(75),
			I  => aDio_out(75),
			T  => aDio_enable_n(75));

	---------------------------------------------------------------
	--I/O Buffer: DIO_75_N
	---------------------------------------------------------------
	IOBUF_75_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(75),
			IO => aDio_n(75),
			I  => aDio_n_out(75),
			T  => aDio_n_enable_n(75));

	---------------------------------------------------------------
	--I/O Buffer: DIO_76
	---------------------------------------------------------------
	IOBUF_76 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(76),
			IO => aDio(76),
			I  => aDio_out(76),
			T  => aDio_enable_n(76));

	---------------------------------------------------------------
	--I/O Buffer: DIO_76_N
	---------------------------------------------------------------
	IOBUF_76_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(76),
			IO => aDio_n(76),
			I  => aDio_n_out(76),
			T  => aDio_n_enable_n(76));

	---------------------------------------------------------------
	--I/O Buffer: DIO_77
	---------------------------------------------------------------
	IOBUF_77 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(77),
			IO => aDio(77),
			I  => aDio_out(77),
			T  => aDio_enable_n(77));

	---------------------------------------------------------------
	--I/O Buffer: DIO_77_N
	---------------------------------------------------------------
	IOBUF_77_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(77),
			IO => aDio_n(77),
			I  => aDio_n_out(77),
			T  => aDio_n_enable_n(77));

	---------------------------------------------------------------
	--I/O Buffer: DIO_78
	---------------------------------------------------------------
	IOBUF_78 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(78),
			IO => aDio(78),
			I  => aDio_out(78),
			T  => aDio_enable_n(78));

	---------------------------------------------------------------
	--I/O Buffer: DIO_78_N
	---------------------------------------------------------------
	IOBUF_78_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(78),
			IO => aDio_n(78),
			I  => aDio_n_out(78),
			T  => aDio_n_enable_n(78));

	---------------------------------------------------------------
	--I/O Buffer: DIO_79
	---------------------------------------------------------------
	IOBUF_79 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(79),
			IO => aDio(79),
			I  => aDio_out(79),
			T  => aDio_enable_n(79));

	---------------------------------------------------------------
	--I/O Buffer: DIO_79_N
	---------------------------------------------------------------
	IOBUF_79_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(79),
			IO => aDio_n(79),
			I  => aDio_n_out(79),
			T  => aDio_n_enable_n(79));

	---------------------------------------------------------------
	--I/O Buffer: DIO_80
	---------------------------------------------------------------
	IOBUF_80 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(80),
			IO => aDio(80),
			I  => aDio_out(80),
			T  => aDio_enable_n(80));

	---------------------------------------------------------------
	--I/O Buffer: DIO_80_N
	---------------------------------------------------------------
	IOBUF_80_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(80),
			IO => aDio_n(80),
			I  => aDio_n_out(80),
			T  => aDio_n_enable_n(80));

	---------------------------------------------------------------
	--I/O Buffer: DIO_81
	---------------------------------------------------------------
	IOBUF_81 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(81),
			IO => aDio(81),
			I  => aDio_out(81),
			T  => aDio_enable_n(81));

	---------------------------------------------------------------
	--I/O Buffer: DIO_81_N
	---------------------------------------------------------------
	IOBUF_81_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(81),
			IO => aDio_n(81),
			I  => aDio_n_out(81),
			T  => aDio_n_enable_n(81));

	---------------------------------------------------------------
	--I/O Buffer: DIO_82
	---------------------------------------------------------------
	IOBUF_82 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(82),
			IO => aDio(82),
			I  => aDio_out(82),
			T  => aDio_enable_n(82));

	---------------------------------------------------------------
	--I/O Buffer: DIO_82_N
	---------------------------------------------------------------
	IOBUF_82_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(82),
			IO => aDio_n(82),
			I  => aDio_n_out(82),
			T  => aDio_n_enable_n(82));

	---------------------------------------------------------------
	--I/O Buffer: DIO_83
	---------------------------------------------------------------
	IOBUF_83 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(83),
			IO => aDio(83),
			I  => aDio_out(83),
			T  => aDio_enable_n(83));

	---------------------------------------------------------------
	--I/O Buffer: DIO_83_N
	---------------------------------------------------------------
	IOBUF_83_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(83),
			IO => aDio_n(83),
			I  => aDio_n_out(83),
			T  => aDio_n_enable_n(83));

	---------------------------------------------------------------
	--I/O Buffer: DIO_84
	---------------------------------------------------------------
	IOBUF_84 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(84),
			IO => aDio(84),
			I  => aDio_out(84),
			T  => aDio_enable_n(84));

	---------------------------------------------------------------
	--I/O Buffer: DIO_84_N
	---------------------------------------------------------------
	IOBUF_84_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(84),
			IO => aDio_n(84),
			I  => aDio_n_out(84),
			T  => aDio_n_enable_n(84));

	---------------------------------------------------------------
	--I/O Buffer: DIO_85
	---------------------------------------------------------------
	IOBUF_85 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(85),
			IO => aDio(85),
			I  => aDio_out(85),
			T  => aDio_enable_n(85));

	---------------------------------------------------------------
	--I/O Buffer: DIO_85_N
	---------------------------------------------------------------
	IOBUF_85_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(85),
			IO => aDio_n(85),
			I  => aDio_n_out(85),
			T  => aDio_n_enable_n(85));

	---------------------------------------------------------------
	--I/O Buffer: DIO_86
	---------------------------------------------------------------
	IOBUF_86 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(86),
			IO => aDio(86),
			I  => aDio_out(86),
			T  => aDio_enable_n(86));

	---------------------------------------------------------------
	--I/O Buffer: DIO_86_N
	---------------------------------------------------------------
	IOBUF_86_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(86),
			IO => aDio_n(86),
			I  => aDio_n_out(86),
			T  => aDio_n_enable_n(86));

	---------------------------------------------------------------
	--I/O Buffer: DIO_87
	---------------------------------------------------------------
	IOBUF_87 : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_in(87),
			IO => aDio(87),
			I  => aDio_out(87),
			T  => aDio_enable_n(87));

	---------------------------------------------------------------
	--I/O Buffer: DIO_87_N
	---------------------------------------------------------------
	IOBUF_87_N : IOBUF
		generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
		port map (
			O  => aDio_n_in(87),
			IO => aDio_n(87),
			I  => aDio_n_out(87),
			T  => aDio_n_enable_n(87));


----------------------------------------------------------------
	--NOT CONNECTED <
----------------------------------------------------------------

end RTL;
