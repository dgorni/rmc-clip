#10MHz = 100ns -> multiply 4x -> 25ns -> Serialize 14x DDR (/7) -> 3.571ns
create_clock -period 1.786 -name AdcBitClkRaw [get_ports {aDio[39]}]
create_clock -period 100.000 -name FpgaClkIn [get_ports {aDio[37]}]

create_generated_clock -name AdcBitClkDivInt -source [get_pins window/theCLIPs/sbRIO_dash_9651_Socket_CLIP0/TopLrvrl14ddr_inst/AdcToplevel_I_AdcClock/AdcClock_I_Bufr/I] -master_clock [get_clocks AdcBitClkRaw] [get_pins window/theCLIPs/sbRIO_dash_9651_Socket_CLIP0/TopLrvrl14ddr_inst/AdcToplevel_I_AdcClock/AdcClock_I_Bufr/O]
set_false_path -from [get_ports {aDio[39]}] -to [get_pins window/theCLIPs/sbRIO_dash_9651_Socket_CLIP0/TopLrvrl14ddr_inst/AdcToplevel_I_AdcClock/AdcClock_I_Isrds_Master/D]
set_false_path -from [get_clocks AdcBitClkDivInt] -to [get_pins window/theCLIPs/sbRIO_dash_9651_Socket_CLIP0/TopLrvrl14ddr_inst/AdcToplevel_I_AdcClock/AdcClock_I_Isrds_Master/CE1]



set_false_path -from [get_clocks -of [get_pins window/theCLIPs/sbRIO_dash_9651_Socket_CLIP0/AdcSampleClkMux/I0]] -to [get_clocks -of [get_pins window/theCLIPs/sbRIO_dash_9651_Socket_CLIP0/AdcSampleClkMux/I1]]
set_false_path -from [get_clocks -of [get_pins window/theCLIPs/sbRIO_dash_9651_Socket_CLIP0/AdcSampleClkMux/I1]] -to [get_clocks -of [get_pins window/theCLIPs/sbRIO_dash_9651_Socket_CLIP0/AdcSampleClkMux/I0]]

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets -of [get_pins window/theCLIPs/sbRIO_dash_9651_Socket_CLIP0/clk_4x_inst/clkout1_buf/I0]]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets -of [get_pins window/theCLIPs/sbRIO_dash_9651_Socket_CLIP0/clk_4x_inst/clkin2_buf/I0]]