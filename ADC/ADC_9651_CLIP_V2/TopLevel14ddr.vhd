library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.std_logic_UNSIGNED.all;
	use IEEE.std_logic_textio.all;
	use std.textio.all;
library UNISIM;
	use UNISIM.VCOMPONENTS.all;

entity TopLrvrl14ddr is
	generic (
        C_AdcChnls          : integer := 8;
        C_AdcBits           : integer := 14;
        C_StatTaps          : integer := 16
	);
    port (
		DCLK_p	           		: in std_logic;
		DCLK_n	           		: in std_logic;  -- Not used.
		FCLK_p	           		: in std_logic;
		FCLK_n	           		: in std_logic;
		DATA_p	           		: in std_logic_vector(C_AdcChnls-1 downto 0);
		DATA_n	           		: in std_logic_vector(C_AdcChnls-1 downto 0);
        -- application connections
        SysRefClk          		: in  std_logic;		-- 200 MHz for IODELAYCTRL from application
        AdcIntrfcRst       		: in  std_logic;
        AdcIntrfcEna       		: in  std_logic;
        AdcReSync          		: in  std_logic;
        AdcBitSlip         		: in  std_logic;
        AdcBitClkAlgnWrn   		: out std_logic;
		AdcBitClkInvrtd    		: out std_logic;
		AdcBitClkDone      		: out std_logic;
		
		AdcBitClkIdlyCtrlRdy	: out std_logic;
			-- Data from the frame clock
        AdcFrmDataOut      		: out std_logic_vector(C_AdcBits-1 downto 0);
        	-- ADC memory data output
        AdcDatClkDiv       		: out std_logic;
		AdcDatOrderRev			: in  std_logic;
        AdcDatDataOut      		: out std_logic_vector((C_AdcBits*C_AdcChnls)-1 downto 0)
    );
end TopLrvrl14ddr;
-----------------------------------------------------------------------------------------------
-- Arcitecture section
-----------------------------------------------------------------------------------------------
architecture RTL of TopLrvrl14ddr  is
-----------------------------------------------------------------------------------------------
-- Component Instantiation
-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------
-- Constants, Signals and Attributes Declarations
-----------------------------------------------------------------------------------------------
-- Functions

-- Constants
constant Low : std_logic := '0';
constant High : std_logic := '1';
-- Signals
signal IntIdlyCtrlRdy		: std_logic;
signal IntBitSlip	      	: std_logic;
signal IntBitSlipVect       : std_logic_vector(C_AdcChnls downto 0);
signal IntRst				: std_logic;
signal IntEna_d				: std_logic;
signal IntEna				: std_logic;
--
signal IntBitClkDone		: std_logic;
signal IntClk				: std_logic;
signal IntClkDiv			: std_logic;

signal AdcFrmDataInt      	: std_logic_vector(C_AdcBits-1 downto 0);
signal AdcDatDataInt      	: std_logic_vector((C_AdcBits*C_AdcChnls)-1 downto 0);

    
attribute IODELAY_GROUP : STRING;
    attribute IODELAY_GROUP of delayctrl: label is "iserdese_14_ddr_group";
-----------------------------------------------------------------------------------------------
--
begin

    bits_slip : for bits_cnt in C_AdcChnls downto 0 generate
        IntBitSlipVect(bits_cnt) <= IntBitSlip;
    end generate bits_slip;
-----------------------------------------------------------------------------------------------
-- IDELAYCTRL
-- An IDELAYCTRL component must be used per IO-bank. Normally a ADC port fits a whole
-- IO-Bank. The number of IDELAYCTRL components should thus fit with the number of ADC port.
-- In case of this test design, two ADC ports fit into one IO-Bank, thus only one IDLEAYCTRL
-- component is needed.
-- Don not forget to hook the outputs of the IDELAYCTRL components correctly to the reset and
-- enable for each ADC block.
-- Don not forget to LOC the IDELAYCTRL components down.
-----------------------------------------------------------------------------------------------
-- IntRst and IntEna are the reset and enable signals to be used in the interface.
-- they are generated from the incoming system enable and reset.

-- IDELAYCTRL is needed for calibration
  delayctrl: IDELAYCTRL
  port map
  (
     RDY    => AdcBitClkIdlyCtrlRdy,
     REFCLK => SysRefClk,
     RST    => AdcIntrfcRst
  );

AdcToplevel_I_Fdce_BitSlip_0 : FDCE
	generic map (INIT => '0')
	port map (C => IntClkDiv, CE => High, CLR => IntRst, D => AdcBitSlip, Q => IntBitSlip);
AdcToplevel_I_Fdpe_Rst : FDPE
	generic map (INIT => '1')
	port map (C => IntClkDiv, CE => High, PRE => AdcIntrfcRst, D => Low, Q => IntRst);
AdcToplevel_I_Fdce_Ena_0 : FDCE
	generic map (INIT => '0')
	port map (C => IntClkDiv, CE => AdcIntrfcEna, CLR => IntRst, D => High, Q => IntEna_d);
AdcToplevel_I_Fdce_Ena_1 : FDCE
	generic map (INIT => '0')
	port map (C => IntClkDiv, CE => High, CLR => IntRst, D => IntEna_d, Q => IntEna);


AdcToplevel_I_AdcClock : entity work.AdcClock
generic map 
	(
        C_AdcBits       => C_AdcBits,       -- integer
        C_StatTaps      => C_StatTaps       -- integer
    )
port map (
	BitClk				=> DCLK_p,			-- in
	BitClkRst			=> IntRst,			-- in
	BitClkEna			=> IntEna,			-- in
	BitClkReSync		=> AdcReSync,		-- in
	BitClk_MonClkOut	=> IntClk,			-- out	-->--|---->----
	BitClk_MonClkIn		=> IntClk,			-- in	--<--|
	BitClk_RefClkOut	=> IntClkDiv,		-- out	-->----|-->----
	BitClk_RefClkIn		=> IntClkDiv,		-- in	--<----|
	BitClkAlignWarn 	=> AdcBitClkAlgnWrn,-- out
	BitClkInvrtd		=> AdcBitClkInvrtd,	-- out
	BitClkDone 			=> IntBitClkDone
);

AdcDatClkDiv <= IntClkDiv;
AdcBitClkDone <= IntBitClkDone;
-----------------------------------------------------------------------------------------------
-- WORD / FRAME CLOCK
-----------------------------------------------------------------------------------------------

iserdese14ddr_frame_inst : entity work.iserdese14ddr_frame
port map (
        data_in_from_pins_p     => FCLK_p,      
        data_in_from_pins_n     => FCLK_n,               
        data_in_to_device       => AdcFrmDataInt,
        in_delay_reset          => '0',       
        in_delay_data_ce        => '0',
        in_delay_data_inc       => '0',    
        in_delay_tap_in         => (others => '0'),
        in_delay_tap_out        => open,     
        bitslip                 => IntBitSlipVect(C_AdcChnls),      
        clk_in                  => IntClk,         
        clk_div_in              => IntClkDiv,      
        clock_enable            => IntBitClkDone,     
        io_reset                => IntRst                      
);

process (IntClkDiv)
begin
   if IntClkDiv'event and IntClkDiv='1' then
      AdcFrmDataOut <= AdcFrmDataInt;
   end if;
end process;

-----------------------------------------------------------------------------------------------
-- DATA INPUTS
-- Default the interface is set in BYTE and MSB first mode.
-----------------------------------------------------------------------------------------------

iserdese14ddr_data_inst : entity work.iserdese14ddr_data
port map (
        data_in_from_pins_p     => DATA_p,      
        data_in_from_pins_n     => DATA_n,       
		data_order_rev			=> AdcDatOrderRev,        
        data_in_to_device       => AdcDatDataInt,
        in_delay_reset          => '0',       
        in_delay_data_ce        => (others => '0'),
        in_delay_data_inc       => (others => '0'),    
        in_delay_tap_in         => (others => '0'),
        in_delay_tap_out        => open,    
        bitslip                 => IntBitSlipVect(C_AdcChnls-1 downto 0),      
        clk_in                  => IntClk,         
        clk_div_in              => IntClkDiv,      
        clock_enable            => IntBitClkDone,     
        io_reset                => IntRst          
);

process (IntClkDiv)
begin
   if IntClkDiv'event and IntClkDiv='1' then
      AdcDatDataOut <= AdcDatDataInt;
   end if;
end process;

end RTL;
