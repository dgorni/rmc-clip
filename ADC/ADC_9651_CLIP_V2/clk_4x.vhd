library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity clk_4x is
	port 
	(
		clk_out		: out STD_LOGIC;
		reset		: in  STD_LOGIC;
		locked		: out STD_LOGIC;
		clk_in		: in  STD_LOGIC;
		clk_in2		: in  STD_LOGIC;
		clk_in_sel	: in  STD_LOGIC;
		clk_in_buf	: out STD_LOGIC;
		clk_in2_buf	: out STD_LOGIC;
		
		MMCM_DADDR 			: in std_logic_vector(7 downto 0);
		MMCM_DEN 			: in std_logic;
		MMCM_DIN 			: in std_logic_vector(15 downto 0);
		MMCM_DOUT 			: out std_logic_vector(15 downto 0);
		MMCM_DRDY 			: out std_logic;
		MMCM_DWE 			: in std_logic;
		MMCM_DCLK 			: in std_logic
	);
end clk_4x;

architecture STRUCTURE of clk_4x is

	signal clk_in_int 	: STD_LOGIC;
	signal clk_in2_sel	: STD_LOGIC;
	signal clk_in2_int 	: STD_LOGIC;
	signal clk_in2_ibuf	: STD_LOGIC;
	signal clk_out_int 	: STD_LOGIC;
	signal clkfbout_buf	: STD_LOGIC;
	signal clkfbout 	: STD_LOGIC;
	signal locked_int	: STD_LOGIC;
	
	begin

	clkf_buf: BUFG
		port map 
		(
			I => clkfbout,
			O => clkfbout_buf
		);
		
	clkin2_ibufg: IBUFG
		generic map
		(
			IOSTANDARD => "LVCMOS25"
		)
		port map 
		(
			I => clk_in2,
			O => clk_in2_ibuf
		);
	
	clkin2_buf: BUFGMUX_CTRL
	port map 
	(
		S 	=> clk_in2_sel,
		I0	=> clk_in,			-- (S=0)
		I1 	=> clk_in2_ibuf,	-- (S=1)
		O	=> clk_in2_int
	);
	
	clk_in2_sel <= locked_int or (not clk_in_sel);
	clk_in2_buf <= clk_in2_int;
		
	clk_in_int <= clk_in;
	clk_in_buf <= clk_in;
		
	clkout1_buf: BUFGMUX_CTRL
	port map 
	(
		S 	=> locked_int,
		I0	=> clk_in,		-- (S=0)
		I1 	=> clk_out_int,	-- (S=1)
		O	=> clk_out
	);
	
	
	mmcm_adv_inst: MMCME2_ADV
		generic map
		(
			BANDWIDTH => "OPTIMIZED",
			CLKOUT4_CASCADE => false,
			COMPENSATION => "ZHOLD",
			STARTUP_WAIT => false,
			DIVCLK_DIVIDE => 1,
			CLKFBOUT_MULT_F => 64.000000, --50.000, --64.000000,
			CLKFBOUT_PHASE => 0.000000,
			CLKFBOUT_USE_FINE_PS => false,
			CLKOUT0_DIVIDE_F => 16.000000, --7.750, --16.000000,
			CLKOUT0_PHASE => 0.000000,
			CLKOUT0_DUTY_CYCLE => 0.500000,
			CLKOUT0_USE_FINE_PS => false,
			CLKIN1_PERIOD => 100.000000, --64.516 --100.000000
			CLKIN2_PERIOD => 100.000000
		  --CLKOUT1_DIVIDE => 1,
		  --CLKOUT1_DUTY_CYCLE => 0.500000,
		  --CLKOUT1_PHASE => 0.000000,
		  --CLKOUT1_USE_FINE_PS => false,
		  --CLKOUT2_DIVIDE => 1,
		  --CLKOUT2_DUTY_CYCLE => 0.500000,
		  --CLKOUT2_PHASE => 0.000000,
		  --CLKOUT2_USE_FINE_PS => false,
		  --CLKOUT3_DIVIDE => 1,
		  --CLKOUT3_DUTY_CYCLE => 0.500000,
		  --CLKOUT3_PHASE => 0.000000,
		  --CLKOUT3_USE_FINE_PS => false,
		  --CLKOUT4_DIVIDE => 1,
		  --CLKOUT4_DUTY_CYCLE => 0.500000,
		  --CLKOUT4_PHASE => 0.000000,
		  --CLKOUT4_USE_FINE_PS => false,
		  --CLKOUT5_DIVIDE => 1,
		  --CLKOUT5_DUTY_CYCLE => 0.500000,
		  --CLKOUT5_PHASE => 0.000000,
		  --CLKOUT5_USE_FINE_PS => false,
		  --CLKOUT6_DIVIDE => 1,
		  --CLKOUT6_DUTY_CYCLE => 0.500000,
		  --CLKOUT6_PHASE => 0.000000,
		  --CLKOUT6_USE_FINE_PS => false,
		  --IS_CLKINSEL_INVERTED => '0',
		  --IS_PSEN_INVERTED => '0',
		  --IS_PSINCDEC_INVERTED => '0',
		  --IS_PWRDWN_INVERTED => '0',
		  --IS_RST_INVERTED => '0',
		  --REF_JITTER1 => 0.010000,
		  --REF_JITTER2 => 0.010000,
		  --SS_EN => "FALSE",
		  --SS_MODE => "CENTER_HIGH",
		  --SS_MOD_PERIOD => 10000,
		)
		port map 
			(
			-- OUT
			CLKFBOUT 	=> clkfbout,
			CLKFBOUTB 	=> open,
			CLKOUT0 	=> clk_out_int,
			CLKOUT0B 	=> open,
			CLKOUT1 	=> open,
			CLKOUT1B 	=> open,
			CLKOUT2 	=> open,
			CLKOUT2B 	=> open,
			CLKOUT3 	=> open,
			CLKOUT3B 	=> open,
			CLKOUT4 	=> open,
			CLKOUT5 	=> open,
			CLKOUT6 	=> open,
			-- IN
			CLKFBIN 	=> clkfbout_buf,
			CLKIN1 		=> clk_in_int,
			CLKIN2 		=> clk_in2_int,
			-- TIE H - CLKIN1
			CLKINSEL 	=> clk_in_sel,
			-- DYNAMIC RECONFIG
			DADDR(6 downto 0)	=> MMCM_DADDR(6 downto 0), --(6 downto 0)
			DCLK 		=> MMCM_DCLK,
			DEN 		=> MMCM_DEN, -- (15 downto 0)
			DI 			=> MMCM_DIN, -- (15 downto 0)
			DO 			=> MMCM_DOUT,
			DRDY 		=> MMCM_DRDY,
			DWE 		=> MMCM_DWE,
			-- DYNAMIC PHASE SHIFT
			PSCLK 		=> '0',
			PSEN 		=> '0',
			PSINCDEC 	=> '0',
			PSDONE 		=> open,
			-- others
			LOCKED 			=> locked_int,
			CLKFBSTOPPED 	=> open,
			CLKINSTOPPED 	=> open,
			PWRDWN => '0',
			RST => reset
		);
		
	locked <= locked_int;
		
end STRUCTURE;
