library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.std_logic_UNSIGNED.all;
library UNISIM;
	use UNISIM.VCOMPONENTS.all;

entity iserdese14ddr_data is
    generic
    (
        -- width of the data for the system
        SYS_W : integer := 8;
        -- width of the data for the device
        DEV_W : integer := 112;
        OnChipLvdsTerm : boolean := TRUE
    );
    port
    (
        -- From the system into the device
        data_in_from_pins_p : in std_logic_vector(SYS_W-1 downto 0);
        data_in_from_pins_n : in std_logic_vector(SYS_W-1 downto 0);
		data_order_rev		: in std_logic;
        data_in_to_device   : out std_logic_vector(DEV_W-1 downto 0);
        in_delay_reset      : in std_logic;                                  -- Active high synchronous reset for input delay
        in_delay_data_ce    : in std_logic_vector(SYS_W-1 downto 0);       -- Enable signal for delay
        in_delay_data_inc   : in std_logic_vector(SYS_W-1 downto 0);      -- Delay increment (high), decrement (low) signal
        in_delay_tap_in     : in std_logic_vector(5*SYS_W-1 downto 0);      -- Dynamically loadable delay tap value for input delay
        in_delay_tap_out    : out std_logic_vector(5*SYS_W-1 downto 0);    -- Delay tap value for monitoring input delay
        bitslip             : in std_logic_vector(SYS_W-1 downto 0);                -- Bitslip module is enabled in NETWORKING mode -- User should tie it to '0' if not needed
        clk_in              : in std_logic;                                          -- Fast clock input from PLL/MMCM
        clk_div_in          : in std_logic;                                      -- Slow clock input from PLL/MMCM
        clock_enable        : in std_logic;
        io_reset            : in std_logic
    );
end iserdese14ddr_data;

architecture RTL of iserdese14ddr_data is

    constant num_serial_bits : integer := DEV_W/SYS_W;
    -- Signal declarations
    ----------------------------------
    -- After the buffer
    signal data_in_from_pins_int 	: std_logic_vector(SYS_W-1 downto 0);
    -- Between the delay and serdes
    signal data_in_from_pins_delay 	: std_logic_vector(SYS_W-1 downto 0);
    signal in_delay_ce 				: std_logic_vector(SYS_W-1 downto 0);
    signal in_delay_inc_dec 		: std_logic_vector(SYS_W-1 downto 0);
    type t_in_delay_tap is array (0 to SYS_W-1) of std_logic_vector(4 downto 0);
    signal in_delay_tap_in_int 		: t_in_delay_tap;        -- fills in starting with 0
    signal in_delay_tap_out_int 	: t_in_delay_tap;       -- fills in starting with 0
    signal ref_clock_bufg 			: std_logic;
    -- Array to use intermediately from the serdes to the internal
    --  devices. bus "0" is the leftmost bus
    type t_iserdes_q is array (SYS_W-1 downto 0) of std_logic_vector(0 to 13);
    signal iserdes_q 				: t_iserdes_q;                 -- fills in starting with 0
    signal iserdes_q_rev 			: t_iserdes_q;                 -- fills in starting with 0

	
     -- local wire only for use in this generate loop
     signal cascade_shift 			: std_logic_vector(SYS_W-1 downto 0);
     type t_icascade is array (0 to SYS_W-1) of std_logic_vector(SYS_W-1 downto 0);
     signal icascade1 				: t_icascade;
     signal icascade2 				: t_icascade;
     signal clk_in_int_inv 			: std_logic_vector(SYS_W-1 downto 0);
    
    begin
    
    in_delay_ce <= in_delay_data_ce(7) & in_delay_data_ce(6) & in_delay_data_ce(5) & in_delay_data_ce(4) & in_delay_data_ce(3) & in_delay_data_ce(2) & in_delay_data_ce(1) & in_delay_data_ce(0);
    in_delay_inc_dec <= in_delay_data_inc(7) & in_delay_data_inc(6) & in_delay_data_inc(5) & in_delay_data_inc(4) & in_delay_data_inc(3) & in_delay_data_inc(2) & in_delay_data_inc(1) & in_delay_data_inc(0);

    in_delay_tap_in_int(0) <= in_delay_tap_in(5*(0 + 1) -1 downto 5*(0)) ;
    in_delay_tap_in_int(1) <= in_delay_tap_in(5*(1 + 1) -1 downto 5*(1)) ;
    in_delay_tap_in_int(2) <= in_delay_tap_in(5*(2 + 1) -1 downto 5*(2)) ;
    in_delay_tap_in_int(3) <= in_delay_tap_in(5*(3 + 1) -1 downto 5*(3)) ;
    in_delay_tap_in_int(4) <= in_delay_tap_in(5*(4 + 1) -1 downto 5*(4)) ;
    in_delay_tap_in_int(5) <= in_delay_tap_in(5*(5 + 1) -1 downto 5*(5)) ;
    in_delay_tap_in_int(6) <= in_delay_tap_in(5*(6 + 1) -1 downto 5*(6)) ;
    in_delay_tap_in_int(7) <= in_delay_tap_in(5*(7 + 1) -1 downto 5*(7)) ;
    in_delay_tap_out(5*(0 + 1) -1 downto 5*(0)) <= in_delay_tap_out_int(0);
    in_delay_tap_out(5*(1 + 1) -1 downto 5*(1)) <= in_delay_tap_out_int(1);
    in_delay_tap_out(5*(2 + 1) -1 downto 5*(2)) <= in_delay_tap_out_int(2);
    in_delay_tap_out(5*(3 + 1) -1 downto 5*(3)) <= in_delay_tap_out_int(3);
    in_delay_tap_out(5*(4 + 1) -1 downto 5*(4)) <= in_delay_tap_out_int(4);
    in_delay_tap_out(5*(5 + 1) -1 downto 5*(5)) <= in_delay_tap_out_int(5);
    in_delay_tap_out(5*(6 + 1) -1 downto 5*(6)) <= in_delay_tap_out_int(6);
    in_delay_tap_out(5*(7 + 1) -1 downto 5*(7)) <= in_delay_tap_out_int(7);
   -- Create the clock logic


  -- We have multiple bits- step over every bit, instantiating the required elements
    pins : for pin_count in SYS_W-1 downto 0 generate

    attribute IODELAY_GROUP : STRING;
    attribute IODELAY_GROUP of idelaye2_bus : label is "iserdese_14_ddr_group";    
    
    begin
    -- Instantiate the buffers
    ----------------------------------
    -- Instantiate a buffer for every bit of the data bus
    ibufds_inst : IBUFDS
        generic map
        (
            DIFF_TERM => OnChipLvdsTerm, -- Differential termination
            IOSTANDARD => "LVDS_25")
        port map
        (          
            I  => data_in_from_pins_p(pin_count),
            IB => data_in_from_pins_n(pin_count),
            O  => data_in_from_pins_int(pin_count)
        );

    -- Instantiate the delay primitive
    -----------------------------------
    idelaye2_bus : IDELAYE2
        generic map
        (
            CINVCTRL_SEL          => "FALSE",       -- TRUE, FALSE
            DELAY_SRC             => "IDATAIN",     -- IDATAIN, DATAIN
            HIGH_PERFORMANCE_MODE => "FALSE",       -- TRUE, FALSE
            IDELAY_TYPE           => "VAR_LOAD",    -- FIXED, VARIABLE, or VAR_LOADABLE
            IDELAY_VALUE          => 0,             -- 0 to 31
            REFCLK_FREQUENCY      => 200.0,
            PIPE_SEL              => "FALSE",
            SIGNAL_PATTERN        => "DATA")        -- CLOCK, DATA
        port map
        (
            DATAOUT               => data_in_from_pins_delay(pin_count),
            DATAIN                => '0',                               -- Data from FPGA logic
            C                     => clk_div_in,
            CE                    => in_delay_ce(pin_count),            -- in_delay_data_ce,
            INC                   => in_delay_inc_dec(pin_count),       -- in_delay_data_inc,
            IDATAIN               => data_in_from_pins_int(pin_count),  -- Driven by IOB
            LD                    => in_delay_reset,
            REGRST                => io_reset,
            LDPIPEEN              => '0',
            CNTVALUEIN            => in_delay_tap_in_int(pin_count),    -- in_delay_tap_in,
            CNTVALUEOUT           => in_delay_tap_out_int(pin_count),  -- in_delay_tap_out,
            CINVCTRL              => '0'
        );


     -- Instantiate the serdes primitive
     ----------------------------------


     clk_in_int_inv(pin_count) <= not clk_in;

     -- declare the iserdes
    iserdese2_master: ISERDESE2
    generic map 
        (
            DATA_RATE        => "DDR",
            DATA_WIDTH       => 14,
            INTERFACE_TYPE   => "NETWORKING", 
            DYN_CLKDIV_INV_EN=> "FALSE",
            DYN_CLK_INV_EN   => "FALSE",
            NUM_CE           => 2,
            OFB_USED         => "FALSE",
            IOBDELAY         => "IFD",                                -- Use input at DDLY to output the data on Q
            SERDES_MODE      => "MASTER"
          )                 
    port map               
        (                 
            Q1               => iserdes_q(pin_count)(0),
            Q2               => iserdes_q(pin_count)(1),
            Q3               => iserdes_q(pin_count)(2),
            Q4               => iserdes_q(pin_count)(3),
            Q5               => iserdes_q(pin_count)(4),
            Q6               => iserdes_q(pin_count)(5),
            Q7               => iserdes_q(pin_count)(6),
            Q8               => iserdes_q(pin_count)(7),
            SHIFTOUT1        => icascade1(pin_count)(pin_count),                -- Cascade connections to Slave ISERDES
            SHIFTOUT2        => icascade2(pin_count)(pin_count),                -- Cascade connections to Slave ISERDES
            BITSLIP          => bitslip(pin_count),                             -- 1-bit Invoke Bitslip. This can be used with any DATA_WIDTH, cascaded or not.  -- The amount of BITSLIP is fixed by the DATA_WIDTH selection. --In DDR mode, every Bitslip operation causes the output pattern to alternate between a shift right by one and shift left by three."
            CE1              => clock_enable,                        -- 1-bit Clock enable input
            CE2              => clock_enable,                        -- 1-bit Clock enable input
            CLK              => clk_in,                              -- Fast clock driven by MMCM
            CLKB             => clk_in_int_inv(pin_count),                      -- Locally inverted fast 
            CLKDIV           => clk_div_in,                          -- Slow clock from MMCM
            CLKDIVP          => '0',                                --
            D                => '0',                                -- 1-bit Input signal from IOB
            DDLY             => data_in_from_pins_delay(pin_count),  -- 1-bit Input from Input Delay component 
            RST              => io_reset,                            -- 1-bit Asynchronous reset only.
            SHIFTIN1         => '0',                                -- unused connections
            SHIFTIN2          =>'0',
            DYNCLKDIVSEL     => '0',
            DYNCLKSEL        => '0',
            OFB              => '0',
            OCLK             => '0',
            OCLKB            => '0',
            O                => open                                    -- unregistered output of ISERDESE1
          );                                   
    
    iserdese2_slave : ISERDESE2
    generic map 
    (
        DATA_RATE         => "DDR",
        DATA_WIDTH        => 14,
        INTERFACE_TYPE    => "NETWORKING",
        DYN_CLKDIV_INV_EN => "FALSE",
        DYN_CLK_INV_EN    => "FALSE",
        NUM_CE            => 2,
        OFB_USED          => "FALSE",
        IOBDELAY          => "IFD",               -- Use input at DDLY to output the data on Q
        SERDES_MODE       => "SLAVE"
    )                      
    port map             
    (                      
        Q1                => open,
        Q2                => open,
        Q3                => iserdes_q(pin_count)(8),
        Q4                => iserdes_q(pin_count)(9),
        Q5                => iserdes_q(pin_count)(10),
        Q6                => iserdes_q(pin_count)(11),
        Q7                => iserdes_q(pin_count)(12),
        Q8                => iserdes_q(pin_count)(13),
        SHIFTOUT1         => open,
        SHIFTOUT2         => open,
        SHIFTIN1          => icascade1(pin_count)(pin_count),  -- Cascade connection with Master ISERDES
        SHIFTIN2          => icascade2(pin_count)(pin_count),  -- Cascade connection with Master ISERDES
        BITSLIP           => bitslip(pin_count),               -- 1-bit Invoke Bitslip. This can be used with any DATA_WIDTH, cascaded or not. -- The amount of BITSLIP is fixed by the DATA_WIDTH selection .
        CE1               => clock_enable,          -- 1-bit Clock enable input
        CE2               => clock_enable,          -- 1-bit Clock enable input 
        CLK               => clk_in,                -- Fast clock driven by MMCM
        CLKB              => clk_in_int_inv(pin_count),        -- Locally inverted fast clock
        CLKDIV            => clk_div_in,            -- Slow clock driven by MMCM
        CLKDIVP           => '0',                  --
        D                 => '0',                  -- Slave ISERDES. No need to connect D, DDLY
        DDLY              => '0',                  --
        RST               => io_reset,              -- 1-bit Asynchronous reset only.  -- unused connections
        DYNCLKDIVSEL      => '0',
        DYNCLKSEL         => '0',
        OFB               => '0',
        OCLK              => '0',
        OCLKB             => '0',
        O                 => open -- unregistered output of ISERDESE1
      );                     
    -- Concatenate the serdes outputs together. Keep the timesliced
    --   bits together, and placing the earliest bits on the right
    --   ie, if data comes in 0, 1, 2, 3, 4, 5, 6, 7, ...
    --       the output will be 3210, 7654, ...
    ---- ---------------------------------------------------------
    
       -- in_slices : for slice_count in num_serial_bits-1 downto 0 generate
            -- This places the first data in time on the right
         --   data_in_to_device((slice_count+1)*SYS_W-1 downto slice_count*SYS_W) <= iserdes_q(num_serial_bits-slice_count-1);
            -- To place the first data in time on the left, use the
            --   following code, instead
            -- data_in_to_device(slice_count*SYS_W+:SYS_W) <= iserdes_q[slice_count];
       -- end generate in_slices;
  
	reverse_order: for i in 0 to 13 generate
		iserdes_q_rev(pin_count)(i) <= iserdes_q(pin_count)(i) when data_order_rev='0' else iserdes_q(pin_count)(13-i);
	end generate reverse_order;
  
  end generate pins;
  
  
	  
	data_in_to_device(13 downto 0)		<= iserdes_q_rev(0);
	data_in_to_device(27 downto 14)		<= iserdes_q_rev(1);
	data_in_to_device(41 downto 28)		<= iserdes_q_rev(2);
	data_in_to_device(55 downto 42)		<= iserdes_q_rev(3);
	data_in_to_device(69 downto 56)		<= iserdes_q_rev(4);
	data_in_to_device(83 downto 70)		<= iserdes_q_rev(5);
	data_in_to_device(97 downto 84)		<= iserdes_q_rev(6);
	data_in_to_device(111 downto 98)	<= iserdes_q_rev(7);
  
---- NO ODELAY
  
  
end RTL;
