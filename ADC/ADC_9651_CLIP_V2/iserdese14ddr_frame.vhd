library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.std_logic_UNSIGNED.all;
library UNISIM;
	use UNISIM.VCOMPONENTS.all;

entity iserdese14ddr_frame is
    generic
    (
        -- width of the data for the device
        DEV_W : integer := 14;
        OnChipLvdsTerm : boolean := TRUE
    );
    port
    (
        -- From the system into the device
        data_in_from_pins_p : in std_logic;
        data_in_from_pins_n : in std_logic;
        data_in_to_device : out std_logic_vector(DEV_W-1 downto 0);
        in_delay_reset : in std_logic;                                  -- Active high synchronous reset for input delay
        in_delay_data_ce : in std_logic;       -- Enable signal for delay
        in_delay_data_inc : in std_logic;      -- Delay increment (high), decrement (low) signal
        in_delay_tap_in : in std_logic_vector(4 downto 0);      -- Dynamically loadable delay tap value for input delay
        in_delay_tap_out : out std_logic_vector(4 downto 0);    -- Delay tap value for monitoring input delay
--        delay_locked : out std_logic;                                   -- Locked signal from IDELAYCTRL
--      ref_clock : in std_logic;                                       -- Reference clock for IDELAYCTRL. Has to come from BUFG.
        bitslip : in std_logic;                -- Bitslip module is enabled in NETWORKING mode -- User should tie it to '0' if not needed
        clk_in : in std_logic;                                          -- Fast clock input from PLL/MMCM
        clk_div_in : in std_logic;                                      -- Slow clock input from PLL/MMCM
        clock_enable : in std_logic;
        io_reset : in std_logic
    );
end iserdese14ddr_frame;

architecture RTL of iserdese14ddr_frame is

    constant num_serial_bits : integer := DEV_W/1;
    -- Signal declarations
    ----------------------------------
    -- After the buffer
    signal data_in_from_pins_int : std_logic;
    -- Between the delay and serdes
    signal data_in_from_pins_delay : std_logic;
    signal in_delay_ce : std_logic;
    signal in_delay_inc_dec : std_logic;
    signal in_delay_tap_in_int : std_logic_vector(4 downto 0);        -- fills in starting with 0
    signal in_delay_tap_out_int : std_logic_vector(4 downto 0);       -- fills in starting with 0
    signal ref_clock_bufg : std_logic;
    -- Array to use intermediately from the serdes to the internal
    --  devices. bus "0" is the leftmost bus
    signal iserdes_q : std_logic_vector(13 downto 0);                 -- fills in starting with 0
    
     -- local wire only for use in this generate loop
     signal cascade_shift : std_logic;
     signal icascade1 : std_logic;
     signal icascade2 : std_logic;
     signal clk_in_int_inv : std_logic;
    
     attribute IODELAY_GROUP : STRING;
     attribute IODELAY_GROUP of idelaye2_bus : label is "iserdese_14_ddr_group";    
    
    --attribute IODELAY_GROUP : STRING;
    --attribute IODELAY_GROUP of delayctrl: label is "iserdese_14_ddr_group";
    
    begin
    
    in_delay_ce <= in_delay_data_ce;
    in_delay_inc_dec <= in_delay_data_inc;

    in_delay_tap_in_int(4 downto 0) <= in_delay_tap_in(4 downto 0) ;
    in_delay_tap_out(4 downto 0) <= in_delay_tap_out_int(4 downto 0);
   -- Create the clock logic


  -- We have multiple bits- step over every bit, instantiating the required elements
  
    -- Instantiate the buffers
    ----------------------------------
    -- Instantiate a buffer for every bit of the data bus
    ibufds_inst : IBUFDS
        generic map
        (
            DIFF_TERM => OnChipLvdsTerm, -- Differential termination
            IOSTANDARD => "LVDS_25")
        port map
        (          
            I  => data_in_from_pins_p,
            IB => data_in_from_pins_n,
            O  => data_in_from_pins_int
        );

    -- Instantiate the delay primitive
    -----------------------------------
    idelaye2_bus : IDELAYE2
        generic map
        (
            CINVCTRL_SEL          => "FALSE",       -- TRUE, FALSE
            DELAY_SRC             => "IDATAIN",     -- IDATAIN, DATAIN
            HIGH_PERFORMANCE_MODE => "FALSE",       -- TRUE, FALSE
            IDELAY_TYPE           => "VAR_LOAD",    -- FIXED, VARIABLE, or VAR_LOADABLE
            IDELAY_VALUE          => 0,             -- 0 to 31
            REFCLK_FREQUENCY      => 200.0,
            PIPE_SEL              => "FALSE",
            SIGNAL_PATTERN        => "DATA")        -- CLOCK, DATA
        port map
        (
            DATAOUT               => data_in_from_pins_delay,
            DATAIN                => '0',                               -- Data from FPGA logic
            C                     => clk_div_in,
            CE                    => in_delay_ce,            -- in_delay_data_ce,
            INC                   => in_delay_inc_dec,       -- in_delay_data_inc,
            IDATAIN               => data_in_from_pins_int,  -- Driven by IOB
            LD                    => in_delay_reset,
            REGRST                => io_reset,
            LDPIPEEN              => '0',
            CNTVALUEIN            => in_delay_tap_in_int,    -- in_delay_tap_in,
            CNTVALUEOUT           => in_delay_tap_out_int,  -- in_delay_tap_out,
            CINVCTRL              => '0'
        );
        
       -- Instantiate the serdes primitive
     ----------------------------------


     clk_in_int_inv <= not clk_in;

     -- declare the iserdes
    iserdese2_master: ISERDESE2
    generic map 
        (
            DATA_RATE        => "DDR",
            DATA_WIDTH       => 14,
            INTERFACE_TYPE   => "NETWORKING", 
            DYN_CLKDIV_INV_EN=> "FALSE",
            DYN_CLK_INV_EN   => "FALSE",
            NUM_CE           => 2,
            OFB_USED         => "FALSE",
            IOBDELAY         => "IFD",                                -- Use input at DDLY to output the data on Q
            SERDES_MODE      => "MASTER"
          )                 
    port map               
        (                 
            Q1               => iserdes_q(0),
            Q2               => iserdes_q(1),
            Q3               => iserdes_q(2),
            Q4               => iserdes_q(3),
            Q5               => iserdes_q(4),
            Q6               => iserdes_q(5),
            Q7               => iserdes_q(6),
            Q8               => iserdes_q(7),
            SHIFTOUT1        => icascade1,                -- Cascade connections to Slave ISERDES
            SHIFTOUT2        => icascade2,                -- Cascade connections to Slave ISERDES
            BITSLIP          => bitslip,                             -- 1-bit Invoke Bitslip. This can be used with any DATA_WIDTH, cascaded or not.  -- The amount of BITSLIP is fixed by the DATA_WIDTH selection.
            CE1              => clock_enable,                        -- 1-bit Clock enable input
            CE2              => clock_enable,                        -- 1-bit Clock enable input
            CLK              => clk_in,                              -- Fast clock driven by MMCM
            CLKB             => clk_in_int_inv,                      -- Locally inverted fast 
            CLKDIV           => clk_div_in,                          -- Slow clock from MMCM
            CLKDIVP          => '0',                                --
            D                => '0',                                -- 1-bit Input signal from IOB
            DDLY             => data_in_from_pins_delay,  -- 1-bit Input from Input Delay component 
            RST              => io_reset,                            -- 1-bit Asynchronous reset only.
            SHIFTIN1         => '0',                                -- unused connections
            SHIFTIN2          =>'0',
            DYNCLKDIVSEL     => '0',
            DYNCLKSEL        => '0',
            OFB              => '0',
            OCLK             => '0',
            OCLKB            => '0',
            O                => open                                    -- unregistered output of ISERDESE1
          );                                   
    
    iserdese2_slave : ISERDESE2
    generic map 
    (
        DATA_RATE         => "DDR",
        DATA_WIDTH        => 14,
        INTERFACE_TYPE    => "NETWORKING",
        DYN_CLKDIV_INV_EN => "FALSE",
        DYN_CLK_INV_EN    => "FALSE",
        NUM_CE            => 2,
        OFB_USED          => "FALSE",
        IOBDELAY          => "IFD",               -- Use input at DDLY to output the data on Q
        SERDES_MODE       => "SLAVE"
    )                      
    port map             
    (                      
        Q1                => open,
        Q2                => open,
        Q3                => iserdes_q(8),
        Q4                => iserdes_q(9),
        Q5                => iserdes_q(10),
        Q6                => iserdes_q(11),
        Q7                => iserdes_q(12),
        Q8                => iserdes_q(13),
        SHIFTOUT1         => open,
        SHIFTOUT2         => open,
        SHIFTIN1          => icascade1,  -- Cascade connection with Master ISERDES
        SHIFTIN2          => icascade2,  -- Cascade connection with Master ISERDES
        BITSLIP           => bitslip,               -- 1-bit Invoke Bitslip. This can be used with any DATA_WIDTH, cascaded or not. -- The amount of BITSLIP is fixed by the DATA_WIDTH selection .
        CE1               => clock_enable,          -- 1-bit Clock enable input
        CE2               => clock_enable,          -- 1-bit Clock enable input 
        CLK               => clk_in,                -- Fast clock driven by MMCM
        CLKB              => clk_in_int_inv,        -- Locally inverted fast clock
        CLKDIV            => clk_div_in,            -- Slow clock driven by MMCM
        CLKDIVP           => '0',                  --
        D                 => '0',                  -- Slave ISERDES. No need to connect D, DDLY
        DDLY              => '0',                  --
        RST               => io_reset,              -- 1-bit Asynchronous reset only.  -- unused connections
        DYNCLKDIVSEL      => '0',
        DYNCLKSEL         => '0',
        OFB               => '0',
        OCLK              => '0',
        OCLKB             => '0',
        O                 => open -- unregistered output of ISERDESE1
      );                     
    -- Concatenate the serdes outputs together. Keep the timesliced
    --   bits together, and placing the earliest bits on the right
    --   ie, if data comes in 0, 1, 2, 3, 4, 5, 6, 7, ...
    --       the output will be 3210, 7654, ...
    ---- ---------------------------------------------------------
		data_in_to_device <= iserdes_q (13 downto 0);
      --  in_slices : for slice_count in num_serial_bits-1 downto 0 generate
            -- This places the first data in time on the right
            --data_in_to_device(slice_count) <= iserdes_q(num_serial_bits-slice_count-1);
            -- To place the first data in time on the left, use the
            --   following code, instead
            -- data_in_to_device(slice_count*SYS_W+:SYS_W) <= iserdes_q[slice_count];
  --      end generate in_slices;
  
---- NO ODELAY

-- IDELAYCTRL is needed for calibration
--  delayctrl: IDELAYCTRL
--  port map
--  (
--     RDY    => delay_locked,
--     REFCLK => ref_clock_bufg,
--     RST    => io_reset
--  );

--  ref_clk_bufg : BUFG
--  port map
--  (
--    I => ref_clock,
--    O => ref_clock_bufg
--  );
  
  
end RTL;