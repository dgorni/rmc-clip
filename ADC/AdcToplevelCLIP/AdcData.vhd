-----------------------------------------------------------------------------------------------
-- � Copyright 2012, Xilinx, Inc. All rights reserved.
-- This file contains confidential and proprietary information of Xilinx, Inc. and is
-- protected under U.S. and international copyright and other intellectual property laws.
-----------------------------------------------------------------------------------------------
--
-- Disclaimer:
--		This disclaimer is not a license and does not grant any rights to the materials
--		distributed herewith. Except as otherwise provided in a valid license issued to you
--		by Xilinx, and to the maximum extent permitted by applicable law: (1) THESE MATERIALS
--		ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL
--		WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED
--		TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR
--		PURPOSE; and (2) Xilinx shall not be liable (whether in contract or tort, including
--		negligence, or under any other theory of liability) for any loss or damage of any
--		kind or nature related to, arising under or in connection with these materials,
--		including for any direct, or any indirect, special, incidental, or consequential
--		loss or damage (including loss of data, profits, goodwill, or any type of loss or
--		damage suffered as a result of any action brought by a third party) even if such
--		damage or loss was reasonably foreseeable or Xilinx had been advised of the
--		possibility of the same.
--
-- CRITICAL APPLICATIONS
--		Xilinx products are not designed or intended to be fail-safe, or for use in any
--		application requiring fail-safe performance, such as life-support or safety devices
--		or systems, Class III medical devices, nuclear facilities, applications related to
--		the deployment of airbags, or any other applications that could lead to death,
--		personal injury, or severe property or environmental damage (individually and
--		collectively, "Critical Applications"). Customer assumes the sole risk and
--		liability of any use of Xilinx products in Critical Applications, subject only to
--		applicable laws and regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.
--
--		Contact:    e-mail  hotline@xilinx.com        phone   + 1 800 255 7778
--   ____  ____
--  /   /\/   /
-- /___/  \  /   Vendor: 				Xilinx
-- \   \   \/    Version:               V0.03
--  \   \        Filename: 				AdcData.vhd
--  /   /        Date Last Modified:	12 Feb 2018
-- /___/   /\    Date Created:			18 Dec 2007
-- \   \  /  \
--  \___\/\___\
--
-- Device: 		Virtex-6, 7-Series
-- Author: 		Marc Defossez
-- Entity Name: AdcData
-- Purpose: 	2-channel ADC data receiver interface.
--				The output of this module is alwasy fprmatted in 32-bit.
--				When the interface is for a 12-bit ADC then the output is formatted as:
--				32 ---------- 16 , 15 ----------- 0
--				 0000 & (12-bit) ,  0000 & (12-bit)
--				When the interface is for 14-bit or 16-bit the the ouput is formatted as:
--				32 ---------- 16 , 15 ----------- 0
--				 (   16-bit   ) ,  (   16-bit    )
-- 				In 1-wire mode the 32-bit output shows two channels
--				In 2-wire mode the 32-bit output shows two words of the same channel.
--
-- Tools: 		Vivado_2017.3 or later
-- Limitations: none
--
-- Revision History:
-----------------------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
-----------------------------------------------------------------------------------------------
--
library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.std_logic_UNSIGNED.all;
library UNISIM;
	use UNISIM.VCOMPONENTS.all;
-----------------------------------------------------------------------------------------------
-- Entity pin description
-----------------------------------------------------------------------------------------------
entity AdcData is
	generic (
		C_AdcBits			: integer := 12;	-- Can be 12, 14 or 16           -- OK(12)
		C_AdcBytOrBitMode	: integer := 0;     -- 1 = BIT mode, 0 = BYTE mode,  -- OK(0)
		C_AdcMsbOrLsbFst	: integer := 0;     -- 0 = MSB first, 1 = LSB first  -- OK(0)
		C_AdcWireInt		: integer := 1		-- 1 = 1-wire, 2 = 2-wire.       -- OK(1)
	);
    port (
        DatD_n			: in std_logic;
		DatD_p			: in std_logic;
		DatClk			: in std_logic;
		DatClkDiv		: in std_logic;
		DatEna			: in std_logic;
		DatDone			: in std_logic;
		DatBitSlip_p	: in std_logic;
        DatBitSlip_n	: in std_logic;
        DatInvrtd		: in std_logic;
        DatReSync		: in std_logic;
		DatOut			: out std_logic_vector(15 downto 0)
    );
end AdcData;
-----------------------------------------------------------------------------------------------
-- Arcitecture section
-----------------------------------------------------------------------------------------------
architecture RTL of AdcData  is
-----------------------------------------------------------------------------------------------
-- Component Instantiation
-----------------------------------------------------------------------------------------------
-- Components are instantiated through library naming.
-----------------------------------------------------------------------------------------------
-- Constants, Signals and Attributes Declarations
-----------------------------------------------------------------------------------------------
-- Functions
-- In two wire mode a 12 bit ADC has 2 channels of 6 bits. The AdcBits stay at 12.
-- In two wire mode a 14 bit ADC has 2 channels of 8 bits. The AdcBits is set at 16.
-- In two wire mode a 16 bit ADC has 2 channels of 8 bits. The AdcBits stay at 16.
function DatBits (Bits : integer) return integer is
variable Temp : integer;
begin
	if (Bits = 12) then
		Temp := 12;
	elsif (Bits = 14) then
		Temp := 16;
	elsif (Bits = 16) then
		Temp := 16;
	end if;
return Temp;
end function DatBits;
-- Constants
constant IntIsrdsDataWidth : integer := DatBits(C_AdcBits)/2;
constant Low			: std_logic := '0';
constant High			: std_logic := '1';

signal IntDatSrdsOut : std_logic_vector(11 downto 0);
signal IntDatSrdsOutPrev : std_logic_vector(1 downto 1);
signal IntDatDoneEna : std_logic;

-----------------------------------------------------------------------------------------------
begin

IntDatDoneEna <= DatDone and DatEna;

--IntDatClk_n <= not DatClk;		-- CLOCK FOR N_side ISERDES
-- Above is commented because the ISERDESE2 has a generic IS_CLK_INVERTED set to '1'.
-- The CLK clock is inverted inside the ISERDESE2.
-----------------------------------------------------------------------------------------------
-- ISERDES for channel ZERO
-----------------------------------------------------------------------------------------------
AdcData_I_Isrds_D0_p : ISERDESE2
    generic map (
		SERDES_MODE			=> "MASTER",			-- string
		INTERFACE_TYPE		=> "NETWORKING",		-- string
		IOBDELAY			=> "NONE",				-- string
		DATA_RATE 			=> "SDR", 				-- string
		DATA_WIDTH 			=> IntIsrdsDataWidth,	-- integer <-- Number of bits
		DYN_CLKDIV_INV_EN	=> "FALSE", 			-- string
		DYN_CLK_INV_EN		=> "FALSE", 			-- string
		NUM_CE				=> 1, 					-- integer
		OFB_USED			=> "FALSE", 			-- string
        INIT_Q1             => '0',         -- bit;
        INIT_Q2             => '0',         -- bit;
        INIT_Q3             => '0',         -- bit;
        INIT_Q4             => '0',         -- bit;
        SRVAL_Q1            => '0',         -- bit;
        SRVAL_Q2            => '0',         -- bit;
        SRVAL_Q3            => '0',         -- bit;
        SRVAL_Q4            => '0',         -- bit;
        IS_CLKB_INVERTED    => '0',         -- CLKB clock input is NOT inverted
        IS_CLKDIVP_INVERTED => '0',         -- CLKDIVP input is NOT inverted
        IS_CLKDIV_INVERTED  => '0',         -- CLKDIV clock input is NOT inverted
        IS_CLK_INVERTED     => '0',         -- CLK clock input is NOT inverted
        IS_D_INVERTED       => '0',         -- D (data) input is NOT inverted
        IS_OCLKB_INVERTED   => '0',         -- OCLKB clock input is NOT inverted
        IS_OCLK_INVERTED    => '0'          -- OCLK clock input is NOT inverted
    )
    port map (
		D				=> DatD_p,		-- in
		DDLY			=> Low, 		-- in
		OFB				=> Low, 		-- in
		BITSLIP			=> DatBitSlip_p,-- in
		CE1				=> IntDatDoneEna,	-- in
		CE2				=> Low,			-- in
		RST				=> DatReSync,	    -- in
		CLK				=> DatClk, 	-- in
		CLKB			=> Low, 		-- in
		CLKDIV			=> DatClkDiv, 	-- in
        CLKDIVP         => Low,         -- in
		OCLK			=> Low, 		-- in
        OCLKB           => Low,         -- in
		DYNCLKDIVSEL	=> Low, 		-- in
		DYNCLKSEL		=> Low, 		-- in
		SHIFTOUT1		=> open, 		-- out
		SHIFTOUT2		=> open, 		-- out
		O				=> open, 		-- out
		Q1				=> IntDatSrdsOut(1), -- out	(1)
		Q2				=> IntDatSrdsOut(3), -- out	(3)
		Q3				=> IntDatSrdsOut(5), -- out	(5)
		Q4				=> IntDatSrdsOut(7), -- out	(7)
		Q5				=> IntDatSrdsOut(9), 		-- out
		Q6				=> IntDatSrdsOut(11), 		-- out
        Q7              => open,        -- out
        Q8              => open,        -- out
		SHIFTIN1		=> Low, 		-- in
		SHIFTIN2		=> Low 			-- in
	);
	
	

AdcData_I_Isrds_D0_n : ISERDESE2
	generic map (
		SERDES_MODE			=> "MASTER",			--
		INTERFACE_TYPE		=> "NETWORKING",		--
		IOBDELAY			=> "NONE",				--
		DATA_RATE 			=> "SDR", 				--
		DATA_WIDTH 			=> IntIsrdsDataWidth,	-- <-- Number of bits
		DYN_CLKDIV_INV_EN	=> "FALSE",      		--
		DYN_CLK_INV_EN		=> "FALSE", 			--
		NUM_CE				=> 1, 					--
		OFB_USED			=> "FALSE", 			--
        INIT_Q1             => '0',         -- bit;
        INIT_Q2             => '0',         -- bit;
        INIT_Q3             => '0',         -- bit;
        INIT_Q4             => '0',         -- bit;
        SRVAL_Q1            => '0',         -- bit;
        SRVAL_Q2            => '0',         -- bit;
        SRVAL_Q3            => '0',         -- bit;
        SRVAL_Q4            => '0',          -- bit;
        IS_CLKB_INVERTED    => '0',         -- CLKB clock input is NOT inverted
        IS_CLKDIVP_INVERTED => '0',         -- CLKDIVP input is NOT inverted
        IS_CLKDIV_INVERTED  => '0',         -- CLKDIV clock input is NOT inverted
        IS_CLK_INVERTED     => '1',         -- CLK clock input IS INVERTED
        IS_D_INVERTED       => '1',         -- D (data) input is NOT inverted
        IS_OCLKB_INVERTED   => '0',         -- OCLKB clock input is NOT inverted
        IS_OCLK_INVERTED    => '0'          -- OCLK clock input is NOT inverted
    )
    port map (
		D				=> DatD_n,		-- in
		DDLY			=> Low, 		-- in
		OFB				=> Low, 		-- in
		BITSLIP			=> DatBitSlip_n,-- in
		CE1				=> IntDatDoneEna,	-- in
		CE2				=> Low,			-- in
		RST				=> DatReSync,	    -- in
		CLK				=> DatClk,	-- in
		CLKB			=> Low, 		-- in
		CLKDIV			=> DatClkDiv, 	-- in
        CLKDIVP         => Low,         -- in
		OCLK			=> Low, 		-- in
        OCLKB           => Low,         -- in
		DYNCLKDIVSEL	=> Low, 		-- in
		DYNCLKSEL		=> Low, 		-- in
		SHIFTOUT1		=> open, 		-- out
		SHIFTOUT2		=> open,		-- out
		O				=> open, 		-- out
		Q1				=> IntDatSrdsOut(0), -- out	(0)
		Q2				=> IntDatSrdsOut(2), -- out	(2)
		Q3				=> IntDatSrdsOut(4), -- out	(4)
		Q4				=> IntDatSrdsOut(6), -- out	(6)
		Q5				=> IntDatSrdsOut(8), 		-- out
		Q6				=> IntDatSrdsOut(10), 		-- out
        Q7              => open,        -- out
        Q8              => open,        -- out
		SHIFTIN1		=> Low, 		-- in
		SHIFTIN2		=> Low 			-- in
	);

   PROCESS (DatClkDiv, DatReSync)
   BEGIN
      IF DatReSync = '1' THEN
         IntDatSrdsOutPrev(1) <= '0';
      ELSIF (DatClkDiv'EVENT AND DatClkDiv = '1') THEN
         IntDatSrdsOutPrev(1) <= IntDatSrdsOut(1);
      END IF;
   END PROCESS;

with DatInvrtd select DatOut <=
    "0000" & IntDatSrdsOutPrev(1) & IntDatSrdsOut(10) & IntDatSrdsOut(11) & IntDatSrdsOut(8) & IntDatSrdsOut(9) & IntDatSrdsOut(6) & IntDatSrdsOut(7) & IntDatSrdsOut(4) & IntDatSrdsOut(5) & IntDatSrdsOut(2) & IntDatSrdsOut(3) & IntDatSrdsOut(0)  when '1',
    "0000" & IntDatSrdsOut when others;


end  RTL;
