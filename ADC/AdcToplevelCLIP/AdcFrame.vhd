-----------------------------------------------------------------------------------------------
-- � Copyright 2012, Xilinx, Inc. All rights reserved.
-- This file contains confidential and proprietary information of Xilinx, Inc. and is
-- protected under U.S. and international copyright and other intellectual property laws.
-----------------------------------------------------------------------------------------------
--
-- Disclaimer:
--		This disclaimer is not a license and does not grant any rights to the materials
--		distributed herewith. Except as otherwise provided in a valid license issued to you
--		by Xilinx, and to the maximum extent permitted by applicable law: (1) THESE MATERIALS
--		ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL
--		WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED
--		TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR
--		PURPOSE; and (2) Xilinx shall not be liable (whether in contract or tort, including
--		negligence, or under any other theory of liability) for any loss or damage of any
--		kind or nature related to, arising under or in connection with these materials,
--		including for any direct, or any indirect, special, incidental, or consequential
--		loss or damage (including loss of data, profits, goodwill, or any type of loss or
--		damage suffered as a result of any action brought by a third party) even if such
--		damage or loss was reasonably foreseeable or Xilinx had been advised of the
--		possibility of the same.
--
-- CRITICAL APPLICATIONS
--		Xilinx products are not designed or intended to be fail-safe, or for use in any
--		application requiring fail-safe performance, such as life-support or safety devices
--		or systems, Class III medical devices, nuclear facilities, applications related to
--		the deployment of airbags, or any other applications that could lead to death,
--		personal injury, or severe property or environmental damage (individually and
--		collectively, "Critical Applications"). Customer assumes the sole risk and
--		liability of any use of Xilinx products in Critical Applications, subject only to
--		applicable laws and regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.
--
--		Contact:    e-mail  hotline@xilinx.com        phone   + 1 800 255 7778
--   ____  ____
--  /   /\/   /
-- /___/  \  /   Vendor:    Xilinx
-- \   \   \/    Version:   V0.02
--  \   \        Filename:  AdcFrame.vhd
--  /   /        Date Last Modified:	25 May 18
-- /___/   /\    Date Created: 			05 Oct 07
-- \   \  /  \
--  \___\/\___\
--
-- Device:		7-Series
-- Author:		Marc Defossez
-- Entity Name: AdcFrame
-- Purpose:		This file is part of an FPGA interface for a Texas Instruments ADC.
-- Tools:		Vivado_2017.3 or later or later
-- Limitations: none
--
-- Revision History:
--  Jan  2016
--      Adapted for 12-bit, 1-wire.
--      Double nibble detect was only suitable for 14 and 16 bits.
--      Made it work for 12-bits too.
-- May 2018
--      - Changed the bitslip counter limit.
--      - Made it really slip 8-bits, before it could only slip 7-bits due to slip counter
--      - limitations.
--      - Changed behavior of re-sync after bitslip counter went out of range.
--      - Resync is now a full reset of the design.
--      - This makes the design clean and robust.
---------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
-----------------------------------------------------------------------------------------------
library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.std_logic_UNSIGNED.all;
	use IEEE.std_logic_textio.all;
	use std.textio.all;
library UNISIM;
	use UNISIM.VCOMPONENTS.all;
library work;
    use work.all;
-----------------------------------------------------------------------------------------------
-- Entity pin description
-----------------------------------------------------------------------------------------------
entity AdcFrame is
	generic (
		C_AdcBits			: integer;
		C_AdcWireInt		: integer;
		C_FrmPattern		: string
	);
    port (
        FrmClk_n		: in std_logic;		-- input n from IBUFDS_DIFF_OUT
        FrmClk_p		: in std_logic;		-- input p from IBUFDS_DIFF_OUT
        FrmClkOut       : out std_logic;
        FrmClkEna		: in std_logic;
        FrmClk			: in std_logic;
        FrmClkDiv		: in std_logic;
        FrmClkDone		: in std_logic;		-- Input from clock syncronisation.
        FrmClkReSync	: in std_logic;
        FrmClkInvrtd    : in std_logic;
        FrmClkBitSlip_p	: out std_logic;
        FrmClkBitSlip_n	: out std_logic;
		FrmClkDat		: out std_logic_vector(15 downto 0);
        FrmClkSyncWarn	: out std_logic
    );
end AdcFrame;
-----------------------------------------------------------------------------------------------
-- Architecture section
-----------------------------------------------------------------------------------------------
architecture RTL of AdcFrame  is
-----------------------------------------------------------------------------------------------
-- Component Instantiation
-----------------------------------------------------------------------------------------------


--
constant IntIsrdsDataWidth : integer := C_AdcBits/2;
constant Low : std_logic := '0';
constant High : std_logic := '1';
-- Signals

   TYPE STATE_TYPE IS (hold_1, hold_2, check, slip, done);
   SIGNAL state_p   : STATE_TYPE;
   SIGNAL state_n   : STATE_TYPE;


signal IntFrmDat		            : std_logic_vector(11 downto 0);
signal IntFrmDat_p		            : std_logic_vector(5 downto 0);
signal IntFrmDat_n		            : std_logic_vector(5 downto 0);
signal IntPattern_p                 : std_logic_vector(5 downto 0);
signal IntPattern_n                 : std_logic_vector(5 downto 0);
signal IntFrmDatPrev_p	            : std_logic_vector(5 downto 0);

signal IntClkBitSlip_p		        : std_logic;
signal IntClkBitSlip_n		        : std_logic;
signal IntClkDoneEna                : std_logic;
-- Attributes
attribute KEEP_HIERARCHY : string;
    attribute KEEP_HIERARCHY of RTL : architecture is "YES";
-----------------------------------------------------------------------------------------------
begin
-----------------------------------------------------------------------------------------------
-- ISERDES FOR FRAME CAPTURE
-----------------------------------------------------------------------------------------------

--IntFrmClk_n <= not FrmClk;
-- Above is commented because the ISERDESE2 has a generic IS_CLK_INVERTED set to '1'.
-- The CLK clock is inverted inside the ISERDESE2.
--

IntClkDoneEna <= FrmClkDone and FrmClkEna;

AdcFrame_I_Isrds_p : ISERDESE2
	generic map (
		SERDES_MODE         => "MASTER",            -- string
        INTERFACE_TYPE      => "NETWORKING",        -- string
        IOBDELAY            => "NONE",              -- string
        DATA_RATE           => "SDR",               -- string
        DATA_WIDTH          => IntIsrdsDataWidth,   -- integer <-- Number of bits
        DYN_CLKDIV_INV_EN   => "FALSE",             -- string
        DYN_CLK_INV_EN      => "FALSE",             -- string
        NUM_CE              => 1,                   -- integer
        OFB_USED            => "FALSE",             -- string
        INIT_Q1             => '0',                 -- bit;
        INIT_Q2             => '0',                 -- bit;
        INIT_Q3             => '0',                 -- bit;
        INIT_Q4             => '0',                 -- bit;
        SRVAL_Q1            => '0',                 -- bit;
        SRVAL_Q2            => '0',                 -- bit;
        SRVAL_Q3            => '0',                 -- bit;
        SRVAL_Q4            => '0',                 -- bit
        IS_CLKB_INVERTED    => '0',                 -- CLKB clock input is NOT inverted
        IS_CLKDIVP_INVERTED => '0',                 -- CLKDIVP input is NOT inverted
        IS_CLKDIV_INVERTED  => '0',                 -- CLKDIV clock input is NOT inverted
        IS_CLK_INVERTED     => '0',                 -- CLK clock input is NOT inverted
        IS_D_INVERTED       => '0',                 -- D (data) input is NOT inverted
        IS_OCLKB_INVERTED   => '0',                 -- OCLKB clock input is NOT inverted
        IS_OCLK_INVERTED    => '0'                  -- OCLK clock input is NOT inverted
	)
	port map (
		D				    => FrmClk_p,		    -- in
		DDLY			    => Low,				    -- in
		OFB				    => Low, 			    -- in
		BITSLIP			    => IntClkBitSlip_p,    -- in
		CE1				    => IntClkDoneEna,      	    -- in
		CE2				    => Low,				    -- in
		RST				    => FrmClkReSync,        -- in
		CLK				    => FrmClk,	 	    -- in
		CLKB			    => Low, 			    -- in
		CLKDIV			    => FrmClkDiv,	 	    -- in
        CLKDIVP             => Low,                 -- in
        OCLK			    => Low, 			    -- in
        OCLKB               => Low,                 -- in
		DYNCLKDIVSEL	    => Low, 			    -- in
		DYNCLKSEL		    => Low, 			    -- in
        SHIFTOUT1		    => open,	 		    -- out
		SHIFTOUT2		    => open, 			    -- out
		O				    => FrmClkOut, 			    -- out
		Q1				    => IntFrmDat_p(0),    -- out	(0)
		Q2				    => IntFrmDat_p(1),    -- out	(2)
		Q3				    => IntFrmDat_p(2),    -- out	(4)
		Q4				    => IntFrmDat_p(3),    -- out	(6)
		Q5				    => IntFrmDat_p(4),	 		    -- out
		Q6				    => IntFrmDat_p(5), 			    -- out
        Q7                  => open,                -- out
        Q8                  => open,                -- out
		SHIFTIN1		    => Low, 			    -- in
		SHIFTIN2		    => Low				    -- in
	);

--
AdcFrame_I_Isrds_n : ISERDESE2
	generic map (
        SERDES_MODE         => "MASTER",            -- string
        INTERFACE_TYPE      => "NETWORKING",        -- string
        IOBDELAY            => "NONE",              -- string
        DATA_RATE           => "SDR",               -- string
        DATA_WIDTH          => IntIsrdsDataWidth,   -- integer 12-bit = 3 and 14/16 b its = 4
        DYN_CLKDIV_INV_EN   => "FALSE",             -- string
        DYN_CLK_INV_EN      => "FALSE",             -- string
        NUM_CE              => 1,                   -- integer
        OFB_USED            => "FALSE",             -- string
        INIT_Q1             => '0',                 -- bit;
        INIT_Q2             => '0',                 -- bit;
        INIT_Q3             => '0',                 -- bit;
        INIT_Q4             => '0',                 -- bit;
        SRVAL_Q1            => '0',                 -- bit;
        SRVAL_Q2            => '0',                 -- bit;
        SRVAL_Q3            => '0',                 -- bit;
        SRVAL_Q4            => '0',                 -- bit;
        IS_CLKB_INVERTED    => '0',                 -- CLKB clock input is NOT inverted
        IS_CLKDIVP_INVERTED => '0',                 -- CLKDIVP input is NOT inverted
        IS_CLKDIV_INVERTED  => '0',                 -- CLKDIV clock input is NOT inverted
        IS_CLK_INVERTED     => '1',                 -- CLK clock input IS INVERTED
        IS_D_INVERTED       => '1',                 -- D (data) input is NOT inverted
        IS_OCLKB_INVERTED   => '0',                 -- OCLKB clock input is NOT inverted
        IS_OCLK_INVERTED    => '0'                  -- OCLK clock input is NOT inverted
	)
	port map (
		D				    => FrmClk_n,		    -- in
		DDLY			    => Low,				    -- in
		OFB				    => Low, 			    -- in
		BITSLIP			    => IntClkBitSlip_n,    -- in
		CE1				    => IntClkDoneEna,      	    -- in
		CE2				    => Low,				    -- in
		RST				    => FrmClkReSync,	    -- in
		CLK				    => FrmClk,	 	    -- in
		CLKB			    => Low, 			    -- in
		CLKDIV			    => FrmClkDiv,	 	    -- in
        CLKDIVP             => Low,                 -- in
		OCLK			    => Low, 			    -- in
        OCLKB               => Low,                 -- in
		DYNCLKDIVSEL	    => Low, 			    -- in
		DYNCLKSEL		    => Low, 			    -- in
		SHIFTOUT1		    => open,	 		    -- out
		SHIFTOUT2		    => open, 			    -- out
		O				    => open, 			    -- out
		Q1				    => IntFrmDat_n(0),    -- out	(0)
		Q2				    => IntFrmDat_n(1),    -- out	(2)
		Q3				    => IntFrmDat_n(2),    -- out	(4)
		Q4				    => IntFrmDat_n(3),    -- out	(6)
		Q5				    => IntFrmDat_n(4),	 		    -- out
		Q6				    => IntFrmDat_n(5), 			    -- out
        Q7                  => open,                -- out
        Q8                  => open,                -- out
		SHIFTIN1		    => Low, 			    -- in
		SHIFTIN2		    => Low				    -- in
	);
	
-----------------------------------------------------------------------------------------------
-- INVERT THE NEEDED BITS.
-----------------------------------------------------------------------------------------------
-- 12-bit single wire. Only three meaningful bits.
with FrmClkInvrtd select IntPattern_p <=
    "110001" when '1',
    "111000" when others;

with FrmClkInvrtd select IntPattern_n <=
    "111000" when others;

   PROCESS (FrmClkDiv, FrmClkReSync)
   BEGIN
      IF FrmClkReSync = '1' THEN
         state_p <= hold_1;
      ELSIF (FrmClkDiv'EVENT AND FrmClkDiv = '1') THEN
         CASE state_p IS
            WHEN hold_1 =>
                  state_p <= hold_2;
            WHEN hold_2 =>
                  state_p <= check;
            WHEN check =>
               IF IntFrmDat_p = IntPattern_p THEN
                  state_p <= done;
               ELSE
                  state_p <= slip;
               END IF;
            WHEN slip =>
                state_p <= hold_1;
            WHEN done =>
               IF IntFrmDat_p /= IntPattern_p THEN
                  state_p <= hold_1;
               ELSE
                  state_p <= done;
               END IF;
         END CASE;
      END IF;
   END PROCESS;
   
   PROCESS (state_p)
   BEGIN
      CASE state_p IS
         WHEN hold_1 =>
            IntClkBitSlip_p <= '0'; 
         WHEN hold_2 =>
            IntClkBitSlip_p <= '0'; 
         WHEN check =>
            IntClkBitSlip_p <= '0';
         WHEN slip =>
            IntClkBitSlip_p <= '1';
         WHEN done =>
            IntClkBitSlip_p <= '0';
      END CASE;
   END PROCESS;


   PROCESS (FrmClkDiv, FrmClkReSync)
   BEGIN
      IF FrmClkReSync = '1' THEN
         state_n <= hold_1;
      ELSIF (FrmClkDiv'EVENT AND FrmClkDiv = '1') THEN
         CASE state_n IS
            WHEN hold_1 =>
                  state_n <= hold_2;
            WHEN hold_2 =>
                  state_n <= check;
            WHEN check =>
               IF IntFrmDat_n = IntPattern_n THEN
                  state_n <= done;
               ELSE
                  state_n <= slip;
               END IF;
            WHEN slip =>
                state_n <= hold_1;
            WHEN done =>
               IF IntFrmDat_n /= IntPattern_n THEN
                  state_n <= hold_1;
               ELSE
                  state_n <= done;
               END IF;
         END CASE;
      END IF;
   END PROCESS;
   
   PROCESS (state_n)
   BEGIN
      CASE state_n IS
         WHEN hold_1 =>
            IntClkBitSlip_n <= '0'; 
         WHEN hold_2 =>
            IntClkBitSlip_n <= '0'; 
         WHEN check =>
            IntClkBitSlip_n <= '0';
         WHEN slip =>
            IntClkBitSlip_n <= '1';
         WHEN done =>
            IntClkBitSlip_n <= '0';
      END CASE;
   END PROCESS;
        
        FrmClkBitSlip_p	<= IntClkBitSlip_p;
        FrmClkBitSlip_n	<= IntClkBitSlip_n;
        FrmClkSyncWarn	<= IntClkBitSlip_p;

   PROCESS (FrmClkDiv, FrmClkReSync)
   BEGIN
      IF FrmClkReSync = '1' THEN
         IntFrmDatPrev_p <= "000000";
      ELSIF (FrmClkDiv'EVENT AND FrmClkDiv = '1') THEN
         IntFrmDatPrev_p <= IntFrmDat_p;
      END IF;
   END PROCESS;

with FrmClkInvrtd select FrmClkDat <=
    "0000" & IntFrmDatPrev_p(0) & IntFrmDat_n(5) & IntFrmDat_p(5) & IntFrmDat_n(4) & IntFrmDat_p(4) & IntFrmDat_n(3) & IntFrmDat_p(3) & IntFrmDat_n(2) & IntFrmDat_p(2) & IntFrmDat_n(1) & IntFrmDat_p(1) & IntFrmDat_n(0) when '1',
    "0000" & IntFrmDat_p(5) & IntFrmDat_n(5) & IntFrmDat_p(4) & IntFrmDat_n(4) & IntFrmDat_p(3) & IntFrmDat_n(3) & IntFrmDat_p(2) & IntFrmDat_n(2) & IntFrmDat_p(1) & IntFrmDat_n(1) & IntFrmDat_p(0) & IntFrmDat_n(0) when others;

--
-----------------------------------------------------------------------------------------------
end  RTL;
