
library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.std_logic_UNSIGNED.all;
    use IEEE.numeric_std.all;
library UNISIM;
	use UNISIM.vcomponents.all;
library work;
    use work.all;
    use work.PkgNiUtilities.all; --LV
    use work.PkgRegPortFlat.all;
    use work.PkgRegPort.all;

library UNISIM;
  use UNISIM.vcomponents.all;
---------------------------------------------------------------------------------------------
-- Entity pin description
---------------------------------------------------------------------------------------------
entity AdcToplevelCLIP is
  generic(
    kNumDioLines            : integer := 96;
    kNumSerialPorts         : integer := 6
  );
  port(
  
    SysRefClkLv          : in std_logic;      -- 200 MHz for IODELAYCTRL from application
    
    AdcClkDivLv          : out std_logic;
--    AdcClkDivRdyLv       : out std_logic;
--    AdcClkDivValidLv     : in std_logic;
    RegClkOutLv          : out std_logic;
    AdcSampleClk_lv_out    : in std_logic;
	AdcSampleClkX2_lv_out  : in std_logic;
    AdcSampleClk_lv_enable : in std_logic;
    AdcDataA             : out std_logic_vector(15 downto 0);
    AdcDataB             : out std_logic_vector(15 downto 0);
    AdcDataC             : out std_logic_vector(15 downto 0);
    AdcDataD             : out std_logic_vector(15 downto 0);
    AdcDataE             : out std_logic_vector(15 downto 0);
    AdcDataF             : out std_logic_vector(15 downto 0);
    AdcDataG             : out std_logic_vector(15 downto 0);
    AdcDataH             : out std_logic_vector(15 downto 0);
    AdcFrmDataOutLv      : out std_logic_vector(15 downto 0);
    
    FpgaClkIn_lv_in  	 : out std_logic_vector(0 downto 0);
    FpgaClkOut_lv_out	 : in std_logic_vector(0 downto 0);
    FpgaClkOut_lv_enable : in std_logic_vector(0 downto 0);

    LedStatus_lv_out  	: in std_logic_vector(0 downto 0);
    LedStatus_lv_enable	: in std_logic_vector(0 downto 0);
	LedUser_lv_out		: in std_logic_vector(0 downto 0);
    LedUser_lv_enable  	: in std_logic_vector(0 downto 0);
	
	DigA_lv_in          : out std_logic_vector(0 downto 0);
    DigB_lv_in          : out std_logic_vector(0 downto 0);
    DigC_lv_in        	: out std_logic_vector(0 downto 0);
	DigD_lv_in          : out std_logic_vector(0 downto 0);
    DigE_lv_in			: out std_logic_vector(0 downto 0);
    DigF_lv_in        	: out std_logic_vector(0 downto 0);
	DigG_lv_in			: out std_logic_vector(0 downto 0);
    DigH_lv_in        	: out std_logic_vector(0 downto 0);
	
	RmcSclk_lv_out		: in std_logic_vector(0 downto 0);
	RmcSclk_lv_enable	: in std_logic_vector(0 downto 0);
	RmcSdio_lv_in      	: out std_logic_vector(0 downto 0);
	RmcSdio_lv_out		: in std_logic_vector(0 downto 0);
    RmcSdio_lv_enable   : in std_logic_vector(0 downto 0);
	RmcCsb_lv_out		: in std_logic_vector(0 downto 0);
	RmcCsb_lv_enable	: in std_logic_vector(0 downto 0);
	
    
    AdcFrmSyncWrnLv      : out std_logic;
    AdcBitClkAlgnWrnLv   : out std_logic;
    AdcBitClkInvrtdLv    : out std_logic;
    AdcBitClkDoneLv      : out std_logic;
    AdcIdlyCtrlRdyLv     : out std_logic;
    
    AdcIntrfcRstLv            : in std_logic;
    AdcIntrfcEnaLv            : in std_logic;
    AdcReSyncLv               : in std_logic;
    RegClkLvInvLv             : in std_logic;


    aReset      : in std_logic;
    aPowerOnReset : in std_logic;
    aRmcDioCLIP : inout std_logic_vector(kNumDioLines - 1 downto 0);

    --common clocks
    SerialClk : in std_logic;

    --serial enable
    aSerialEnable : out std_logic_vector(kNumSerialPorts - 1 downto 0) := (others => '0');

    --connect this to soft serial components
    sSerialRegPortIn  : in std_logic_vector(100 downto 0);
    sSerialRegPortOut : out std_logic_vector(64 downto 0);

    aClipIrq : out std_logic_vector(kNumSerialPorts - 1 downto 0) := (others => '0');
    aClipBank0_2v5 : out std_logic;
    aClipBank1_2v5 : out std_logic;
	GpioOut : out std_logic_vector(15 downto 0) := (others => '0');
	GpioIn : in std_logic_vector(15 downto 0)
  );	
end AdcToplevelCLIP;

---------------------------------------------------------------------------------------------
-- Architecture section
---------------------------------------------------------------------------------------------
architecture RTL of AdcToplevelCLIP is

  signal sSerialRegPortOutArray : RegPortOutAry_t(kNumSerialPorts - 1  downto 0); --LV
  signal aClipIrqVector : std_logic_vector(kNumSerialPorts - 1 downto 0) := (others => '0');

  ---------------------------------------------------------------
  -- I/O Signals
  ---------------------------------------------------------------
  signal aRmcDioCLIP_in : std_logic_vector(kNumDioLines - 1 downto 0);
  signal aRmcDioCLIP_out : std_logic_vector(kNumDioLines - 1 downto 0);
  signal aRmcDioCLIP_tri : std_logic_vector(kNumDioLines - 1 downto 0);
  signal aRmcDioCLIP_enable : std_logic_vector(kNumDioLines - 1 downto 0);
  
  
---------------------------------------------------------------------------------------------
-- Component Instantiation
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
-- Constants, Signals and Attributes Declarations
constant C_OnChipLvdsTerm        : integer := 1;     -- 0 = No Term, 1 - Termination ON. -- OK(1)
constant C_AdcChnls              : integer := 8;     -- Number of ADC in a package       -- OK(8)
constant C_AdcWireInt            : integer := 1;     -- 2 = 2-wire, 1 = 1-wire interface -- OK(1)
constant C_AdcBits               : integer := 12;                                        -- OK(12)
constant C_StatTaps              : integer := 16;                                        -- OK(16)
constant C_AdcUseIdlyCtrl        : integer := 1;      -- 0 = No, 1 = Yes                 -- OK(1)
constant C_FrmPattern            : string  := "111111000000";  -- "1111111100000000"      -- OK(111111000000)
                                          -- 2-Wire, 16-bit      -- 1-wire, 16-bit
                                          --
                                          -- "111111000000"      -- "000000111000"
                                          -- 1-wire, 12-bit      -- 2-wire, 12-bit
---------------------------------------------------------------------------------------------
-- Functions
function ConvTrueFalse (Term : integer) return boolean is
begin
    if (Term = 0) then
        return FALSE;
    else
        return TRUE;
    end if;
end ConvTrueFalse;
-- Constants
constant Low  : std_logic	:= '0';
constant High : std_logic	:= '1';
-- Signals
signal IntDCLK_p : std_logic;
signal IntDCLK_n : std_logic;
signal IntFCLK_p : std_logic;
signal IntFCLK_n : std_logic;
signal IntDATA_p : std_logic_vector((C_AdcChnls*C_AdcWireInt)-1 downto 0);
signal IntDATA_n : std_logic_vector((C_AdcChnls*C_AdcWireInt)-1 downto 0);

signal IntClkDivLv         : std_logic;
signal MuxIntClkDivLv      : std_logic;
signal glReset             : std_logic;
signal AdcBitClkDone       : std_logic;
signal AdcDataOutLv        : std_logic_vector((32*((C_AdcChnls/2)*C_AdcWireInt))-1 downto 0); 
-- Attributes
signal RegClkOutLvPre : std_logic;

---------------------------------------------------------------------------------------------
begin
  
    AdcDataA     <= AdcDataOutLv(15 downto 0);
    AdcDataB     <= AdcDataOutLv(31 downto 16);
    AdcDataC     <= AdcDataOutLv(47 downto 32);
    AdcDataD     <= AdcDataOutLv(63 downto 48);
    AdcDataE     <= AdcDataOutLv(79 downto 64);
    AdcDataF     <= AdcDataOutLv(95 downto 80);
    AdcDataG     <= AdcDataOutLv(111 downto 96);
    AdcDataH     <= AdcDataOutLv(127 downto 112);
    
    glReset     <= aReset;

--  AdcClkDivRdyLv <= AdcSampleClk_lv_enable and AdcBitClkDone;
 AdcBitClkDoneLv <= AdcBitClkDone;
  
  aClipIrq <= aClipIrqVector;
  aClipBank0_2V5 <= '1';
  aClipBank1_2V5 <= '0';
  aRmcDioCLIP_tri <= not aRmcDioCLIP_enable;
  sSerialRegPortOut <= RegPortToSlv(OrAry(sSerialRegPortOutArray)); --LV
 

--

--  BUFGMUXx: BUFGMUX
--    port map (
--      I0      => AdcSampleClk_lv_out,     -- in  std_logic
--      I1      => IntClkDivLv,       -- in  std_logic
--      S       => RegClkLvInvLv,    -- in  std_logic
--      O       => MuxIntClkDivLv);   -- out std_logic
MuxIntClkDivLv <= AdcSampleClk_lv_out;
AdcClkDivLv <= MuxIntClkDivLv;

AdcToplevel_Toplevel_I_Ibufgds_BitClk : IBUFGDS_DIFF_OUT
    generic map (DIFF_TERM  => ConvTrueFalse(C_OnChipLvdsTerm), IOSTANDARD  => "LVDS_25")
    port map (I => aRmcDioCLIP(41), IB => aRmcDioCLIP(40), O => IntDCLK_n, OB => IntDCLK_p);
--
AdcToplevel_Toplevel_I_Ibufds_FrmClk : IBUFDS_DIFF_OUT
    generic map (IOSTANDARD => "LVDS_25", DIFF_TERM => ConvTrueFalse(C_OnChipLvdsTerm))
    port map (I => aRmcDioCLIP(8), IB => aRmcDioCLIP(7), O => IntFCLK_n, OB => IntFCLK_p);
--

AdcToplevel_Toplevel_I_Ibufds_DI_A : IBUFDS_DIFF_OUT
    generic map (IOSTANDARD => "LVDS_25", DIFF_TERM => ConvTrueFalse(C_OnChipLvdsTerm))
    port map (I => aRmcDioCLIP(4), IB => aRmcDioCLIP(3), O => IntDATA_p(0), OB => IntDATA_n(0));
AdcToplevel_Toplevel_I_Ibufds_DI_B : IBUFDS_DIFF_OUT
    generic map (IOSTANDARD => "LVDS_25", DIFF_TERM => ConvTrueFalse(C_OnChipLvdsTerm))
    port map (I => aRmcDioCLIP(37), IB => aRmcDioCLIP(36), O => IntDATA_p(1), OB => IntDATA_n(1));
AdcToplevel_Toplevel_I_Ibufds_DI_C : IBUFDS_DIFF_OUT
    generic map (IOSTANDARD => "LVDS_25", DIFF_TERM => ConvTrueFalse(C_OnChipLvdsTerm))
    port map (I => aRmcDioCLIP(6), IB => aRmcDioCLIP(5), O => IntDATA_p(2), OB => IntDATA_n(2));
AdcToplevel_Toplevel_I_Ibufds_DI_D : IBUFDS_DIFF_OUT
    generic map (IOSTANDARD => "LVDS_25", DIFF_TERM => ConvTrueFalse(C_OnChipLvdsTerm))
    port map (I => aRmcDioCLIP(39), IB => aRmcDioCLIP(38), O => IntDATA_p(3), OB => IntDATA_n(3)); 
AdcToplevel_Toplevel_I_Ibufds_DI_E : IBUFDS_DIFF_OUT
    generic map (IOSTANDARD => "LVDS_25", DIFF_TERM => ConvTrueFalse(C_OnChipLvdsTerm))
    port map (I => aRmcDioCLIP(10), IB => aRmcDioCLIP(9), O => IntDATA_p(4), OB => IntDATA_n(4));
AdcToplevel_Toplevel_I_Ibufds_DI_F : IBUFDS_DIFF_OUT
    generic map (IOSTANDARD => "LVDS_25", DIFF_TERM => ConvTrueFalse(C_OnChipLvdsTerm))
    port map (I => aRmcDioCLIP(43), IB => aRmcDioCLIP(42), O => IntDATA_p(5), OB => IntDATA_n(5));
AdcToplevel_Toplevel_I_Ibufds_DI_G : IBUFDS_DIFF_OUT
    generic map (IOSTANDARD => "LVDS_25", DIFF_TERM => ConvTrueFalse(C_OnChipLvdsTerm))
    port map (I => aRmcDioCLIP(12), IB => aRmcDioCLIP(11), O => IntDATA_p(6), OB => IntDATA_n(6));
AdcToplevel_Toplevel_I_Ibufds_DI_H : IBUFDS_DIFF_OUT
    generic map (IOSTANDARD => "LVDS_25", DIFF_TERM => ConvTrueFalse(C_OnChipLvdsTerm))
    port map (I => aRmcDioCLIP(14), IB => aRmcDioCLIP(13), O => IntDATA_p(7), OB => IntDATA_n(7));    
    
---------------------------------------------------------------------------------------------


AdcToplevel_Toplevel_I_AdcToplevel : entity work.AdcToplevel
    generic map(
        C_AdcChnls          => C_AdcChnls,
        C_AdcWireInt        => C_AdcWireInt,
        C_AdcBits           => C_AdcBits,
        C_StatTaps          => C_StatTaps,
        C_AdcUseIdlyCtrl    => C_AdcUseIdlyCtrl,
		C_FrmPattern	    => C_FrmPattern
    )
    port map (
        DCLK_p             => IntDCLK_p, -- in
        DCLK_n             => IntDCLK_n, -- in
        FCLK_p             => IntFCLK_p, -- in
        FCLK_n             => IntFCLK_n, -- in
        DATA_p             => IntDATA_p, -- in
        DATA_n             => IntDATA_n, -- in
        -- application connections
        SysRefClk          => SysRefClkLv, -- in
--        CLKx2_lv_out       => AdcSampleClkX2_lv_out,
        AdcIntrfcRstLv     => AdcIntrfcRstLv, -- in
        AdcFrmSyncWrn      => AdcFrmSyncWrnLv, -- out
        AdcBitClkAlgnWrn   => AdcBitClkAlgnWrnLv, -- out
        AdcBitClkInvrtd    => AdcBitClkInvrtdLv, -- out
        AdcBitClkDone      => AdcBitClkDone, -- out
        AdcIdlyCtrlRdy     => AdcIdlyCtrlRdyLv, -- out

        glReset             => glReset,
        IntClkDivOutLv      => IntClkDivLv,
        IntClkDivInLv       => MuxIntClkDivLv,
        RegClkOutLv         => RegClkOutLv,
        AdcDataOutLv        => AdcDataOutLv,
        AdcIntrfcEnaLv      => AdcIntrfcEnaLv,
        AdcReSyncLv         => AdcReSyncLv,
        RegClkLvInvLv       => RegClkLvInvLv,
        AdcFrmDataOutLv     => AdcFrmDataOutLv
    );
---------------------------------------------------------------------------------------------
-- 


  ---------------------------------------------------------------
  --Peripheral : Serial2
  ---------------------------------------------------------------
  sSerialRegPortOutArray(0) <= kRegPortOutZero;
  aClipIrqVector(0) <= '0';
  aSerialEnable(0) <= '0';



  ---------------------------------------------------------------
  --Peripheral : Serial3
  ---------------------------------------------------------------
  sSerialRegPortOutArray(1) <= kRegPortOutZero;
  aClipIrqVector(1) <= '0';
  aSerialEnable(1) <= '0';



  ---------------------------------------------------------------
  --Peripheral : Serial4
  ---------------------------------------------------------------
  sSerialRegPortOutArray(2) <= kRegPortOutZero;
  aClipIrqVector(2) <= '0';
  aSerialEnable(2) <= '0';



  ---------------------------------------------------------------
  --Peripheral : Serial5
  ---------------------------------------------------------------
  sSerialRegPortOutArray(3) <= kRegPortOutZero;
  aClipIrqVector(3) <= '0';
  aSerialEnable(3) <= '0';



  ---------------------------------------------------------------
  --Peripheral : Serial6
  ---------------------------------------------------------------
  sSerialRegPortOutArray(4) <= kRegPortOutZero;
  aClipIrqVector(4) <= '0';
  aSerialEnable(4) <= '0';



  ---------------------------------------------------------------
  --Peripheral : Serial7
  ---------------------------------------------------------------
  sSerialRegPortOutArray(5) <= kRegPortOutZero;
  aClipIrqVector(5) <= '0';
  aSerialEnable(5) <= '0';

  ---------------------------------------------------------------
  --I/O Buffer: DIO_46 (DIO_47_N), DIO_47 (DIO_47_P)
  ---------------------------------------------------------------
  OBUFTDS_47 : OBUFTDS
  generic map (IOSTANDARD => "LVDS_25")
  port map (
    O  => aRmcDioCLIP(47),
    OB => aRmcDioCLIP(46),
    I  => aRmcDioCLIP_out(47),
    T  => aRmcDioCLIP_tri(47));
	
  aRmcDioCLIP_in(47) <= aRmcDioCLIP_out(47);
  aRmcDioCLIP_out(47) <= AdcSampleClk_lv_out;
  aRmcDioCLIP_enable(47) <= AdcSampleClk_lv_enable;

  ---------------------------------------------------------------
  --I/O Buffer: DIO_85 (DIO_86_N)
  ---------------------------------------------------------------
  IOBUF_85 : IBUF
  generic map (IOSTANDARD => "LVCMOS25")
  port map (
    O  => aRmcDioCLIP_in(85),
    I  => aRmcDioCLIP(85));
	
	FpgaClkIn_lv_in(0) <= aRmcDioCLIP_in(85);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_86 (DIO_86_P)
  ---------------------------------------------------------------
  IOBUF_86 : OBUFT
  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 12, SLEW => "FAST")
  port map (
    O  => aRmcDioCLIP(86),
    I  => aRmcDioCLIP_out(86),
    T  => aRmcDioCLIP_tri(86));

	aRmcDioCLIP_in(86) 		<= aRmcDioCLIP_out(86);
	aRmcDioCLIP_out(86) 	<= FpgaClkOut_lv_out(0);
	aRmcDioCLIP_enable(86) 	<= FpgaClkOut_lv_enable(0);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_16 (DIO_17_N)
  ---------------------------------------------------------------
  IOBUF_16 : OBUFT
  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
  port map (
    O  => aRmcDioCLIP(16),
    I  => aRmcDioCLIP_out(16),
    T  => aRmcDioCLIP_tri(16));
	
	aRmcDioCLIP_in(16) 		<= aRmcDioCLIP_out(16);
	aRmcDioCLIP_out(16) 	<= LedUser_lv_out(0);
	aRmcDioCLIP_enable(16) 	<= LedUser_lv_enable(0);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_48
  ---------------------------------------------------------------
  IOBUF_48 : OBUFT
  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
  port map (
    O  => aRmcDioCLIP(48),
    I  => aRmcDioCLIP_out(48),
    T  => aRmcDioCLIP_tri(48));
	
	aRmcDioCLIP_in(48) 		<= aRmcDioCLIP_out(48);
	aRmcDioCLIP_out(48) 	<= LedStatus_lv_out(0);
	aRmcDioCLIP_enable(48) 	<= LedStatus_lv_enable(0);

  ---------------------------------------------------------------
  --I/O Buffer: DIO_92 (DIO_92_P)
  ---------------------------------------------------------------
  IOBUF_92 : IBUF
  generic map (IOSTANDARD => "LVCMOS25")
  port map (
    O  => aRmcDioCLIP_in(92),
    I  => aRmcDioCLIP(92));
	
	DigA_lv_in(0) <= aRmcDioCLIP_in(92);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_91 (DIO_92_N)
  ---------------------------------------------------------------
  IOBUF_91 : IBUF
  generic map (IOSTANDARD => "LVCMOS25")
  port map (
    O  => aRmcDioCLIP_in(91),
    I  => aRmcDioCLIP(91));
	
	DigB_lv_in(0) <= aRmcDioCLIP_in(91);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_58 (DIO_58_P)
  ---------------------------------------------------------------
  IOBUF_58 : IBUF
  generic map (IOSTANDARD => "LVCMOS25")
  port map (
    O  => aRmcDioCLIP_in(58),
    I  => aRmcDioCLIP(58));
	
	DigC_lv_in(0) <= aRmcDioCLIP_in(58);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_57 (DIO_58_N)
  ---------------------------------------------------------------
  IOBUF_57 : IBUF
  generic map (IOSTANDARD => "LVCMOS25")
  port map (
    O  => aRmcDioCLIP_in(57),
    I  => aRmcDioCLIP(57));
	
	DigD_lv_in(0) <= aRmcDioCLIP_in(57);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_90 (DIO_90_P)
  ---------------------------------------------------------------
  IOBUF_90 : IBUF
  generic map (IOSTANDARD => "LVCMOS25")
  port map (
    O  => aRmcDioCLIP_in(90),
    I  => aRmcDioCLIP(90));
	
	DigE_lv_in(0) <= aRmcDioCLIP_in(90);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_89 (DIO_90_N)
  ---------------------------------------------------------------
  IOBUF_89 : IBUF
  generic map (IOSTANDARD => "LVCMOS25")
  port map (
    O  => aRmcDioCLIP_in(89),
    I  => aRmcDioCLIP(89));
	
	DigF_lv_in(0) <= aRmcDioCLIP_in(89);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_56 (DIO_56_P)
  ---------------------------------------------------------------
  IOBUF_56 : IBUF
  generic map (IOSTANDARD => "LVCMOS25")
  port map (
    O  => aRmcDioCLIP_in(56),
    I  => aRmcDioCLIP(56));
	
	DigG_lv_in(0) <= aRmcDioCLIP_in(56);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_55 (DIO_56_N)
  ---------------------------------------------------------------
  IOBUF_55 : IBUF
  generic map (IOSTANDARD => "LVCMOS25")
  port map (
    O  => aRmcDioCLIP_in(55),
    I  => aRmcDioCLIP(55));
	
	DigH_lv_in(0) <= aRmcDioCLIP_in(55);

  ---------------------------------------------------------------
  --I/O Buffer: DIO_54 (DIO_54_P)
  ---------------------------------------------------------------
  IOBUF_54 : OBUFT
  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "FAST")
  port map (
    O  => aRmcDioCLIP(54),
    I  => aRmcDioCLIP_out(54),
    T  => aRmcDioCLIP_tri(54));
	
	aRmcDioCLIP_in(54) 		<= aRmcDioCLIP_out(54);
	aRmcDioCLIP_out(54) 	<= RmcSclk_lv_out(0);
	aRmcDioCLIP_enable(54) 	<= RmcSclk_lv_enable(0);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_87 (DIO_88_N)
  ---------------------------------------------------------------
  IOBUF_87 : IOBUF
  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
  port map (
    O  => aRmcDioCLIP_in(87),
    IO => aRmcDioCLIP(87),
    I  => aRmcDioCLIP_out(87),
    T  => aRmcDioCLIP_tri(87));
	
	RmcSdio_lv_in(0)		<= aRmcDioCLIP_in(87);
	aRmcDioCLIP_out(87) 	<= RmcSdio_lv_out(0);
	aRmcDioCLIP_enable(87) 	<= RmcSdio_lv_enable(0);
  ---------------------------------------------------------------
  --I/O Buffer: DIO_88 (DIO_88_P)
  ---------------------------------------------------------------
  IOBUF_88 : OBUFT
  generic map (IOSTANDARD => "LVCMOS25", DRIVE => 8, SLEW => "SLOW")
  port map (
    O  => aRmcDioCLIP(88),
    I  => aRmcDioCLIP_out(88),
    T  => aRmcDioCLIP_tri(88));
	
	aRmcDioCLIP_in(88) 		<= aRmcDioCLIP_out(88);
	aRmcDioCLIP_out(88) 	<= RmcCsb_lv_out(0);
	aRmcDioCLIP_enable(88) 	<= RmcCsb_lv_enable(0);

  ---------------------------------------------------------------
  --I/O Buffer: DIO_00
  ---------------------------------------------------------------
  IOBUF_0 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(0),
    IO => aRmcDioCLIP(0),
    I  => aRmcDioCLIP_out(0),
    T  => aRmcDioCLIP_tri(0));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_01 (DIO_02_N)
  ---------------------------------------------------------------
  IOBUF_1 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(1),
    IO => aRmcDioCLIP(1),
    I  => aRmcDioCLIP_out(1),
    T  => aRmcDioCLIP_tri(1));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_02 (DIO_02_P)
  ---------------------------------------------------------------
  IOBUF_2 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(2),
    IO => aRmcDioCLIP(2),
    I  => aRmcDioCLIP_out(2),
    T  => aRmcDioCLIP_tri(2));

  ---------------------------------------------------------------
  --I/O Buffer: DIO_15
  ---------------------------------------------------------------
  IOBUF_15 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(15),
    IO => aRmcDioCLIP(15),
    I  => aRmcDioCLIP_out(15),
    T  => aRmcDioCLIP_tri(15));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_17 (DIO_17_P)
  ---------------------------------------------------------------
  IOBUF_17 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(17),
    IO => aRmcDioCLIP(17),
    I  => aRmcDioCLIP_out(17),
    T  => aRmcDioCLIP_tri(17));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_18 (DIO_19_N)
  ---------------------------------------------------------------
  IOBUF_18 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(18),
    IO => aRmcDioCLIP(18),
    I  => aRmcDioCLIP_out(18),
    T  => aRmcDioCLIP_tri(18));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_19 (DIO_19_P)
  ---------------------------------------------------------------
  IOBUF_19 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(19),
    IO => aRmcDioCLIP(19),
    I  => aRmcDioCLIP_out(19),
    T  => aRmcDioCLIP_tri(19));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_20 (DIO_21_N)
  ---------------------------------------------------------------
  IOBUF_20 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(20),
    IO => aRmcDioCLIP(20),
    I  => aRmcDioCLIP_out(20),
    T  => aRmcDioCLIP_tri(20));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_21 (DIO_21_P)
  ---------------------------------------------------------------
  IOBUF_21 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(21),
    IO => aRmcDioCLIP(21),
    I  => aRmcDioCLIP_out(21),
    T  => aRmcDioCLIP_tri(21));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_22 (DIO_23_N)
  ---------------------------------------------------------------
  IOBUF_22 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(22),
    IO => aRmcDioCLIP(22),
    I  => aRmcDioCLIP_out(22),
    T  => aRmcDioCLIP_tri(22));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_23 (DIO_23_P)
  ---------------------------------------------------------------
  IOBUF_23 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(23),
    IO => aRmcDioCLIP(23),
    I  => aRmcDioCLIP_out(23),
    T  => aRmcDioCLIP_tri(23));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_24 (DIO_25_N)
  ---------------------------------------------------------------
  IOBUF_24 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(24),
    IO => aRmcDioCLIP(24),
    I  => aRmcDioCLIP_out(24),
    T  => aRmcDioCLIP_tri(24));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_25 (DIO_25_P)
  ---------------------------------------------------------------
  IOBUF_25 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(25),
    IO => aRmcDioCLIP(25),
    I  => aRmcDioCLIP_out(25),
    T  => aRmcDioCLIP_tri(25));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_26 (DIO_27_N)
  ---------------------------------------------------------------
  IOBUF_26 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(26),
    IO => aRmcDioCLIP(26),
    I  => aRmcDioCLIP_out(26),
    T  => aRmcDioCLIP_tri(26));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_27 (DIO_27_P)
  ---------------------------------------------------------------
  IOBUF_27 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(27),
    IO => aRmcDioCLIP(27),
    I  => aRmcDioCLIP_out(27),
    T  => aRmcDioCLIP_tri(27));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_28 (DIO_29_N)
  ---------------------------------------------------------------
  IOBUF_28 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(28),
    IO => aRmcDioCLIP(28),
    I  => aRmcDioCLIP_out(28),
    T  => aRmcDioCLIP_tri(28));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_29 (DIO_29_P)
  ---------------------------------------------------------------
  IOBUF_29 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(29),
    IO => aRmcDioCLIP(29),
    I  => aRmcDioCLIP_out(29),
    T  => aRmcDioCLIP_tri(29));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_30 (DIO_31_N)
  ---------------------------------------------------------------
  IOBUF_30 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(30),
    IO => aRmcDioCLIP(30),
    I  => aRmcDioCLIP_out(30),
    T  => aRmcDioCLIP_tri(30));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_31 (DIO_31_P)
  ---------------------------------------------------------------
  IOBUF_31 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(31),
    IO => aRmcDioCLIP(31),
    I  => aRmcDioCLIP_out(31),
    T  => aRmcDioCLIP_tri(31));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_32 (DIO_33_N)
  ---------------------------------------------------------------
  IOBUF_32 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(32),
    IO => aRmcDioCLIP(32),
    I  => aRmcDioCLIP_out(32),
    T  => aRmcDioCLIP_tri(32));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_33 (DIO_33_P)
  ---------------------------------------------------------------
  IOBUF_33 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(33),
    IO => aRmcDioCLIP(33),
    I  => aRmcDioCLIP_out(33),
    T  => aRmcDioCLIP_tri(33));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_34 (DIO_35_N)
  ---------------------------------------------------------------
  IOBUF_34 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(34),
    IO => aRmcDioCLIP(34),
    I  => aRmcDioCLIP_out(34),
    T  => aRmcDioCLIP_tri(34));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_35 (DIO_35_P)
  ---------------------------------------------------------------
  IOBUF_35 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(35),
    IO => aRmcDioCLIP(35),
    I  => aRmcDioCLIP_out(35),
    T  => aRmcDioCLIP_tri(35));

  ---------------------------------------------------------------
  --I/O Buffer: DIO_44 (DIO_45_N)
  ---------------------------------------------------------------
  IOBUF_44 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(44),
    IO => aRmcDioCLIP(44),
    I  => aRmcDioCLIP_out(44),
    T  => aRmcDioCLIP_tri(44));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_45 (DIO_45_P)
  ---------------------------------------------------------------
  IOBUF_45 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(45),
    IO => aRmcDioCLIP(45),
    I  => aRmcDioCLIP_out(45),
    T  => aRmcDioCLIP_tri(45));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_49 (DIO_50_N)
  ---------------------------------------------------------------
  IOBUF_49 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(49),
    IO => aRmcDioCLIP(49),
    I  => aRmcDioCLIP_out(49),
    T  => aRmcDioCLIP_tri(49));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_50 (DIO_50_P)
  ---------------------------------------------------------------
  IOBUF_50 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(50),
    IO => aRmcDioCLIP(50),
    I  => aRmcDioCLIP_out(50),
    T  => aRmcDioCLIP_tri(50));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_51 (DIO_52_N)
  ---------------------------------------------------------------
  IOBUF_51 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(51),
    IO => aRmcDioCLIP(51),
    I  => aRmcDioCLIP_out(51),
    T  => aRmcDioCLIP_tri(51));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_52 (DIO_52_P)
  ---------------------------------------------------------------
  IOBUF_52 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(52),
    IO => aRmcDioCLIP(52),
    I  => aRmcDioCLIP_out(52),
    T  => aRmcDioCLIP_tri(52));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_53 (DIO_54_N)
  ---------------------------------------------------------------
  IOBUF_53 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(53),
    IO => aRmcDioCLIP(53),
    I  => aRmcDioCLIP_out(53),
    T  => aRmcDioCLIP_tri(53));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_59 (DIO_60_N)
  ---------------------------------------------------------------
  IOBUF_59 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(59),
    IO => aRmcDioCLIP(59),
    I  => aRmcDioCLIP_out(59),
    T  => aRmcDioCLIP_tri(59));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_60 (DIO_60_P)
  ---------------------------------------------------------------
  IOBUF_60 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(60),
    IO => aRmcDioCLIP(60),
    I  => aRmcDioCLIP_out(60),
    T  => aRmcDioCLIP_tri(60));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_61 (DIO_62_N)
  ---------------------------------------------------------------
  IOBUF_61 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(61),
    IO => aRmcDioCLIP(61),
    I  => aRmcDioCLIP_out(61),
    T  => aRmcDioCLIP_tri(61));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_62 (DIO_62_P)
  ---------------------------------------------------------------
  IOBUF_62 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(62),
    IO => aRmcDioCLIP(62),
    I  => aRmcDioCLIP_out(62),
    T  => aRmcDioCLIP_tri(62));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_63
  ---------------------------------------------------------------
  IOBUF_63 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(63),
    IO => aRmcDioCLIP(63),
    I  => aRmcDioCLIP_out(63),
    T  => aRmcDioCLIP_tri(63));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_64 (DIO_65_N)
  ---------------------------------------------------------------
  IOBUF_64 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(64),
    IO => aRmcDioCLIP(64),
    I  => aRmcDioCLIP_out(64),
    T  => aRmcDioCLIP_tri(64));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_65 (DIO_65_P)
  ---------------------------------------------------------------
  IOBUF_65 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(65),
    IO => aRmcDioCLIP(65),
    I  => aRmcDioCLIP_out(65),
    T  => aRmcDioCLIP_tri(65));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_66 (DIO_67_N)
  ---------------------------------------------------------------
  IOBUF_66 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(66),
    IO => aRmcDioCLIP(66),
    I  => aRmcDioCLIP_out(66),
    T  => aRmcDioCLIP_tri(66));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_67 (DIO_67_P)
  ---------------------------------------------------------------
  IOBUF_67 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(67),
    IO => aRmcDioCLIP(67),
    I  => aRmcDioCLIP_out(67),
    T  => aRmcDioCLIP_tri(67));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_68 (DIO_69_N)
  ---------------------------------------------------------------
  IOBUF_68 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(68),
    IO => aRmcDioCLIP(68),
    I  => aRmcDioCLIP_out(68),
    T  => aRmcDioCLIP_tri(68));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_69 (DIO_69_P)
  ---------------------------------------------------------------
  IOBUF_69 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(69),
    IO => aRmcDioCLIP(69),
    I  => aRmcDioCLIP_out(69),
    T  => aRmcDioCLIP_tri(69));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_70 (DIO_71_N)
  ---------------------------------------------------------------
  IOBUF_70 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(70),
    IO => aRmcDioCLIP(70),
    I  => aRmcDioCLIP_out(70),
    T  => aRmcDioCLIP_tri(70));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_71 (DIO_71_P)
  ---------------------------------------------------------------
  IOBUF_71 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(71),
    IO => aRmcDioCLIP(71),
    I  => aRmcDioCLIP_out(71),
    T  => aRmcDioCLIP_tri(71));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_72 (DIO_73_N)
  ---------------------------------------------------------------
  IOBUF_72 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(72),
    IO => aRmcDioCLIP(72),
    I  => aRmcDioCLIP_out(72),
    T  => aRmcDioCLIP_tri(72));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_73 (DIO_73_P)
  ---------------------------------------------------------------
  IOBUF_73 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(73),
    IO => aRmcDioCLIP(73),
    I  => aRmcDioCLIP_out(73),
    T  => aRmcDioCLIP_tri(73));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_74 (DIO_75_N)
  ---------------------------------------------------------------
  IOBUF_74 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(74),
    IO => aRmcDioCLIP(74),
    I  => aRmcDioCLIP_out(74),
    T  => aRmcDioCLIP_tri(74));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_75 (DIO_75_P)
  ---------------------------------------------------------------
  IOBUF_75 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(75),
    IO => aRmcDioCLIP(75),
    I  => aRmcDioCLIP_out(75),
    T  => aRmcDioCLIP_tri(75));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_76 (DIO_77_N)
  ---------------------------------------------------------------
  IOBUF_76 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(76),
    IO => aRmcDioCLIP(76),
    I  => aRmcDioCLIP_out(76),
    T  => aRmcDioCLIP_tri(76));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_77 (DIO_77_P)
  ---------------------------------------------------------------
  IOBUF_77 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(77),
    IO => aRmcDioCLIP(77),
    I  => aRmcDioCLIP_out(77),
    T  => aRmcDioCLIP_tri(77));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_78 (DIO_79_N)
  ---------------------------------------------------------------
  IOBUF_78 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(78),
    IO => aRmcDioCLIP(78),
    I  => aRmcDioCLIP_out(78),
    T  => aRmcDioCLIP_tri(78));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_79 (DIO_79_P)
  ---------------------------------------------------------------
  IOBUF_79 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(79),
    IO => aRmcDioCLIP(79),
    I  => aRmcDioCLIP_out(79),
    T  => aRmcDioCLIP_tri(79));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_80
  ---------------------------------------------------------------
  IOBUF_80 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(80),
    IO => aRmcDioCLIP(80),
    I  => aRmcDioCLIP_out(80),
    T  => aRmcDioCLIP_tri(80));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_81 (DIO_82_N)
  ---------------------------------------------------------------
  IOBUF_81 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(81),
    IO => aRmcDioCLIP(81),
    I  => aRmcDioCLIP_out(81),
    T  => aRmcDioCLIP_tri(81));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_82 (DIO_82_P)
  ---------------------------------------------------------------
  IOBUF_82 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(82),
    IO => aRmcDioCLIP(82),
    I  => aRmcDioCLIP_out(82),
    T  => aRmcDioCLIP_tri(82));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_83 (DIO_84_N)
  ---------------------------------------------------------------
  IOBUF_83 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(83),
    IO => aRmcDioCLIP(83),
    I  => aRmcDioCLIP_out(83),
    T  => aRmcDioCLIP_tri(83));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_84 (DIO_84_P)
  ---------------------------------------------------------------
  IOBUF_84 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(84),
    IO => aRmcDioCLIP(84),
    I  => aRmcDioCLIP_out(84),
    T  => aRmcDioCLIP_tri(84));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_93 (DIO_94_N)
  ---------------------------------------------------------------
  IOBUF_93 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(93),
    IO => aRmcDioCLIP(93),
    I  => aRmcDioCLIP_out(93),
    T  => aRmcDioCLIP_tri(93));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_94 (DIO_94_P)
  ---------------------------------------------------------------
  IOBUF_94 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(94),
    IO => aRmcDioCLIP(94),
    I  => aRmcDioCLIP_out(94),
    T  => aRmcDioCLIP_tri(94));
  ---------------------------------------------------------------
  --I/O Buffer: DIO_95
  ---------------------------------------------------------------
  IOBUF_95 : IOBUF
  port map (
    O  => aRmcDioCLIP_in(95),
    IO => aRmcDioCLIP(95),
    I  => aRmcDioCLIP_out(95),
    T  => aRmcDioCLIP_tri(95));

---------------------------------------------------------------------------------------------
end RTL;
