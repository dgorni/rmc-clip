create_clock -name AdcBitClkRaw -period 4.1667 [get_ports {aRmcDio[41]}]
set_false_path  -from [get_ports {aRmcDio[41]}] \
                -through [get_cells window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Isrds_Master] \
                -through [get_cells window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Bufio] \
                -to [get_cells -hierarchical "*Isrds*"]
				
set_false_path  -from [get_clocks AdcBitClkDiv] \
                -to [get_pins {window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/DiagramClockXingx/AdcDataOutLv_reg[*]/R}]

create_generated_clock -name AdcBitClkDiv \
                        -source [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Isrds_Master/DDLY] \
                        -divide_by 6 [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Bufr/O]
						
create_generated_clock -name AdcBitClk \
                        -source [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Isrds_Master/DDLY] \
                        -divide_by 1 [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/AdcClock_I_Bufio/O]
						
#create_generated_clock -name RegClkLv \
#                        -source [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/Gen_Bufr_Div_3.AdcClock_I_Bufr/O] \
#                        -divide_by 1 [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/RegClk_Bufr/O]

create_generated_clock -name AdcBitClkDivLv \
                       -source [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/DataClkDivBufG/SafeBUFGCTRLx/I1] \
                       -divide_by 1 \
                       [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/DataClkDivBufG/SafeBUFGCTRLx/O]
					   
set_clock_uncertainty -hold 0.290 -from [get_pins window/theCLIPs/RMC_Socket_CLIP0/AdcToplevel_Toplevel_I_AdcToplevel/AdcToplevel_I_AdcClock/DataClkDivBufG/SafeBUFGCTRLx/O]] -to [get_clocks AdcBitClkDivLv]

set clipBufgCtrl [get_cells -hier window/theCLIPs/RMC_Socket_CLIP0/*SafeBUFGCTRLx -filter {IS_SEQUENTIAL==TRUE}]
set clipBufgCtrlCePins [get_pins -of_objects $clipBufgCtrl -filter {REF_PIN_NAME == S0 || REF_PIN_NAME == S1}]
set_false_path -through $clipBufgCtrlCePins


set_false_path -from [get_clocks AdcBitClkDiv] -to [get_clocks AdcBitClkDivLv]
set_false_path -from [get_clocks AdcBitClkDivLv] -to [get_clocks AdcBitClkDiv]
set_false_path -from [get_clocks AdcBitClkDivLv] -to [get_clocks Clk40]
set_false_path -from [get_clocks AdcBitClkDiv] -to [get_clocks AdcBitClk]

#konieczne dla synchronizacji zegarow
set_false_path -from [get_clocks AdcBitClkRaw] -to [get_clocks AdcBitClk]

#jesli AdcBitClkRaw wprost uzyj tego
set_false_path -from [get_clocks AdcBitClkDiv] -to [get_clocks Clk40]
#jesli zanegowane to takze tego
set_false_path -from [get_clocks Clk40] -to [get_clocks AdcBitClkDiv]

#################################################################
# DIO_00
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[0]}]
set_property SLEW Slow [get_ports {aRmcDio[0]}]
set_property DRIVE 8 [get_ports {aRmcDio[0]}]

#################################################################
# DIO_01 (DIO_02_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[1]}]
set_property SLEW Slow [get_ports {aRmcDio[1]}]
set_property DRIVE 8 [get_ports {aRmcDio[1]}]

#################################################################
# DIO_02 (DIO_02_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[2]}]
set_property SLEW Slow [get_ports {aRmcDio[2]}]
set_property DRIVE 8 [get_ports {aRmcDio[2]}]

#################################################################
# DIO_15
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[15]}]
set_property SLEW Slow [get_ports {aRmcDio[15]}]
set_property DRIVE 8 [get_ports {aRmcDio[15]}]

#################################################################
# DIO_16 (DIO_17_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[16]}]
set_property SLEW Slow [get_ports {aRmcDio[16]}]
set_property DRIVE 8 [get_ports {aRmcDio[16]}]

#################################################################
# DIO_17 (DIO_17_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[17]}]
set_property SLEW Slow [get_ports {aRmcDio[17]}]
set_property DRIVE 8 [get_ports {aRmcDio[17]}]

#################################################################
# DIO_18 (DIO_19_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[18]}]
set_property SLEW Slow [get_ports {aRmcDio[18]}]
set_property DRIVE 8 [get_ports {aRmcDio[18]}]

#################################################################
# DIO_19 (DIO_19_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[19]}]
set_property SLEW Slow [get_ports {aRmcDio[19]}]
set_property DRIVE 8 [get_ports {aRmcDio[19]}]

#################################################################
# DIO_20 (DIO_21_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[20]}]
set_property SLEW Slow [get_ports {aRmcDio[20]}]
set_property DRIVE 8 [get_ports {aRmcDio[20]}]

#################################################################
# DIO_21 (DIO_21_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[21]}]
set_property SLEW Slow [get_ports {aRmcDio[21]}]
set_property DRIVE 8 [get_ports {aRmcDio[21]}]

#################################################################
# DIO_22 (DIO_23_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[22]}]
set_property SLEW Slow [get_ports {aRmcDio[22]}]
set_property DRIVE 8 [get_ports {aRmcDio[22]}]

#################################################################
# DIO_23 (DIO_23_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[23]}]
set_property SLEW Slow [get_ports {aRmcDio[23]}]
set_property DRIVE 8 [get_ports {aRmcDio[23]}]

#################################################################
# DIO_24 (DIO_25_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[24]}]
set_property SLEW Slow [get_ports {aRmcDio[24]}]
set_property DRIVE 8 [get_ports {aRmcDio[24]}]

#################################################################
# DIO_25 (DIO_25_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[25]}]
set_property SLEW Slow [get_ports {aRmcDio[25]}]
set_property DRIVE 8 [get_ports {aRmcDio[25]}]

#################################################################
# DIO_26 (DIO_27_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[26]}]
set_property SLEW Slow [get_ports {aRmcDio[26]}]
set_property DRIVE 8 [get_ports {aRmcDio[26]}]

#################################################################
# DIO_27 (DIO_27_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[27]}]
set_property SLEW Slow [get_ports {aRmcDio[27]}]
set_property DRIVE 8 [get_ports {aRmcDio[27]}]

#################################################################
# DIO_28 (DIO_29_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[28]}]
set_property SLEW Slow [get_ports {aRmcDio[28]}]
set_property DRIVE 8 [get_ports {aRmcDio[28]}]

#################################################################
# DIO_29 (DIO_29_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[29]}]
set_property SLEW Slow [get_ports {aRmcDio[29]}]
set_property DRIVE 8 [get_ports {aRmcDio[29]}]

#################################################################
# DIO_30 (DIO_31_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[30]}]
set_property SLEW Slow [get_ports {aRmcDio[30]}]
set_property DRIVE 8 [get_ports {aRmcDio[30]}]

#################################################################
# DIO_31 (DIO_31_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[31]}]
set_property SLEW Slow [get_ports {aRmcDio[31]}]
set_property DRIVE 8 [get_ports {aRmcDio[31]}]

#################################################################
# DIO_32 (DIO_33_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[32]}]
set_property SLEW Slow [get_ports {aRmcDio[32]}]
set_property DRIVE 8 [get_ports {aRmcDio[32]}]

#################################################################
# DIO_33 (DIO_33_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[33]}]
set_property SLEW Slow [get_ports {aRmcDio[33]}]
set_property DRIVE 8 [get_ports {aRmcDio[33]}]

#################################################################
# DIO_34 (DIO_35_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[34]}]
set_property SLEW Slow [get_ports {aRmcDio[34]}]
set_property DRIVE 8 [get_ports {aRmcDio[34]}]

#################################################################
# DIO_35 (DIO_35_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[35]}]
set_property SLEW Slow [get_ports {aRmcDio[35]}]
set_property DRIVE 8 [get_ports {aRmcDio[35]}]

#################################################################
# DIO_44 (DIO_45_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[44]}]
set_property SLEW Slow [get_ports {aRmcDio[44]}]
set_property DRIVE 8 [get_ports {aRmcDio[44]}]

#################################################################
# DIO_45 (DIO_45_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[45]}]
set_property SLEW Slow [get_ports {aRmcDio[45]}]
set_property DRIVE 8 [get_ports {aRmcDio[45]}]

#################################################################
# DIO_49 (DIO_50_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[49]}]
set_property SLEW Slow [get_ports {aRmcDio[49]}]
set_property DRIVE 8 [get_ports {aRmcDio[49]}]

#################################################################
# DIO_50 (DIO_50_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[50]}]
set_property SLEW Slow [get_ports {aRmcDio[50]}]
set_property DRIVE 8 [get_ports {aRmcDio[50]}]

#################################################################
# DIO_51 (DIO_52_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[51]}]
set_property SLEW Slow [get_ports {aRmcDio[51]}]
set_property DRIVE 8 [get_ports {aRmcDio[51]}]

#################################################################
# DIO_52 (DIO_52_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[52]}]
set_property SLEW Slow [get_ports {aRmcDio[52]}]
set_property DRIVE 8 [get_ports {aRmcDio[52]}]

#################################################################
# DIO_53 (DIO_54_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[53]}]
set_property SLEW Slow [get_ports {aRmcDio[53]}]
set_property DRIVE 8 [get_ports {aRmcDio[53]}]

#################################################################
# DIO_59 (DIO_60_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[59]}]
set_property SLEW Slow [get_ports {aRmcDio[59]}]
set_property DRIVE 8 [get_ports {aRmcDio[59]}]

#################################################################
# DIO_60 (DIO_60_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[60]}]
set_property SLEW Slow [get_ports {aRmcDio[60]}]
set_property DRIVE 8 [get_ports {aRmcDio[60]}]

#################################################################
# DIO_61 (DIO_62_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[61]}]
set_property SLEW Slow [get_ports {aRmcDio[61]}]
set_property DRIVE 8 [get_ports {aRmcDio[61]}]

#################################################################
# DIO_62 (DIO_62_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[62]}]
set_property SLEW Slow [get_ports {aRmcDio[62]}]
set_property DRIVE 8 [get_ports {aRmcDio[62]}]

#################################################################
# DIO_63
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[63]}]
set_property SLEW Slow [get_ports {aRmcDio[63]}]
set_property DRIVE 8 [get_ports {aRmcDio[63]}]

#################################################################
# DIO_64 (DIO_65_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[64]}]
set_property SLEW Slow [get_ports {aRmcDio[64]}]
set_property DRIVE 8 [get_ports {aRmcDio[64]}]

#################################################################
# DIO_65 (DIO_65_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[65]}]
set_property SLEW Slow [get_ports {aRmcDio[65]}]
set_property DRIVE 8 [get_ports {aRmcDio[65]}]

#################################################################
# DIO_66 (DIO_67_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[66]}]
set_property SLEW Slow [get_ports {aRmcDio[66]}]
set_property DRIVE 8 [get_ports {aRmcDio[66]}]

#################################################################
# DIO_67 (DIO_67_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[67]}]
set_property SLEW Slow [get_ports {aRmcDio[67]}]
set_property DRIVE 8 [get_ports {aRmcDio[67]}]

#################################################################
# DIO_68 (DIO_69_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[68]}]
set_property SLEW Slow [get_ports {aRmcDio[68]}]
set_property DRIVE 8 [get_ports {aRmcDio[68]}]

#################################################################
# DIO_69 (DIO_69_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[69]}]
set_property SLEW Slow [get_ports {aRmcDio[69]}]
set_property DRIVE 8 [get_ports {aRmcDio[69]}]

#################################################################
# DIO_70 (DIO_71_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[70]}]
set_property SLEW Slow [get_ports {aRmcDio[70]}]
set_property DRIVE 8 [get_ports {aRmcDio[70]}]

#################################################################
# DIO_71 (DIO_71_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[71]}]
set_property SLEW Slow [get_ports {aRmcDio[71]}]
set_property DRIVE 8 [get_ports {aRmcDio[71]}]

#################################################################
# DIO_72 (DIO_73_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[72]}]
set_property SLEW Slow [get_ports {aRmcDio[72]}]
set_property DRIVE 8 [get_ports {aRmcDio[72]}]

#################################################################
# DIO_73 (DIO_73_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[73]}]
set_property SLEW Slow [get_ports {aRmcDio[73]}]
set_property DRIVE 8 [get_ports {aRmcDio[73]}]

#################################################################
# DIO_74 (DIO_75_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[74]}]
set_property SLEW Slow [get_ports {aRmcDio[74]}]
set_property DRIVE 8 [get_ports {aRmcDio[74]}]

#################################################################
# DIO_75 (DIO_75_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[75]}]
set_property SLEW Slow [get_ports {aRmcDio[75]}]
set_property DRIVE 8 [get_ports {aRmcDio[75]}]

#################################################################
# DIO_76 (DIO_77_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[76]}]
set_property SLEW Slow [get_ports {aRmcDio[76]}]
set_property DRIVE 8 [get_ports {aRmcDio[76]}]

#################################################################
# DIO_77 (DIO_77_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[77]}]
set_property SLEW Slow [get_ports {aRmcDio[77]}]
set_property DRIVE 8 [get_ports {aRmcDio[77]}]

#################################################################
# DIO_78 (DIO_79_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[78]}]
set_property SLEW Slow [get_ports {aRmcDio[78]}]
set_property DRIVE 8 [get_ports {aRmcDio[78]}]

#################################################################
# DIO_79 (DIO_79_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[79]}]
set_property SLEW Slow [get_ports {aRmcDio[79]}]
set_property DRIVE 8 [get_ports {aRmcDio[79]}]

#################################################################
# DIO_80
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[80]}]
set_property SLEW Slow [get_ports {aRmcDio[80]}]
set_property DRIVE 8 [get_ports {aRmcDio[80]}]

#################################################################
# DIO_81 (DIO_82_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[81]}]
set_property SLEW Slow [get_ports {aRmcDio[81]}]
set_property DRIVE 8 [get_ports {aRmcDio[81]}]

#################################################################
# DIO_82 (DIO_82_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[82]}]
set_property SLEW Slow [get_ports {aRmcDio[82]}]
set_property DRIVE 8 [get_ports {aRmcDio[82]}]

#################################################################
# DIO_83 (DIO_84_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[83]}]
set_property SLEW Slow [get_ports {aRmcDio[83]}]
set_property DRIVE 8 [get_ports {aRmcDio[83]}]

#################################################################
# DIO_84 (DIO_84_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[84]}]
set_property SLEW Slow [get_ports {aRmcDio[84]}]
set_property DRIVE 8 [get_ports {aRmcDio[84]}]

#################################################################
# DIO_93 (DIO_94_N)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[93]}]
set_property SLEW Slow [get_ports {aRmcDio[93]}]
set_property DRIVE 8 [get_ports {aRmcDio[93]}]

#################################################################
# DIO_94 (DIO_94_P)
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[94]}]
set_property SLEW Slow [get_ports {aRmcDio[94]}]
set_property DRIVE 8 [get_ports {aRmcDio[94]}]

#################################################################
# DIO_95
#################################################################
set_property IOSTANDARD LVCMOS25 [get_ports {aRmcDio[95]}]
set_property SLEW Slow [get_ports {aRmcDio[95]}]
set_property DRIVE 8 [get_ports {aRmcDio[95]}]
