
library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.std_logic_UNSIGNED.all;
    use IEEE.numeric_std.all;
      
    use IEEE.std_logic_misc.all;
library UNISIM;
	use UNISIM.vcomponents.all;
library work;
    use work.all;
--    use work.PkgNiUtilities.all; --LV
--    use work.PkgRegPortFlat.all;
--    use work.PkgRegPort.all;

library UNISIM;
  use UNISIM.vcomponents.all;
---------------------------------------------------------------------------------------------
-- Entity pin description
---------------------------------------------------------------------------------------------
entity AdcToplevelWrapper is
  generic(
    kNumDioLines            : integer := 96;
    kNumSerialPorts         : integer := 6
  );
  port(
  
    Clk80Top        : in std_logic;
    aRmcDio : inout std_logic_vector(kNumDioLines - 1 downto 0)
  );	
end AdcToplevelWrapper;

---------------------------------------------------------------------------------------------
-- Architecture section
---------------------------------------------------------------------------------------------
architecture RTL of AdcToplevelWrapper is

component clk_wiz_0
port
 (-- Clock in ports
  -- Clock out ports
  SysRefClk          : out    std_logic;
  CLK_lv_out          : out    std_logic;
  Clk80Top           : in     std_logic
 );
end component;

signal SysRefClk : std_logic;
signal CLK_lv_out : std_logic;
signal CLK_lv_enable : std_logic;

signal AdcClkDivLv : std_logic;
signal RegClkOutLv : std_logic;
 signal AdcDataA             : std_logic_vector(15 downto 0);
 signal AdcDataB             : std_logic_vector(15 downto 0);
 signal AdcDataC             : std_logic_vector(15 downto 0);
 signal AdcDataD             : std_logic_vector(15 downto 0);
 signal AdcDataE             : std_logic_vector(15 downto 0);
 signal AdcDataF             : std_logic_vector(15 downto 0);
 signal AdcDataG             : std_logic_vector(15 downto 0);
 signal AdcDataH             : std_logic_vector(15 downto 0);
 signal AdcFrmDataOutLv      : std_logic_vector(15 downto 0);
 signal LED_lv_in   : std_logic_vector(0 downto 0);
  signal LED_lv_out   : std_logic_vector(0 downto 0);
  signal LED_lv_enable   : std_logic_vector(0 downto 0);
  signal AdcFrmSyncWrnLv      : std_logic;
  signal AdcBitClkAlgnWrnLv   : std_logic;
  signal AdcBitClkInvrtdLv    : std_logic;
  signal AdcBitClkDoneLv      : std_logic;
signal   AdcIdlyCtrlRdyLv     : std_logic;
  
signal AdcIntrfcRstLv            :  std_logic;
signal AdcIntrfcEnaLv            :  std_logic;
signal AdcReSyncLv               :  std_logic;
signal RegClkLvInvLv : std_logic;
signal aReset      :  std_logic;
signal aPowerOnReset :  std_logic;
signal GpioOut :  std_logic_vector(15 downto 0);
begin

ClkMMCM : clk_wiz_0
   port map ( 
  -- Clock out ports  
   SysRefClk => SysRefClk,
   CLK_lv_out => CLK_lv_out,
   -- Clock in ports
   Clk80Top => Clk80Top
 );


parserPr : process (AdcClkDivLv)
  begin
    if falling_edge(AdcClkDivLv) then
        LED_lv_out(0) <= or_reduce(AdcDataA) and or_reduce(AdcDataB) and or_reduce(AdcDataC) and or_reduce(AdcDataD) and or_reduce(AdcDataE) and or_reduce(AdcDataF) and or_reduce(AdcDataG) and or_reduce(AdcDataH) and or_reduce(AdcFrmDataOutLv);  
        LED_lv_enable(0) <=   AdcFrmSyncWrnLv xor AdcBitClkAlgnWrnLv xor AdcBitClkInvrtdLv xor AdcBitClkDoneLv xor AdcIdlyCtrlRdyLv; 
        AdcIntrfcRstLv   <=   LED_lv_in(0);  
        AdcIntrfcEnaLv    <=   LED_lv_in(0);  
        AdcReSyncLv      <=   LED_lv_in(0); 
          RegClkLvInvLv <=   LED_lv_in(0); 
        aReset     <=   LED_lv_in(0);  
        aPowerOnReset <=   LED_lv_in(0);  
        CLK_lv_enable <=   LED_lv_in(0);  
    end if;
  end process parserPr;


AdcCLIP : entity work.AdcToplevelCLIP
  generic map(
    kNumDioLines           => kNumDioLines,
    kNumSerialPorts        => kNumSerialPorts
  )
  port map(
  
    SysRefClkLv          => SysRefClk,
    CLKx2_lv_out        => '1',  
    AdcClkDivLv          => AdcClkDivLv, 
    AdcClkDivRdyLv       => open,
    RegClkOutLv          => RegClkOutLv,
    CLK_lv_out           => CLK_lv_out,        
    CLK_lv_enable        => CLK_lv_enable,    
    AdcDataA             => AdcDataA,          
    AdcDataB             => AdcDataB,          
    AdcDataC             => AdcDataC,          
    AdcDataD             => AdcDataD,          
    AdcDataE             => AdcDataE,          
    AdcDataF             => AdcDataF,          
    AdcDataG             => AdcDataG,          
    AdcDataH             => AdcDataH,          
    AdcFrmDataOutLv      => AdcFrmDataOutLv ,  
                         
    LED_lv_in            => LED_lv_in,      
    LED_lv_out           => LED_lv_out,        
    LED_lv_enable        => LED_lv_enable,     

    AdcFrmSyncWrnLv      => AdcFrmSyncWrnLv, 
    AdcBitClkAlgnWrnLv   => AdcBitClkAlgnWrnLv,
    AdcBitClkInvrtdLv    => AdcBitClkInvrtdLv,
    AdcBitClkDoneLv      => AdcBitClkDoneLv,
    AdcIdlyCtrlRdyLv     => AdcIdlyCtrlRdyLv,  
    
    AdcIntrfcRstLv       => AdcIntrfcRstLv,
    AdcIntrfcEnaLv       => AdcIntrfcEnaLv,
    AdcReSyncLv          => AdcReSyncLv, 
    RegClkLvInvLv        => RegClkLvInvLv,
                         
    aReset               => aReset,  
    aPowerOnReset        => aPowerOnReset, 
    aRmcDioCLIP          => aRmcDio,   
                         
    SerialClk            => SysRefClk,      
    
    aSerialEnable        => open,  
                     
    sSerialRegPortIn     => (others => '0'),
    sSerialRegPortOut    => open,
                      
    aClipIrq             => open,       
    aClipBank0_2v5       => open, 
    aClipBank1_2v5       => open, 
	GpioOut              => GpioOut,           
	GpioIn               => GpioOut            
  );	

end RTL;
