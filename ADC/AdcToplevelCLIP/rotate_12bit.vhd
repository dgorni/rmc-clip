
library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.std_logic_UNSIGNED.all;
    use IEEE.numeric_std.all;
library UNISIM;
	use UNISIM.vcomponents.all;
library work;
    use work.all;

library UNISIM;
  use UNISIM.vcomponents.all;
---------------------------------------------------------------------------------------------
-- Entity pin description
---------------------------------------------------------------------------------------------
entity FixRotate is
  port(
  DataIn              : in std_logic_vector(15 downto 0);
  Rotate			  : in std_logic_vector(7  downto 0);
  DataOut             : out std_logic_vector(15 downto 0)
    );
end FixRotate;

architecture RTL of FixRotate is

    
begin

with Rotate select DataOut <=
	"0000" & DataIn(11 downto 0) 			   			when "00000000",
	"0000" & DataIn(10 downto 0) & DataIn(11 downto 11) when "00000001",
	"0000" & DataIn(9  downto 0) & DataIn(11 downto 10) when "00000010",
	"0000" & DataIn(8  downto 0) & DataIn(11 downto  9) when "00000011",
	"0000" & DataIn(7  downto 0) & DataIn(11 downto  8) when "00000100",
	"0000" & DataIn(6  downto 0) & DataIn(11 downto  7) when "00000101",
	"0000" & DataIn(5  downto 0) & DataIn(11 downto  6) when "00000110",
	"0000" & DataIn(4  downto 0) & DataIn(11 downto  5) when "00000111",
	"0000" & DataIn(3  downto 0) & DataIn(11 downto  4) when "00001000",
	"0000" & DataIn(2  downto 0) & DataIn(11 downto  3) when "00001001",
	"0000" & DataIn(1  downto 0) & DataIn(11 downto  2) when "00001010",
	"0000" & DataIn(0  downto 0) & DataIn(11 downto  1) when "00001011",
	"0000" & "000000000000" 							when "00001100",
	"0000" & "111111111111"							    when "00001101",
	"0000" & "111111000000" 							when "00001110",
	"0000" & "000000111111" 							when "00001111",
	"0000" & "000000000000" 							when others;
	
end RTL;